using Gtk 4.0;

template $PlayControls: Box {
  MenuButton {
    menu-model: repeat_menu;
    icon-name: bind $repeat_menu_icon(template.player as <$Player>.repeat) as <string>;
    tooltip-text: _("Repeat Menu");

    accessibility {
      label: C_("a11y", "Repeat menu");
    }
  }

  Box {
    sensitive: bind $is_valid(template.player as <$Player>.queue as <$Queue>.playing-track-position) as <bool>;

    Button {
      icon-name: "media-skip-backward";
      action-name: "app.play-previous";
      sensitive: bind template.player as <$Player>.can-play-previous;
      tooltip-text: _("Play the Previous Track");

      accessibility {
        label: C_("a11y", "Skip backward");
      }
    }

    Button {
      icon-name: bind $play_button_icon(template.player as <$Player>.playing) as <string>;
      action-name: "app.playing";
      sensitive: false;
      tooltip-text: _("Toggle Play");

      accessibility {
        label: C_("a11y", "Toggle play");
      }
    }

    Button {
      icon-name: "media-skip-forward";
      action-name: "app.play-next";
      sensitive: bind template.player as <$Player>.can-play-next;
      tooltip-text: _("Play the Next Track");

      accessibility {
        label: C_("a11y", "Skip forward");
      }
    }
  }
}

menu repeat_menu {
  item {
    label: _("Do Not Repeat");
    action: "app.repeat";
    target: "none";
  }

  item {
    label: _("Repeat All Tracks");
    action: "app.repeat";
    target: "all";
  }

  item {
    label: _("Repeat Current Track");
    action: "app.repeat";
    target: "track";
  }
}
