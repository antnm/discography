using Gtk 4.0;
using Adw 1;

template $TrackPage: Adw.NavigationPage {
  title: _("Tracks");

  child: Adw.ToolbarView {
    [top]
    Adw.HeaderBar {
      show-title: false;

      [end]
      Adw.SplitButton {
        icon-name: "music-album-play";
        action-name: "app.play-selected-album";
        menu-model: album_play_menu_model;
        tooltip-text: _("Play Album");
        dropdown-tooltip: _("More Options");

        accessibility {
          label: C_("a11y", "Play album");
        }
      }
    }

    content: Gtk.Box {
      orientation: vertical;

      Gtk.Box {
        orientation: horizontal;

        styles [
          "osd",
        ]

        Picture {
          paintable: bind template.player as <$Player>.current-albums-selection as <SingleSelection>.selected-item as <$Album>.art;
          height-request: 80;
          width-request: 80;
        }

        Box {
          margin-start: 12;
          margin-end: 12;
          margin-top: 9;
          margin-bottom: 9;
          valign: center;

          Box {
            orientation: vertical;
            hexpand: true;

            Label {
              wrap: true;
              wrap-mode: word_char;
              label: bind template.player as <$Player>.current-albums-selection as <SingleSelection>.selected-item as <$Album>.formatted-title;
              lines: 2;
              ellipsize: end;
              use-markup: true;
              halign: start;

              styles [
                "title-4",
              ]
            }

            Label {
              wrap: true;
              wrap-mode: word_char;
              label: bind template.player as <$Player>.current-albums-selection as <SingleSelection>.selected-item as <$Album>.artist as <$Artist>.formatted-title;
              lines: 2;
              ellipsize: end;
              use-markup: true;
              halign: start;
            }
          }

          Box {
            orientation: vertical;
            valign: start;

            Label {
              label: bind template.player as <$Player>.current-albums-selection as <SingleSelection>.selected-item as <$Album>.duration;
              halign: end;
            }

            Label {
              label: bind template.player as <$Player>.current-albums-selection as <SingleSelection>.selected-item as <$Album>.year as <string>;
              halign: start;
            }
          }
        }
      }

      ScrolledWindow {
        vexpand: true;
        hscrollbar-policy: never;

        child: ListView list_view {
          model: NoSelection {
            model: bind template.player as <$Player>.current-tracks-slice;
          };

          single-click-activate: true;
          activate => $row_activate(template);

          factory: SignalListItemFactory {
            setup => $setup_row(template);
          };

          header-factory: SignalListItemFactory {
            setup => $setup_header(template);
          };
        };
      }
    };
  };
}

menu album_play_menu_model {
  item {
    label: _("Queue Album");
    action: "app.queue-selected-album";
  }

  item {
    label: _("Shuffle Album");
    action: "app.shuffle-selected-album";
  }
}
