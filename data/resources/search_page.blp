using Gtk 4.0;
using Adw 1;

template $SearchPage: Adw.NavigationPage {
  title: _("Search");
  tag: "search";
  shown => $shown();
  hiding => $hiding();

  child: Adw.ToolbarView {
    [top]
    Adw.HeaderBar {
      title-widget: SearchEntry search_entry {
        accessibility {
          label: C_("a11y", "Search");
        }
      };
    }

    content: Stack {
      visible-child-name: bind $visible_stack_name(template.player as <$Player>.search-store as <$SearchStore>.n-items) as <string>;

      StackPage {
        name: "empty";

        child: Adw.StatusPage {
          icon-name: "edit-find";
          title: bind $empty_state_title(search_entry.text) as <string>;
        };
      }

      StackPage {
        name: "list";

        child: ScrolledWindow {
          child: Adw.ClampScrollable {
            vexpand: true;

            child: ListView {
              single-click-activate: true;
              activate => $row_activate() swapped;

              styles [
                "navigation-sidebar",
              ]

              model: NoSelection {
                model: bind template.player as <$Player>.search-store;
              };

              header-factory: BuilderListItemFactory {
                template ListHeader {
                  child: Label {
                    label: bind template.item as <$SearchItem>.section-title;
                  };
                }
              };

              factory: BuilderListItemFactory {
                template ListItem {
                  child: Box {
                    margin-top: 12;
                    margin-bottom: 6;
                    spacing: 6;

                    Image image {
                      paintable: bind template.item as <$SearchItem>.art;
                      icon-size: large;

                      accessibility {
                        label: C_("a11y", "Album cover");
                      }
                    }

                    Box {
                      orientation: vertical;

                      Label {
                        label: bind template.item as <$SearchItem>.title;
                        ellipsize: end;
                        hexpand: true;
                        halign: start;
                      }

                      Label {
                        label: bind template.item as <$SearchItem>.additional-info;
                        ellipsize: end;
                        hexpand: true;
                        halign: start;

                        styles [
                          "dim-label",
                        ]
                      }
                    }
                  };
                }
              };
            };
          };
        };
      }
    };
  };
}
