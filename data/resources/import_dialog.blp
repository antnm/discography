using Gtk 4.0;
using GLib 2.0;
using Adw 1;

template $ImportDialog: Adw.Dialog {
  content-width: 800;
  content-height: 600;
  can-close: false;
  title: _("Import Music");

  child: Adw.NavigationView navigation_view {
    Adw.NavigationPage {
      tag: "searching";
      title: _("Importing");
      can-pop: false;

      child: Adw.ToolbarView {
        [top]
        Adw.HeaderBar {
          show-title: false;
        }

        content: Adw.StatusPage searching_status_page {
          title: _("Importing…");

          paintable: Adw.SpinnerPaintable {
            widget: searching_status_page;
          };
        };
      };
    }

    Adw.NavigationPage {
      tag: "main";
      title: _("Tagging");
      can-pop: false;

      child: Adw.ToolbarView {
        [top]
        Adw.HeaderBar {
          show-title: false;
        }

        [bottom]
        Button {
          label: _("Continue");
          margin-end: 6;
          margin-top: 6;
          margin-start: 6;
          margin-bottom: 6;
          halign: center;
          action-name: "dialog.continue";
          sensitive: bind template.can-continue;

          styles [
            "pill",
            "suggested-action"
          ]
        }

        content: ScrolledWindow {
          vexpand: true;

          child: Viewport {
            child: Adw.Clamp {
              Box {
                margin-bottom: 6;
                margin-start: 12;
                margin-end: 12;
                valign: start;
                orientation: vertical;
                spacing: 12;

                Adw.PreferencesGroup {
                  title: _("Tracks to Import");

                  ListBox new_tracks_list_box {
                    styles [
                      "boxed-list"
                    ]

                    selection-mode: none;
                  }
                }

                Adw.PreferencesGroup old_tracks_group {
                  title: _("Duplicate Tracks Already Imported");

                  ListBox old_tracks_list_box {
                    styles [
                      "boxed-list"
                    ]

                    selection-mode: none;
                  }
                }

                Adw.PreferencesGroup {
                  title: _("Choices");

                  ListBox choices_list_box {
                    styles [
                      "boxed-list"
                    ]

                    selection-mode: none;

                    Adw.ExpanderRow search_by_title_row {
                      visible: false;
                      title: _("S_earch");
                      subtitle: _("Search for a recording by its artist and title");
                      use-underline: true;

                      [prefix]
                      CheckButton search_by_title_check_button {
                        action-name: "dialog.activate-choice";
                        action-target: bind $search_by_title_variant(artist_entry_row.text, title_entry_row.text) as <GLib.Variant>;
                        notify::action-target => $activate_choice() swapped;

                        accessibility {
                          label: C_("a11y", "Search");
                        }
                      }

                      Adw.EntryRow artist_entry_row {
                        title: _("Artist");
                      }

                      Adw.EntryRow title_entry_row {
                        title: _("Title");
                      }
                    }

                    Adw.ExpanderRow search_by_id_row {
                      visible: false;
                      title: _("Enter _ID");
                      subtitle: _("Enter a MusicBrainz ID");
                      use-underline: true;

                      [prefix]
                      CheckButton search_by_id_check_button {
                        action-name: "dialog.activate-choice";
                        action-target: bind $search_by_id_variant(id_entry_row.text) as <GLib.Variant>;
                        notify::action-target => $activate_choice() swapped;

                        accessibility {
                          label: C_("a11y", "Enter ID");
                        }
                      }

                      Adw.EntryRow id_entry_row {
                        title: _("ID");
                      }
                    }

                    Adw.ActionRow import_as_is_row {
                      visible: false;
                      title: _("Import _Without Changing");
                      subtitle: _("Import the tracks without changing their tags");
                      use-underline: true;
                      activatable-widget: import_as_is_check_button;

                      [prefix]
                      CheckButton import_as_is_check_button {
                        action-name: "dialog.activate-choice";
                        action-target: '"\\"ImportAsIs\\""';
                      }
                    }

                    Adw.ActionRow skip_row {
                      visible: false;
                      title: _("_Skip");
                      subtitle: _("Don't import the tracks");
                      use-underline: true;
                      activatable-widget: skip_check_button;

                      [prefix]
                      CheckButton skip_check_button {
                        action-name: "dialog.activate-choice";
                        action-target: '"\\"Skip\\""';
                      }
                    }

                    Adw.ActionRow merge_row {
                      visible: false;
                      title: _("_Merge the Recordings");
                      subtitle: _("Merge the recording to be imported with the ones already");
                      use-underline: true;
                      activatable-widget: merge_check_button;

                      [prefix]
                      CheckButton merge_check_button {
                        action-name: "dialog.activate-choice";
                        action-target: '"\\"Merge\\""';
                      }
                    }

                    Adw.ActionRow keep_row {
                      visible: false;
                      title: _("_Keep All");
                      subtitle: _("Keeps both the existing recodings and the one to be imported");
                      use-underline: true;
                      activatable-widget: keep_check_button;

                      [prefix]
                      CheckButton keep_check_button {
                        action-name: "dialog.activate-choice";
                        action-target: '"\\"Keep\\""';
                      }
                    }

                    Adw.ActionRow remove_old_row {
                      visible: false;
                      title: _("_Remove the Old Recordings");
                      subtitle: _("Keep only the recording to be impoted and remove the old ones");
                      use-underline: true;
                      activatable-widget: remove_old_check_button;

                      [prefix]
                      CheckButton remove_old_check_button {
                        action-name: "dialog.activate-choice";
                        action-target: '"\\"Remove\\""';
                      }
                    }

                    Adw.ActionRow as_tracks_row {
                      visible: false;
                      title: _("_Ungroup Tracks");
                      subtitle: _("Don't group the tracks into an album");
                      use-underline: true;
                      activatable-widget: as_tracks_check_button;

                      [prefix]
                      CheckButton as_tracks_check_button {
                        action-name: "dialog.activate-choice";
                        action-target: '"\\"AsTracks\\""';
                      }
                    }

                    Adw.ActionRow group_row {
                      visible: false;
                      title: _("_Group Albums");
                      subtitle: _("Group the tracks into an album");
                      use-underline: true;
                      activatable-widget: groups_check_button;

                      [prefix]
                      CheckButton groups_check_button {
                        action-name: "dialog.activate-choice";
                        action-target: '"\\"Group\\""';
                      }
                    }
                  }
                }
              }
            };
          };
        };
      };
    }
  };
}
