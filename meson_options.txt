option(
  'profile',
  type: 'combo',
  choices: [
    'default',
    'development'
  ],
  value: 'default',
  description: 'The build profile for Encore. One of "default" or "development".'
)
