use {
    crate::{models::ReplayGain, player::Player},
    adw::{prelude::*, subclass::prelude::*, Dialog, Spinner},
    glib::{subclass::InitializingObject, Object, Properties, SignalHandlerId},
    gtk::{CompositeTemplate, Widget},
    std::cell::{Cell, RefCell},
};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/eu/a11s/Encore/preferences_dialog.ui")]
    #[properties(wrapper_type = super::PreferencesDialog)]
    pub struct PreferencesDialog {
        player: RefCell<Option<Player>>,
        updating_library_signal: RefCell<Option<SignalHandlerId>>,
        #[property(get, set, builder(ReplayGain::None))]
        replay_gain: Cell<ReplayGain>,
        #[property(get, set)]
        restore_state: Cell<bool>,
        #[property(get, set)]
        library_path: RefCell<String>,
        #[template_child]
        spinner: TemplateChild<Spinner>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PreferencesDialog {
        const NAME: &'static str = "PreferencesDialog";
        type Type = super::PreferencesDialog;
        type ParentType = adw::PreferencesDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for PreferencesDialog {
        fn dispose(&self) {
            if let Some(signal_handler_id) = self.updating_library_signal.take() {
                if let Some(player) = self.player.take() {
                    player.disconnect(signal_handler_id);
                }
            }
        }
    }

    impl WidgetImpl for PreferencesDialog {}

    impl AdwDialogImpl for PreferencesDialog {
        fn close_attempt(&self) {
            if self
                .player
                .borrow()
                .as_ref()
                .is_some_and(|player| !player.updating_library())
            {
                self.obj().force_close();
            }
        }
    }

    impl PreferencesDialogImpl for PreferencesDialog {}

    impl super::PreferencesDialog {
        pub fn new(player: &Player) -> Self {
            let dialog: Self = Object::new();
            let imp = dialog.imp();
            imp.updating_library_signal
                .replace(Some(player.connect_updating_library_notify(glib::clone!(
                    #[weak]
                    imp,
                    move |player| {
                        let obj = imp.obj();
                        if player.updating_library() {
                            imp.spinner.set_visible(true);
                            obj.set_sensitive(false);
                            obj.set_can_close(false);
                        } else {
                            imp.spinner.set_visible(false);
                            obj.set_sensitive(true);
                            obj.set_can_close(true);
                        }
                    }
                ))));
            imp.player.replace(Some(player.clone()));
            dialog
        }
    }
}

glib::wrapper! {
    pub struct PreferencesDialog(ObjectSubclass<imp::PreferencesDialog>)
        @extends Widget, Dialog, adw::PreferencesDialog;
}
