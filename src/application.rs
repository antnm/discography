use {
    crate::{
        config::{APP_ID, PKGDATADIR, PROFILE, VERSION},
        library,
        models::{AlbumShuffle, ArtistShuffle, Enqueue, MoveTracks},
        mpris,
        player::Player,
        window::Window,
    },
    adw::{prelude::*, subclass::prelude::*},
    gio::{ActionEntry, ActionGroup, ActionMap, PropertyAction},
    glib::{ExitCode, MainContext, Object, Properties, VariantTy, WeakRef},
    gstreamer_play::gst::ClockTime,
    gtk::{CssProvider, FileDialog},
    std::{
        cell::OnceCell,
        ffi::OsStr,
        path::{Path, PathBuf},
    },
    tracing::{debug, error},
};

mod imp {
    use super::*;

    #[derive(Debug, Default, Properties)]
    #[properties(wrapper_type = super::Application)]
    pub struct Application {
        window: OnceCell<WeakRef<Window>>,
        #[property(get)]
        player: Player,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Application {
        const NAME: &'static str = "Application";
        type Type = super::Application;
        type ParentType = adw::Application;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Application {}

    impl ApplicationImpl for Application {
        fn activate(&self) {
            self.parent_activate();
            let app = self.obj();

            let window = if let Some(window) = self.window.get() {
                if let Some(window) = window.upgrade() {
                    window
                } else {
                    error!("Failed to upgrade window reference");
                    return;
                }
            } else {
                let window = Window::new(&app);
                if self.window.set(window.downgrade()).is_err() {
                    error!("Failed to set application window");
                    return;
                }
                mpris::start_server(self.player.clone(), window.clone(), app.clone());
                window
            };
            window.present();
        }

        fn startup(&self) {
            self.parent_startup();
            let app = self.obj();

            gtk::Window::set_default_icon_name(APP_ID);

            app.setup_css();
            app.setup_gactions();
            app.setup_accels();
        }
    }

    impl GtkApplicationImpl for Application {}

    impl AdwApplicationImpl for Application {}

    impl super::Application {
        fn main_window(&self) -> Option<Window> {
            let win = self.imp().window.get().and_then(|w| w.upgrade());
            if win.is_none() {
                error!("Application window not set");
            }
            win
        }

        fn setup_gactions(&self) {
            self.add_action_entries([
                ActionEntry::builder("quit")
                    .activate(move |app: &Self, _, _| {
                        if let Some(w) = app.main_window() {
                            w.close();
                        }
                        app.quit();
                    })
                    .build(),
                ActionEntry::builder("seek")
                    .parameter_type(Some(VariantTy::UINT64))
                    .activate(|app: &Self, _, variant| {
                        if variant
                            .and_then(|v| v.get())
                            .map(|v| app.player().seek(ClockTime::from_nseconds(v)))
                            .is_none()
                        {
                            error!("app.seek argument not correct: {variant:#?}");
                        }
                    })
                    .build(),
                ActionEntry::builder("play-next")
                    .activate(|app: &Self, _, _| app.player().play_next())
                    .build(),
                ActionEntry::builder("play-previous")
                    .activate(|app: &Self, _, _| app.player().play_previous())
                    .build(),
                ActionEntry::builder("seek-forward")
                    .activate(|app: &Self, _, _| {
                        app.player().seek_forward(ClockTime::from_seconds(5))
                    })
                    .build(),
                ActionEntry::builder("seek-backward")
                    .activate(|app: &Self, _, _| {
                        app.player().seek_backward(ClockTime::from_seconds(5))
                    })
                    .build(),
                ActionEntry::builder("play-from-queue")
                    .parameter_type(Some(VariantTy::UINT32))
                    .activate(|app: &Self, _, variant| {
                        if variant
                            .and_then(|v| v.get::<u32>())
                            .map(|v| app.player().queue().set_playing_track_position(v))
                            .is_none()
                        {
                            error!("app.play-from-queue argument not correct: {variant:#?}");
                        }
                    })
                    .build(),
                ActionEntry::builder("select-from-queue")
                    .parameter_type(Some(VariantTy::UINT32))
                    .activate(|app: &Self, _, variant| {
                        if variant
                            .and_then(|v| v.get::<u32>())
                            .map(|v| app.player().queue().toggle_select(v))
                            .is_none()
                        {
                            error!("app.select-from-queue argument not correct: {variant:#?}");
                        }
                    })
                    .build(),
                ActionEntry::builder("set-select-all-from-queue")
                    .parameter_type(Some(VariantTy::BOOLEAN))
                    .activate(|app: &Self, _, variant| {
                        if variant
                            .and_then(|v| v.get())
                            .map(|v| app.player().queue().set_select_all_from_queue(v))
                            .is_none()
                        {
                            error!("app.select-from-queue argument not correct: {variant:#?}");
                        }
                    })
                    .build(),
                ActionEntry::builder("queue-move-up")
                    .activate(|app: &Self, _, _| {
                        app.player().queue().move_tracks(MoveTracks::Up);
                    })
                    .build(),
                ActionEntry::builder("queue-move-down")
                    .activate(|app: &Self, _, _| {
                        app.player().queue().move_tracks(MoveTracks::Down);
                    })
                    .build(),
                ActionEntry::builder("queue-move-top")
                    .activate(|app: &Self, _, _| {
                        app.player().queue().move_tracks(MoveTracks::Top);
                    })
                    .build(),
                ActionEntry::builder("queue-move-bottom")
                    .activate(|app: &Self, _, _| {
                        app.player().queue().move_tracks(MoveTracks::Bottom);
                    })
                    .build(),
                ActionEntry::builder("queue-remove-selected")
                    .activate(|app: &Self, _, _| {
                        app.player().queue().remove_selected();
                    })
                    .build(),
                ActionEntry::builder("play-selected-artist")
                    .activate(|app: &Self, _, _| {
                        app.player()
                            .play_artist(ArtistShuffle::None, Enqueue::Replace)
                    })
                    .build(),
                ActionEntry::builder("queue-selected-artist")
                    .activate(|app: &Self, _, _| {
                        app.player().play_artist(ArtistShuffle::None, Enqueue::Add)
                    })
                    .build(),
                ActionEntry::builder("shuffle-selected-artist")
                    .activate(|app: &Self, _, _| {
                        app.player()
                            .play_artist(ArtistShuffle::Track, Enqueue::Replace)
                    })
                    .build(),
                ActionEntry::builder("shuffle-selected-artist-by-album")
                    .activate(|app: &Self, _, _| {
                        app.player()
                            .play_artist(ArtistShuffle::Album, Enqueue::Replace)
                    })
                    .build(),
                ActionEntry::builder("play-selected-album")
                    .activate(|app: &Self, _, _| {
                        app.player()
                            .play_album(AlbumShuffle::None, Enqueue::Replace)
                    })
                    .build(),
                ActionEntry::builder("queue-selected-album")
                    .activate(|app: &Self, _, _| {
                        app.player().play_album(AlbumShuffle::None, Enqueue::Add)
                    })
                    .build(),
                ActionEntry::builder("shuffle-selected-album")
                    .activate(|app: &Self, _, _| {
                        app.player()
                            .play_album(AlbumShuffle::Track, Enqueue::Replace)
                    })
                    .build(),
                ActionEntry::builder("show-artist")
                    .parameter_type(Some(VariantTy::UINT32))
                    .activate(|app: &Self, _, variant| {
                        if let Some(v) = variant.and_then(|v| v.get::<u32>()) {
                            app.player().show_artist(v);
                        } else {
                            error!("app.show-artist argument not correct: {variant:#?}");
                        }
                    })
                    .build(),
                ActionEntry::builder("show-album")
                    .parameter_type(Some(VariantTy::UINT32))
                    .activate(|app: &Self, _, variant| {
                        if variant
                            .and_then(|v| v.get())
                            .map(|v| app.player().show_album(v))
                            .is_none()
                        {
                            error!("app.show-album argument not correct: {variant:#?}");
                        }
                    })
                    .build(),
                ActionEntry::builder("show-track")
                    .parameter_type(Some(VariantTy::UINT32))
                    .activate(|app: &Self, _, variant| {
                        if variant
                            .and_then(|v| v.get())
                            .map(|v| {
                                let Some(track) = app.player().show_album_of_track(v) else {
                                    return;
                                };
                                if let Some(w) = app.main_window() {
                                    w.focus_track(track.position_in_album());
                                }
                            })
                            .is_none()
                        {
                            error!("app.show-album-of-track argument not correct: {variant:#?}");
                        }
                    })
                    .build(),
                ActionEntry::builder("select-music-folder")
                    .activate(|app: &Self, _, _| {
                        let app = app.clone();
                        MainContext::default().spawn_local(async move {
                            let win = app.main_window();
                            let win = win.as_ref();
                            let file = FileDialog::new().select_folder_future(win).await;
                            if let Some(path) = file.ok().and_then(|file| file.path()) {
                                app.update_library(path, false).await;
                            }
                        });
                    })
                    .build(),
                ActionEntry::builder("reload-library")
                    .activate(|app: &Self, _, _| {
                        let app = app.clone();
                        MainContext::default().spawn_local(async move {
                            app.update_library(
                                Path::new(&app.player().library_path()).to_path_buf(),
                                false,
                            )
                            .await;
                        });
                    })
                    .build(),
                ActionEntry::builder("clear-library")
                    .activate(|app: &Self, _, _| {
                        let app = app.clone();
                        MainContext::default().spawn_local(async move {
                            app.update_library(
                                Path::new(&app.player().library_path()).to_path_buf(),
                                true,
                            )
                            .await;
                        });
                    })
                    .build(),
                ActionEntry::builder("play-track-from-album")
                    .parameter_type(Some(VariantTy::UINT32))
                    .activate(|app: &Self, _, variant| {
                        if let Some(track) = variant.and_then(|v| v.get()) {
                            app.player().play_track_from_album(track);
                        } else {
                            error!("app.show-album-of-track argument not correct: {variant:#?}");
                        }
                    })
                    .build(),
                ActionEntry::builder("add-to-queue")
                    .parameter_type(Some(VariantTy::UINT32))
                    .activate(|app: &Self, _, variant| {
                        if let Some(track) = variant.and_then(|v| v.get()) {
                            app.player().add_to_queue(track);
                        } else {
                            error!("app.add-to-queue argument not correct: {variant:#?}");
                        }
                    })
                    .build(),
            ]);
            self.add_action(&PropertyAction::new("playing", &self.player(), "playing"));
            self.add_action(&PropertyAction::new("repeat", &self.player(), "repeat"));
            self.add_action(&PropertyAction::new(
                "dragging-time",
                &self.player(),
                "dragging",
            ));
        }

        async fn update_library(&self, path: PathBuf, clear: bool) {
            let current_exe = match std::env::current_exe() {
                Ok(p) => p,
                Err(e) => {
                    error!("Failed to retrieve current executable: {e}");
                    return;
                }
            };
            let subprocess = match library::subprocess_launcher().spawn(&[
                current_exe.as_os_str(),
                OsStr::new(if clear { "clear" } else { "update" }),
                path.as_os_str(),
            ]) {
                Ok(s) => s,
                Err(e) => {
                    error!("Failed to spawn subprocess: {e}");
                    return;
                }
            };
            let player = self.player();
            player.set_updating_library(true);
            if let Err(e) = subprocess.wait_check_future().await {
                error!("Failed to wait for future: {e}");
            }
            match String::from_utf8(path.into_os_string().into_encoded_bytes()) {
                Ok(p) => player.set_library_path(p),
                Err(e) => error!("Failed to get library path as UTF-8: {e}"),
            }
            player.set_updating_library(false);
            player.reload_library();
        }

        fn setup_accels(&self) {
            self.set_accels_for_action("app.quit", &["<Control>q"]);
            self.set_accels_for_action("window.close", &["<Control>w"]);
        }

        fn setup_css(&self) {
            let provider = CssProvider::new();
            provider.load_from_resource("/eu/a11s/Encore/style.css");
            if let Some(display) = gdk::Display::default() {
                gtk::style_context_add_provider_for_display(
                    &display,
                    &provider,
                    gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
                );
            }
        }

        pub fn run(&self) -> ExitCode {
            debug!(APP_ID, VERSION, PROFILE, PKGDATADIR, "Running app");

            ApplicationExtManual::run(self)
        }
    }

    impl Default for super::Application {
        fn default() -> Self {
            Object::builder()
                .property("application-id", APP_ID)
                .property("resource-base-path", "/eu/a11s/Encore/")
                .build()
        }
    }
}

glib::wrapper! {
    pub struct Application(ObjectSubclass<imp::Application>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements ActionMap, ActionGroup;
}
