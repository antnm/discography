use {crate::i18n::fgettext, gstreamer_play::gst::ClockTime};

pub fn format_clock_time(clock_time: ClockTime) -> String {
    let seconds = clock_time.seconds() % 60;
    let minutes = clock_time.minutes() % 60;
    let hours = clock_time.hours();
    if hours > 0 {
        // Translators: This is a string that shows the duration of a track in hours, minutes and seconds. Do not translate the contents between '{' and '}', this is a variable name
        fgettext(
            "{hours}:{minutes}:{seconds}",
            &[
                ("hours", &hours.to_string()),
                ("minutes", &format!("{minutes:02}")),
                ("seconds", &format!("{seconds:02}")),
            ],
        )
    } else {
        // Translators: This is a string that shows the duration of a track in minutes and seconds. Do not translate the contents between '{' and '}', this is a variable name
        fgettext(
            "{minutes}:{seconds}",
            &[
                ("minutes", &format!("{minutes}")),
                ("seconds", &format!("{seconds:02}")),
            ],
        )
    }
}

// Source: https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/css-variables.html#error-colors
pub const DARK_ERROR_COLOR: &str = "#ff938c";
pub const LIGHT_ERROR_COLOR: &str = "#c30000";
