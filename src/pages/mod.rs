mod album_page;
mod search_page;
mod sidebar_page;
mod track_page;

pub use {
    album_page::AlbumPage, search_page::SearchPage, sidebar_page::SidebarPage,
    track_page::TrackPage,
};
