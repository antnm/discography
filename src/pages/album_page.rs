use {
    crate::{i18n::nfgettext, models::Album, widgets::ArtContainer},
    adw::{prelude::*, subclass::prelude::*, NavigationPage},
    gdk::pango::EllipsizeMode,
    glib::{subclass::InitializingObject, Object, Properties},
    gtk::{Align, Box, CompositeTemplate, Label, ListItem, Orientation, SingleSelection, Widget},
    std::cell::{Cell, RefCell},
};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/eu/a11s/Encore/album_page.ui")]
    #[properties(wrapper_type = super::AlbumPage)]
    pub struct AlbumPage {
        #[property(get, set)]
        current_albums_selection: RefCell<SingleSelection>,
        #[property(get, set)]
        sidebar_selection: RefCell<SingleSelection>,
        #[property(get, set)]
        show_artist_sidebar_button: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AlbumPage {
        const NAME: &'static str = "AlbumPage";
        type Type = super::AlbumPage;
        type ParentType = NavigationPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[gtk::template_callbacks]
    impl AlbumPage {
        #[template_callback]
        fn number_of_albums(&self, n: u32) -> String {
            nfgettext(
                // Translators: This is a string that appears after the name of an artist. For example, "Artist name (3 albums)". Do not translate the contents between '{' and '}', this is a variable name
                " ({number_of_albums} album)",
                " ({number_of_albums} albums)",
                n,
                &[("number_of_albums", &n.to_string())],
            )
        }

        #[template_callback]
        fn setup_cell(&self, list_item: ListItem) {
            let child = Box::new(Orientation::Vertical, 0);
            let art_container = ArtContainer::new();
            list_item
                .property_expression_weak("item")
                .chain_property::<Album>("art")
                .bind(&art_container, "art", None::<&Object>);
            child.append(&art_container);
            let label_box = Box::builder()
                .orientation(Orientation::Vertical)
                .valign(Align::End)
                .vexpand(true)
                .build();
            child.append(&label_box);
            let title_label = Label::builder()
                .lines(1)
                .ellipsize(EllipsizeMode::End)
                .hexpand(true)
                .use_markup(true)
                .build();
            list_item
                .property_expression_weak("item")
                .chain_property::<Album>("formatted-title")
                .bind(&title_label, "label", None::<&Object>);
            label_box.append(&title_label);
            let year_label = Label::builder()
                .lines(1)
                .ellipsize(EllipsizeMode::End)
                .hexpand(true)
                .css_classes(["dim-label"])
                .build();
            list_item
                .property_expression_weak("item")
                .chain_property::<Album>("year")
                .bind(&year_label, "label", None::<&Object>);
            label_box.append(&year_label);
            list_item.set_child(Some(&child));
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for AlbumPage {}

    impl WidgetImpl for AlbumPage {}

    impl NavigationPageImpl for AlbumPage {}
}

glib::wrapper! {
    pub struct AlbumPage(ObjectSubclass<imp::AlbumPage>)
        @extends Widget;
}
