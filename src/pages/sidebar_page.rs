use {
    crate::models::SidebarItem,
    adw::{prelude::*, subclass::prelude::*, NavigationPage},
    gdk::pango::EllipsizeMode,
    gio::MenuModel,
    glib::{subclass::InitializingObject, Object, Properties},
    gtk::{
        Align, CompositeTemplate, Label, ListHeader, ListItem, Orientation, Separator,
        SignalListItemFactory, SingleSelection, Widget,
    },
    std::cell::RefCell,
};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/eu/a11s/Encore/sidebar_page.ui")]
    #[properties(wrapper_type = super::SidebarPage)]
    pub struct SidebarPage {
        #[property(get, set)]
        sidebar_selection: RefCell<SingleSelection>,
        #[property(get, set)]
        app_menu: RefCell<Option<MenuModel>>,
        #[property(get, set)]
        import_menu: RefCell<Option<MenuModel>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SidebarPage {
        const NAME: &'static str = "SidebarPage";
        type Type = super::SidebarPage;
        type ParentType = NavigationPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for SidebarPage {}

    impl WidgetImpl for SidebarPage {}

    impl NavigationPageImpl for SidebarPage {}

    #[gtk::template_callbacks]
    impl SidebarPage {
        #[template_callback]
        fn setup_row(_: SignalListItemFactory, list_item: ListItem) {
            let child = Label::builder()
                .lines(1)
                .ellipsize(EllipsizeMode::End)
                .hexpand(true)
                .halign(Align::Start)
                .margin_bottom(12)
                .margin_end(12)
                .margin_start(12)
                .margin_top(12)
                .use_markup(true)
                .build();
            let item_expression = list_item.property_expression_weak("item");
            item_expression
                .chain_property::<SidebarItem>("formatted-title")
                .bind(&child, "label", None::<&Object>);
            list_item.set_child(Some(&child));
        }

        #[template_callback]
        fn setup_header(_: SignalListItemFactory, list_header: ListHeader) {
            let child = Separator::new(Orientation::Horizontal);
            list_header
                .property_expression_weak("start")
                .chain_closure::<bool>(glib::closure_local!(
                    move |_: Option<&Object>, start: u32| start != 0
                ))
                .bind(&child, "visible", None::<&Object>);
            list_header.set_child(Some(&child));
        }
    }
}

glib::wrapper! {
    pub struct SidebarPage(ObjectSubclass<imp::SidebarPage>)
        @extends Widget;
}
