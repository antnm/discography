use {
    crate::{
        models::{Queue, QueuedTrack, Track},
        player::Player,
        widgets::TrackRow,
    },
    adw::{prelude::*, subclass::prelude::*, NavigationPage},
    glib::{subclass::InitializingObject, Object, Properties},
    gtk::{
        ClosureExpression, CompositeTemplate, Label, ListHeader, ListItem, ListScrollFlags,
        ListView, Widget,
    },
    std::cell::RefCell,
    tracing::error,
};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/eu/a11s/Encore/track_page.ui")]
    #[properties(wrapper_type = super::TrackPage)]
    pub struct TrackPage {
        #[property(get, set)]
        player: RefCell<Option<Player>>,
        #[template_child]
        list_view: TemplateChild<ListView>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TrackPage {
        const NAME: &'static str = "TrackPage";
        type Type = super::TrackPage;
        type ParentType = NavigationPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl super::TrackPage {
        pub fn focus_track(&self, position: u32) {
            let imp = self.imp();
            imp.list_view.grab_focus();
            imp.list_view
                .scroll_to(position, ListScrollFlags::FOCUS, None);
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for TrackPage {}

    impl WidgetImpl for TrackPage {}

    impl NavigationPageImpl for TrackPage {}

    #[gtk::template_callbacks]
    impl TrackPage {
        #[template_callback]
        fn row_activate(&self, position: u32) {
            if let Err(e) = self
                .obj()
                .activate_action("app.play-track-from-album", Some(&position.to_variant()))
            {
                error!("Failed to activate action: {e}");
            }
        }

        #[template_callback]
        fn setup_row(&self, list_item: ListItem) {
            let child = TrackRow::new();
            list_item
                .bind_property("item", &child, "item")
                .sync_create()
                .build();
            if let Some(player) = self.player.borrow().as_ref() {
                ClosureExpression::new::<bool>(
                    [
                        player
                            .property_expression_weak("queue")
                            .chain_property::<Queue>("playing-track"),
                        list_item.property_expression_weak("item"),
                    ],
                    glib::closure!(move |_: Option<&Object>,
                                         playing_track: Option<QueuedTrack>,
                                         track: Option<Track>| {
                        if let Some(track) = track {
                            if let Some(playing_track) =
                                playing_track.as_ref().and_then(QueuedTrack::track)
                            {
                                return playing_track == track;
                            }
                        }
                        false
                    }),
                )
                .bind(&child, "playing", None::<&Object>);
            } else {
                error!("Player not set");
            }
            list_item.set_child(Some(&child));
        }

        #[template_callback]
        fn setup_header(&self, list_header: ListHeader) {
            let child = Label::new(None);
            let Some(track_store) = self.player.borrow().as_ref().map(Player::track_store) else {
                error!("Track store not set");
                return;
            };
            let disc_title = ClosureExpression::new::<Option<String>>(
                [list_header.property_expression_weak("item")],
                glib::closure_local!(move |_: Option<&Object>, track: Option<&Track>| {
                    track.and_then(|track| track_store.disc_title(track.position()))
                }),
            );
            disc_title.bind(&child, "label", None::<&Object>);
            disc_title
                .chain_closure::<bool>(glib::closure!(
                    |_: Option<&Object>, title: Option<String>| title.is_some()
                ))
                .bind(&child, "visible", None::<&Object>);
            list_header.set_child(Some(&child));
        }
    }
}

glib::wrapper! {
    pub struct TrackPage(ObjectSubclass<imp::TrackPage>)
        @extends Widget;
}
