use {
    crate::{
        models::{SearchItem, SearchItemPosition},
        player::Player,
    },
    adw::{prelude::*, subclass::prelude::*, NavigationPage},
    gettextrs::gettext,
    glib::{subclass::InitializingObject, Properties},
    gtk::{CompositeTemplate, SearchEntry, Widget},
    std::cell::RefCell,
    tracing::error,
};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/eu/a11s/Encore/search_page.ui")]
    #[properties(wrapper_type = super::SearchPage)]
    pub struct SearchPage {
        #[property(get, set = Self::set_player)]
        player: RefCell<Option<Player>>,
        #[template_child]
        search_entry: TemplateChild<SearchEntry>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SearchPage {
        const NAME: &'static str = "SearchPage";
        type Type = super::SearchPage;
        type ParentType = NavigationPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[gtk::template_callbacks]
    impl SearchPage {
        fn set_player(&self, player: Option<Player>) {
            if let Some(player) = player {
                self.search_entry
                    .bind_property("text", &player.search_store(), "query")
                    .sync_create()
                    .bidirectional()
                    .build();
                self.player.replace(Some(player));
            }
        }

        #[template_callback]
        fn empty_state_title(&self, query: &str) -> String {
            if query.is_empty() {
                gettext("Start Typing to Search")
            } else {
                gettext("No Results Found")
            }
        }

        #[template_callback]
        fn visible_stack_name(&self, track_count: u32) -> &'static str {
            if track_count == 0 {
                "empty"
            } else {
                "list"
            }
        }

        #[template_callback]
        fn row_activate(&self, position: u32) {
            let obj = self.obj();
            let position = if let Some(position) = self
                .player
                .borrow()
                .as_ref()
                .and_then(|player| player.search_store().item(position))
                .and_then(|item| item.downcast::<SearchItem>().ok())
                .map(|item| item.position())
            {
                position
            } else {
                error!("Failed to get search item position");
                return;
            };
            let action_result = match position {
                SearchItemPosition::Track(position) => {
                    obj.activate_action("app.show-track", Some(&position.to_variant()))
                }
                SearchItemPosition::Album(position) => {
                    obj.activate_action("app.show-album", Some(&position.to_variant()))
                }
                SearchItemPosition::Artist(position) => {
                    obj.activate_action("app.show-artist", Some(&position.to_variant()))
                }
            }
            .and_then(|_| obj.activate_action("navigation.pop", None));
            if let Err(e) = action_result {
                error!("Failed to activate action: {e}");
            }
        }

        #[template_callback]
        fn shown(&self) {
            self.search_entry.grab_focus();
        }

        #[template_callback]
        fn hiding(&self) {
            if let Some(player) = self.player.borrow().as_ref() {
                player.search_store().set_query(String::from(""));
            } else {
                error!("Failed to get search store");
            }
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for SearchPage {}

    impl WidgetImpl for SearchPage {}

    impl NavigationPageImpl for SearchPage {}
}

glib::wrapper! {
    pub struct SearchPage(ObjectSubclass<imp::SearchPage>)
        @extends Widget;
}
