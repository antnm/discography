use gettextrs::{gettext, ngettext};

pub fn fgettext(msgid: &str, args: &[(&str, &str)]) -> String {
    let s = gettext(msgid);
    replace(s, args)
}

pub fn nfgettext(msgid: &str, msgid_plural: &str, n: u32, args: &[(&str, &str)]) -> String {
    let s = ngettext(msgid, msgid_plural, n);
    replace(s, args)
}

fn replace(mut s: String, args: &[(&str, &str)]) -> String {
    for (k, v) in args {
        s = s.replace(&format!("{{{k}}}"), v);
    }
    s
}
