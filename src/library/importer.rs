use {
    super::SqliteOrPythonError,
    crate::{
        i18n::fgettext,
        library::{database::new_library, recreate_database},
        models::{
            ImportDialogChoice, ImportDialogMessage, ImportFailure, ImportItem,
            ImportItemDescription, ImportSessionMessage,
        },
        utils::{format_clock_time, LIGHT_ERROR_COLOR},
    },
    core::str,
    gettextrs::gettext,
    gstreamer_play::gst::ClockTime,
    ipc_channel::ipc::{IpcOneShotServer, IpcReceiver, IpcSender},
    pyo3::{
        exceptions::{PyException, PyValueError},
        prelude::*,
        types::{PyBytes, PyDict, PyList, PyType},
        wrap_pymodule, Bound, PyAny, PyErr, PyResult, Python,
    },
    serde::{Deserialize, Serialize},
    similar::ChangeTag,
    std::{
        cell::OnceCell,
        ffi::OsStr,
        os::unix::ffi::OsStrExt,
        path::{Path, PathBuf},
        sync::Mutex,
    },
    tracing::{debug, info, warn},
};

#[derive(Debug, Serialize, Deserialize)]
enum UriType {
    File,
    Web,
}

#[derive(Debug, Serialize, Deserialize)]
struct Uri {
    href: String,
    display: String,
    uri_type: UriType,
}

impl Uri {
    fn formatted_subtitle(&self) -> String {
        let anchor = format!(
            "<a href=\"{}\">{}</a>",
            self.href,
            glib::markup_escape_text(&self.display)
        );
        match self.uri_type {
            UriType::File => fgettext(
                // Translators: Location refers here to the location to one of the user's music files or a folder containing music. Do not translate the contents between '{' and '}', this is a variable name
                "Location: {location}",
                &[("location", &anchor)],
            ),
            UriType::Web => fgettext(
                // Translators: ID refers here to the MusicBrainz ID of a music album or music track. MusicBrainz is an external service to retrieve information about music. Do not translate the contents between '{' and '}', this is a variable name
                "ID: {location}",
                &[("location", &anchor)],
            ),
        }
    }
}

trait AlbumOrTrack {
    fn uri(&self) -> Option<&Uri>;
    fn formatted_title(&self) -> String;
}

#[derive(Debug, Serialize, Deserialize)]
struct Track {
    track: Option<u32>,
    length: Option<f64>,
    artist: Option<String>,
    title: String,
    uri: Option<Uri>,
}

impl Track {
    fn approx_eq(&self, other: &Self) -> bool {
        self.track == other.track
            && self.title == other.title
            && self.artist == other.artist
            && match (self.length, other.length) {
                (Some(a), Some(b)) => lengths_approx_eq(a, b),
                _ => false,
            }
    }

    fn from_track_info(info: &Bound<PyAny>) -> PyResult<Self> {
        Ok(Self {
            track: info.getattr("index")?.extract()?,
            length: info.getattr("length")?.extract::<Option<f64>>()?,
            title: info.getattr("title")?.extract()?,
            artist: info.getattr("artist")?.extract()?,
            uri: if let Some(data_url) = info.getattr("data_url")?.extract::<Option<String>>()? {
                info.getattr("track_id")?
                    .extract::<Option<String>>()?
                    .map(|track_id| Uri {
                        href: data_url,
                        display: track_id,
                        uri_type: UriType::Web,
                    })
            } else {
                None
            },
        })
    }

    fn from_item(item: &Bound<PyAny>) -> PyResult<Self> {
        let path = item.getattr("path")?;
        let path = Path::new(OsStr::from_bytes(path.downcast::<PyBytes>()?.as_bytes()));
        Ok(Self {
            track: item.getattr("track")?.extract()?,
            length: Some(item.getattr("length")?.extract()?),
            title: item.getattr("title")?.extract()?,
            artist: item.getattr("artist")?.extract()?,
            uri: Some(Uri {
                href: glib::markup_escape_text(
                    &glib::filename_to_uri(path, None)
                        .map_err(|e| PyValueError::new_err(e.to_string()))?,
                )
                .to_string(),
                display: path
                    .strip_prefix(glib::home_dir())
                    .unwrap_or(path)
                    .to_string_lossy()
                    .to_string(),
                uri_type: UriType::File,
            }),
        })
    }

    fn as_import_item_description(&self) -> ImportItemDescription {
        ImportItemDescription {
            title: self.formatted_title(),
            subtitle: self.uri.as_ref().map(Uri::formatted_subtitle),
        }
    }

    fn as_import_item(&self) -> ImportItem {
        ImportItem::Single {
            description: self.as_import_item_description(),
        }
    }

    fn formatted_difference_title(&self, new: &Self) -> String {
        if self.approx_eq(new) {
            return new.formatted_title();
        }
        let mut new_title = bold_markup(&gettext(
            // Translators: This is a string that appears before information about a new music track that is to be imported (in English, it looks like this: "New: Artist - 1. Title")
            "New: ",
        ));
        let mut old_title = bold_markup(&gettext(
            // Translators: This is a string that appears before information about a new music track that is to be changed (in English, it looks like this: "Old: Artist - 1. Title")
            "Old: ",
        ));
        match (&self.artist, &new.artist) {
            (None, None) => (),
            (None, Some(artist)) => new_title.push_str(&error_markup_escape(artist)),
            (Some(artist), None) => old_title.push_str(&error_markup_escape(artist)),
            (Some(old_artist), Some(new_artist)) => {
                for change in similar::utils::diff_unicode_words(
                    similar::Algorithm::Patience,
                    old_artist,
                    new_artist,
                ) {
                    match change {
                        (ChangeTag::Equal, s) => {
                            let s = glib::markup_escape_text(s);
                            old_title.push_str(&s);
                            new_title.push_str(&s);
                        }
                        (ChangeTag::Insert, s) => new_title.push_str(&error_markup_escape(s)),
                        (ChangeTag::Delete, s) => old_title.push_str(&error_markup_escape(s)),
                    }
                }
                new_title.push_str(&separator());
                old_title.push_str(&separator());
            }
        }
        match (self.track, new.track) {
            (None, None) => (),
            (None, Some(track)) => new_title.push_str(&error_markup(&format_track(track))),
            (Some(track), None) => old_title.push_str(&error_markup(&format_track(track))),
            (Some(old_track), Some(new_track)) if old_track == new_track => {
                let s = format_track(old_track);
                old_title.push_str(&s);
                new_title.push_str(&s);
            }
            (Some(old_track), Some(new_track)) => {
                old_title.push_str(&error_markup(&format_track(old_track)));
                new_title.push_str(&error_markup(&format_track(new_track)));
            }
        }
        for change in similar::utils::diff_unicode_words(
            similar::Algorithm::Patience,
            &self.title,
            &new.title,
        ) {
            match change {
                (ChangeTag::Equal, s) => {
                    let s = glib::markup_escape_text(s);
                    old_title.push_str(&s);
                    new_title.push_str(&s);
                }
                (ChangeTag::Insert, s) => new_title.push_str(&error_markup_escape(s)),
                (ChangeTag::Delete, s) => old_title.push_str(&error_markup_escape(s)),
            }
        }
        match (self.length, new.length) {
            (None, None) => (),
            (None, Some(length)) => new_title.push_str(&error_markup(&format_length(length))),
            (Some(length), None) => old_title.push_str(&error_markup(&format_length(length))),
            (Some(old_length), Some(new_length)) if lengths_approx_eq(old_length, new_length) => {
                old_title.push_str(&format_length(old_length));
                new_title.push_str(&format_length(new_length));
            }
            (Some(old_length), Some(new_length)) => {
                old_title.push_str(&error_markup(&format_length(old_length)));
                new_title.push_str(&error_markup(&format_length(new_length)));
            }
        }
        format!("{}\n{}", new_title, old_title)
    }
}

impl AlbumOrTrack for Track {
    fn uri(&self) -> Option<&Uri> {
        self.uri.as_ref()
    }

    fn formatted_title(&self) -> String {
        let mut title = String::new();
        if let Some(artist) = &self.artist {
            title.push_str(&glib::markup_escape_text(artist));
            title.push_str(&separator());
        }
        if let Some(track) = self.track {
            title.push_str(&format_track(track));
        }
        title.push_str(&glib::markup_escape_text(&self.title));
        if let Some(length) = self.length {
            title.push_str(&format_length(length));
        }
        title
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct Album {
    tracks: Vec<Track>,
    year: Option<u32>,
    country: Option<String>,
    artist: Option<String>,
    title: String,
    uri: Option<Uri>,
}

impl Album {
    fn from_album(album: &Bound<PyAny>) -> PyResult<Self> {
        let items = album.call_method0("items")?;
        let mut tracks = Vec::new();
        for item in items.iter()? {
            tracks.push(Track::from_item(&item?)?);
        }
        Ok(Self {
            tracks,
            year: album.getattr("year")?.extract()?,
            country: album.getattr("country")?.extract()?,
            artist: album.getattr("albumartist")?.extract()?,
            title: album.getattr("album")?.extract()?,
            uri: None,
        })
    }

    fn as_import_item(&self) -> ImportItem {
        ImportItem::Multiple {
            description: ImportItemDescription {
                title: self.formatted_title(),
                subtitle: self.uri.as_ref().map(Uri::formatted_subtitle),
            },
            children: self
                .tracks
                .iter()
                .map(|track| ImportItemDescription {
                    title: track.formatted_title(),
                    subtitle: track.uri.as_ref().map(Uri::formatted_subtitle),
                })
                .collect(),
        }
    }
}

impl AlbumOrTrack for Album {
    fn uri(&self) -> Option<&Uri> {
        self.uri.as_ref()
    }

    fn formatted_title(&self) -> String {
        let mut title = String::new();
        if let Some(artist) = &self.artist {
            title.push_str(&glib::markup_escape_text(artist));
            title.push_str(&separator());
        }
        title.push_str(&glib::markup_escape_text(&self.title));
        match (self.year, &self.country) {
            (Some(year), Some(country)) => title.push_str(&fgettext(
                // Translators: This is a string that appears after an album name containing the year that the album was released in and the code of the country where it was released. For example, "Album name (2018, FR)". Do not translate the contents between '{' and '}', this is a variable name
                " ({year}, {country})",
                &[
                    ("year", &year.to_string()),
                    ("country", &glib::markup_escape_text(country)),
                ],
            )),
            (Some(year), None) => title.push_str(&fgettext(
                // Translators: This is a string that appears after an album name containing the year that the album was released in. For example, "Album name (2018)". Do not translate the contents between '{' and '}', this is a variable name
                " ({year})",
                &[("year", &year.to_string())],
            )),
            (None, Some(country)) => title.push_str(&fgettext(
                // Translators: This is a string that appears after an album name containing the code of the country where it was released. For example, "Album name (FR)". Do not translate the contents between '{' and '}', this is a variable name
                " ({country})",
                &[("country", &glib::markup_escape_text(country))],
            )),
            (None, None) => (),
        }
        title
    }
}

trait Candidate {
    fn album_or_track(&self) -> &impl AlbumOrTrack;
    fn distance(&self) -> f64;

    fn formatted_subtitle(&self) -> String {
        let mut subtitle = fgettext(
            // Translators: This string refers to the similarity between two albums or tracks. For example, "Similarity: 98.3%". Do not translate the contents between '{' and '}', this is a variable name
            "Similarity: {percentage}%",
            &[(
                "percentage",
                &format!("{:.1}", (1.0 - self.distance()) * 100.0,),
            )],
        );
        if let Some(uri) = &self.album_or_track().uri() {
            subtitle.push('\n');
            subtitle.push_str(&uri.formatted_subtitle());
        }
        subtitle
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct AlbumCandidate {
    album: Album,
    mapped_tracks: Vec<Option<Track>>,
    missing_tracks: Vec<Track>,
    distance: f64,
}

impl AlbumCandidate {
    fn from_album_match(album_match: &Bound<PyAny>) -> PyResult<Self> {
        let info = album_match.getattr("info")?;
        let mapping = album_match
            .getattr("mapping")?
            .downcast::<PyDict>()?
            .iter()
            .map(|(key, value)| -> PyResult<(Track, Option<Track>)> {
                Ok((
                    Track::from_track_info(&value)?,
                    Some(Track::from_item(&key)?),
                ))
            });
        let extra_tracks = album_match
            .getattr("extra_tracks")?
            .downcast::<PyList>()?
            .iter()
            .map(|item| Ok((Track::from_track_info(&item)?, None)));
        let mut track_mappings = Vec::new();
        for mapping in mapping.chain(extra_tracks) {
            track_mappings.push(mapping?);
        }
        track_mappings.sort_unstable_by_key(|(track, _)| track.track);
        let (tracks, mapped_tracks) = track_mappings.into_iter().unzip();
        let mut missing_tracks = Vec::new();
        for item in album_match
            .getattr("extra_items")?
            .downcast::<PyList>()?
            .iter()
        {
            missing_tracks.push(Track::from_item(&item)?);
        }
        Ok(Self {
            distance: album_match
                .getattr("distance")?
                .getattr("distance")?
                .extract()?,
            missing_tracks,
            album: Album {
                tracks,
                year: info.getattr("year")?.extract()?,
                country: info.getattr("country")?.extract()?,
                artist: info.getattr("artist")?.extract()?,
                title: info.getattr("album")?.extract()?,
                uri: if let Some(data_url) =
                    info.getattr("data_url")?.extract::<Option<String>>()?
                {
                    info.getattr("album_id")?
                        .extract::<Option<String>>()?
                        .map(|album_id| Uri {
                            href: data_url,
                            display: album_id,
                            uri_type: UriType::Web,
                        })
                } else {
                    None
                },
            },
            mapped_tracks,
        })
    }
}

impl Candidate for AlbumCandidate {
    fn album_or_track(&self) -> &impl AlbumOrTrack {
        &self.album
    }

    fn distance(&self) -> f64 {
        self.distance
    }
}

#[derive(Debug, Serialize, Deserialize)]
struct TrackCandidate {
    track: Track,
    distance: f64,
}

impl TrackCandidate {
    fn from_track_match(track_match: &Bound<PyAny>) -> PyResult<Self> {
        Ok(Self {
            track: Track::from_track_info(&track_match.getattr("info")?)?,
            distance: track_match
                .getattr("distance")?
                .getattr("distance")?
                .extract()?,
        })
    }
}

impl Candidate for TrackCandidate {
    fn album_or_track(&self) -> &impl AlbumOrTrack {
        &self.track
    }

    fn distance(&self) -> f64 {
        self.distance
    }
}

fn error_markup_escape(s: &str) -> String {
    error_markup(&glib::markup_escape_text(s))
}

fn error_markup(s: &str) -> String {
    format!("<span color=\"{LIGHT_ERROR_COLOR}\">{s}</span>")
}

fn bold_markup(s: &str) -> String {
    format!("<b>{s}</b>")
}

fn format_track(track: u32) -> String {
    fgettext(
        // Translators: This is a string that shows the track number of a music track. The number will appear before the title of a music track. For example, "7. Title". Do not translate the contents between '{' and '}', this is a variable name
        "{track}. ",
        &[("track", &track.to_string())],
    )
}

fn format_length(length: f64) -> String {
    fgettext(
        // Translators: This is a string that appears after the tile of a track that contains the track's duration. For example, "Title (3:12)". Do not translate the contents between '{' and '}', this is a variable name
        " ({duration})",
        &[(
            "duration",
            &format_clock_time(ClockTime::from_seconds_f64(length)),
        )],
    )
}

fn separator() -> String {
    gettext(
        // Translators: This is a string to separate an artist name from the title of an album or track. For example, "Artist name - album name"
        " — ",
    )
}

fn lengths_approx_eq(a: f64, b: f64) -> bool {
    ClockTime::from_seconds_f64(a).absdiff(ClockTime::from_seconds_f64(b))
        < ClockTime::from_seconds(5)
}

pyo3::create_exception!(encore, BrokenPipeException, PyException);
pyo3::create_exception!(encore, MutexException, PyException);
pyo3::create_exception!(encore, UnexpectedMessageException, PyException);

#[pymodule]
fn encore(m: &Bound<'_, PyModule>) -> PyResult<()> {
    m.add_class::<ImportSession>()?;
    Ok(())
}

#[pyclass(module = "encore", subclass)]
#[derive(Default)]
struct ImportSession {
    sender: OnceCell<IpcSender<ImportSessionMessage>>,
    receiver: OnceCell<Mutex<IpcReceiver<ImportDialogMessage>>>,
}

#[pymethods]
impl ImportSession {
    #[new]
    fn new(
        _lib: PyObject,
        _loghandler: PyObject,
        _paths: PyObject,
        _query: PyObject,
    ) -> Result<Self, PyErr> {
        Ok(Default::default())
    }

    fn choose_item<'a>(
        &self,
        py: Python<'a>,
        task: &Bound<'a, PyAny>,
    ) -> PyResult<Bound<'a, PyAny>> {
        loop {
            let py_candidates = task.getattr("candidates")?;
            let py_candidates = py_candidates.downcast::<PyList>()?;
            let mut candidates = Vec::new();
            let item = task.getattr("item")?;
            let old = Track::from_item(&item)?;
            for candidate in py_candidates.iter() {
                let candidate = TrackCandidate::from_track_match(&candidate)?;
                candidates.push(ImportItem::Single {
                    description: ImportItemDescription {
                        title: old.formatted_difference_title(&candidate.track),
                        subtitle: Some(candidate.formatted_subtitle()),
                    },
                });
            }
            self.send(ImportSessionMessage::Import {
                item: old.as_import_item(),
                candidates,
                can_group: false,
            })?;
            match self.receive(py)? {
                ImportDialogMessage::Choice(ImportDialogChoice::Candidate(i)) => {
                    return py_candidates.get_item(i as usize);
                }
                ImportDialogMessage::Choice(ImportDialogChoice::ImportAsIs) => {
                    return py
                        .import_bound("beets.importer")?
                        .getattr("action")?
                        .getattr("ASIS");
                }
                ImportDialogMessage::Choice(ImportDialogChoice::Skip) => {
                    return py
                        .import_bound("beets.importer")?
                        .getattr("action")?
                        .getattr("SKIP");
                }
                ImportDialogMessage::Choice(ImportDialogChoice::SearchById(id)) => {
                    let new_candidates = py
                        .import_bound("beets")?
                        .getattr("autotag")?
                        .call_method1("tag_item", (item, None::<PyObject>, None::<PyObject>, [id]))?
                        .getattr("candidates")?;
                    task.setattr("candidates", new_candidates)?;
                }
                ImportDialogMessage::Choice(ImportDialogChoice::SearchByTitle {
                    artist,
                    title,
                }) => {
                    let new_candidates = py
                        .import_bound("beets")?
                        .getattr("autotag")?
                        .call_method1("tag_item", (item, artist, title))?
                        .getattr("candidates")?;
                    task.setattr("candidates", new_candidates)?;
                }
                ImportDialogMessage::Abort => {
                    return self.abort(py);
                }
                unexpected => {
                    return self.unexpected(unexpected);
                }
            }
        }
    }

    fn choose_match<'a>(
        &self,
        py: Python<'a>,
        task: Bound<'a, PyAny>,
    ) -> PyResult<Bound<'a, PyAny>> {
        loop {
            let py_candidates = task.getattr("candidates")?;
            let py_candidates = py_candidates.downcast::<PyList>()?;
            let mut candidates = Vec::new();
            for candidate in py_candidates.iter() {
                let candidate = AlbumCandidate::from_album_match(&candidate)?;
                let mut children = Vec::new();
                for (i, old_track) in candidate.mapped_tracks.iter().enumerate() {
                    let new_track = &candidate.album.tracks[i];
                    children.push(if let Some(old_track) = old_track {
                        ImportItemDescription {
                            title: old_track.formatted_difference_title(new_track),
                            subtitle: new_track.uri.as_ref().map(Uri::formatted_subtitle),
                        }
                    } else {
                        ImportItemDescription {
                            title: format!(
                                "{}{}",
                                bold_markup(&gettext(
                                    // Translators: This is a string that appearts before a track title that shows that some track should not be part of an album. For example, "Extra: Track title"
                                    "Extra: ",
                                )),
                                error_markup(&new_track.formatted_title()),
                            ),
                            subtitle: new_track.uri.as_ref().map(Uri::formatted_subtitle),
                        }
                    });
                }
                for item in &candidate.missing_tracks {
                    children.push(ImportItemDescription {
                        title: format!(
                            "{}{}",
                            bold_markup(&gettext(
                                // Translators: This is a string that appearts before a track title that shows that some track is missing from an album. For example, "Missing: Track title"
                                "Missing: ",
                            )),
                            error_markup(&item.formatted_title()),
                        ),
                        subtitle: None,
                    });
                }
                candidates.push(ImportItem::Multiple {
                    description: ImportItemDescription {
                        title: candidate.album.formatted_title(),
                        subtitle: Some(candidate.formatted_subtitle()),
                    },
                    children,
                });
            }
            let py_items = task.getattr("items")?;
            let mut items = Vec::new();
            for item in py_items.downcast::<PyList>()?.iter() {
                items.push(Track::from_item(&item)?.as_import_item_description());
            }
            self.send(ImportSessionMessage::Import {
                item: ImportItem::Multiple {
                    description: ImportItemDescription {
                        title: format!("{} new tracks", items.len()),
                        subtitle: None,
                    },
                    children: items,
                },
                candidates,
                can_group: true,
            })?;
            match self.receive(py)? {
                ImportDialogMessage::Choice(ImportDialogChoice::Candidate(i)) => {
                    return py_candidates.get_item(i as usize);
                }
                ImportDialogMessage::Choice(ImportDialogChoice::ImportAsIs) => {
                    return py
                        .import_bound("beets.importer")?
                        .getattr("action")?
                        .getattr("ASIS");
                }
                ImportDialogMessage::Choice(ImportDialogChoice::Skip) => {
                    return py
                        .import_bound("beets.importer")?
                        .getattr("action")?
                        .getattr("SKIP");
                }
                ImportDialogMessage::Choice(ImportDialogChoice::SearchById(id)) => {
                    let new_candidates = py
                        .import_bound("beets")?
                        .getattr("autotag")?
                        .call_method1(
                            "tag_album",
                            (py_items, None::<PyObject>, None::<PyObject>, [id]),
                        )?
                        .get_item(2)?
                        .getattr("candidates")?;
                    task.setattr("candidates", new_candidates)?;
                }
                ImportDialogMessage::Choice(ImportDialogChoice::SearchByTitle {
                    artist,
                    title,
                }) => {
                    let new_candidates = py
                        .import_bound("beets")?
                        .getattr("autotag")?
                        .call_method1("tag_album", (py_items, artist, title))?
                        .get_item(2)?
                        .getattr("candidates")?;
                    task.setattr("candidates", new_candidates)?;
                }
                ImportDialogMessage::Choice(ImportDialogChoice::AsTracks) => {
                    return py
                        .import_bound("beets.importer")?
                        .getattr("action")?
                        .getattr("TRACKS");
                }
                ImportDialogMessage::Choice(ImportDialogChoice::Group) => {
                    return py
                        .import_bound("beets.importer")?
                        .getattr("action")?
                        .getattr("ALBUMS");
                }
                ImportDialogMessage::Abort => {
                    return self.abort(py);
                }
                unexpected => {
                    return self.unexpected(unexpected);
                }
            }
        }
    }

    fn resolve_duplicate<'a>(
        &self,
        py: Python<'a>,
        task: &Bound<'a, PyAny>,
        found_duplicates: Bound<'a, PyAny>,
    ) -> PyResult<()> {
        let is_album = task.getattr("is_album")?.extract::<bool>()?;
        if is_album {
            let mut old = Vec::new();
            for item in found_duplicates.downcast::<PyList>()?.iter() {
                old.push(Album::from_album(&item)?.as_import_item());
            }
            let mut new = Vec::new();
            for item in task.call_method0("imported_items")?.iter()? {
                new.push(Track::from_item(&item?)?.as_import_item_description());
            }
            self.send(ImportSessionMessage::ResolveConflict {
                old,
                new: ImportItem::Multiple {
                    description: ImportItemDescription {
                        title: format!("{} new tracks", new.len()),
                        subtitle: None,
                    },
                    children: new,
                },
            })?;
        } else {
            let mut old = Vec::new();
            for item in found_duplicates.downcast::<PyList>()?.iter() {
                old.push(Track::from_item(&item)?.as_import_item());
            }
            self.send(ImportSessionMessage::ResolveConflict {
                old,
                new: Track::from_item(
                    &task
                        .call_method0("imported_items")?
                        .iter()?
                        .next()
                        .ok_or_else(|| PyValueError::new_err("imported_items has no items"))??,
                )?
                .as_import_item(),
            })?;
        }
        match self.receive(py)? {
            ImportDialogMessage::Choice(ImportDialogChoice::Skip) => {
                task.call_method1(
                    "set_choice",
                    (py.import_bound("beets.importer")?
                        .getattr("action")?
                        .getattr("SKIP")?,),
                )?;
            }
            ImportDialogMessage::Choice(ImportDialogChoice::Keep) => (),
            ImportDialogMessage::Choice(ImportDialogChoice::Remove) => {
                task.setattr("should_remove_duplicates", true)?;
            }
            ImportDialogMessage::Choice(ImportDialogChoice::Merge) => {
                task.setattr("should_merge_duplicates", true)?;
            }
            ImportDialogMessage::Abort => {
                return self.abort(py);
            }
            unexpected => {
                return self.unexpected(unexpected);
            }
        };
        Ok(())
    }

    fn should_resume(&self, py: Python, path: &[u8]) -> PyResult<bool> {
        self.send(ImportSessionMessage::Resume {
            path: Path::new(OsStr::from_bytes(path)).to_owned(),
        })?;
        match self.receive(py)? {
            ImportDialogMessage::Resume => Ok(true),
            ImportDialogMessage::NoResume => Ok(false),
            ImportDialogMessage::Abort => self.abort(py),
            unexpected => self.unexpected(unexpected),
        }
    }
}

impl ImportSession {
    fn send(&self, message: ImportSessionMessage) -> PyResult<()> {
        self.sender
            .get()
            .ok_or_else(|| PyValueError::new_err("ImportSession sender not initialized"))?
            .send(message)
            .map_err(|e| BrokenPipeException::new_err(e.to_string()))
    }

    fn receive(&self, py: Python) -> PyResult<ImportDialogMessage> {
        let receiver = self
            .receiver
            .get()
            .ok_or_else(|| PyValueError::new_err("ImportSession receiver not initialized"))?;
        py.allow_threads(|| {
            receiver
                .lock()
                .map_err(|e| MutexException::new_err(e.to_string()))
                .and_then(|r| {
                    r.recv()
                        .map_err(|e| BrokenPipeException::new_err(e.to_string()))
                })
        })
    }

    fn abort<T>(&self, py: Python) -> PyResult<T> {
        Err(PyErr::from_type_bound(
            py.import_bound("beets.importer")?
                .getattr("ImportAbortError")?
                .downcast::<PyType>()?
                .clone(),
            (),
        ))
    }

    fn unexpected<T>(&self, message: ImportDialogMessage) -> PyResult<T> {
        Err(UnexpectedMessageException::new_err(format!("{message:?}")))
    }
}

pub fn importer(
    server_name: String,
    library_path: PathBuf,
    import_path: PathBuf,
    move_files: bool,
    autotag: bool,
) -> Result<(), SqliteOrPythonError> {
    use SqliteOrPythonError::{PythonError, SqliteError};
    let (dialog_message_server, dialog_message_server_name) =
        IpcOneShotServer::<ImportDialogMessage>::new()
            .map_err(|e| PyValueError::new_err(e.to_string()))
            .map_err(PythonError)?;
    let sender = IpcSender::connect(server_name)
        .map_err(|e| PyValueError::new_err(e.to_string()))
        .map_err(PythonError)?;
    sender
        .send(ImportSessionMessage::StartedServer {
            name: dialog_message_server_name,
        })
        .map_err(|e| PyValueError::new_err(e.to_string()))
        .map_err(PythonError)?;
    let (receiver, message) = dialog_message_server
        .accept()
        .map_err(|e| PyValueError::new_err(e.to_string()))
        .map_err(PythonError)?;
    match message {
        ImportDialogMessage::StartedServer => (),
        unexpected => {
            return Err(PythonError(PyValueError::new_err(format!(
                "Unexpected message: {unexpected:?}"
            ))));
        }
    }
    Python::with_gil(|py| {
        let library = new_library(&library_path, py).map_err(PythonError)?;
        let import_config = py
            .import_bound("beets")
            .map_err(PythonError)?
            .getattr("config")
            .map_err(PythonError)?
            .get_item("import")
            .map_err(PythonError)?;
        import_config
            .set_item("move", move_files)
            .map_err(PythonError)?;
        import_config
            .set_item("autotag", autotag)
            .map_err(PythonError)?;
        let handler = py
            .import_bound("logging")
            .map_err(PythonError)?
            .getattr("StreamHandler")
            .map_err(PythonError)?
            .call0()
            .map_err(PythonError)?;
        handler
            .call_method1(
                "setLevel",
                (py.import_bound("logging")
                    .map_err(PythonError)?
                    .getattr("DEBUG")
                    .map_err(PythonError)?,),
            )
            .map_err(PythonError)?;
        let session_py = py
            .import_bound("builtins")
            .map_err(PythonError)?
            .getattr("type")
            .map_err(PythonError)?
            .call1((
                "EncoreSession",
                (
                    wrap_pymodule!(encore)(py)
                        .getattr(py, "ImportSession")
                        .map_err(PythonError)?,
                    py.import_bound("beets.importer")
                        .map_err(PythonError)?
                        .getattr("ImportSession")
                        .map_err(PythonError)?,
                ),
                PyDict::new_bound(py),
            ))
            .map_err(PythonError)?
            .call1((library, handler, [import_path.into_os_string()], py.None()))
            .map_err(PythonError)?;
        {
            let session = session_py
                .downcast::<ImportSession>()
                .map_err(|e| PythonError(e.into()))?
                .borrow();
            session
                .sender
                .set(sender)
                .map_err(|_| PyValueError::new_err("ImportSession sender already set"))
                .map_err(PythonError)?;
            session
                .receiver
                .set(Mutex::new(receiver))
                .map_err(|_| PyValueError::new_err("ImportSession receiver already set"))
                .map_err(PythonError)?;
        }
        let import_failure = if let Err(e) = session_py.call_method0("run") {
            let music_brainz_exception = py
                .import_bound("beets.autotag.mb")
                .map_err(PythonError)?
                .getattr("MusicBrainzAPIError")
                .map_err(PythonError)?;
            if e.is_instance_bound(py, &music_brainz_exception) {
                info!("MusicBrainz is not reachable");
                Some(ImportFailure::MusicBrainzNotReachable)
            } else {
                warn!("Unknown import error");
                e.display(py);
                Some(ImportFailure::Unknown)
            }
        } else {
            None
        };
        {
            recreate_database().map_err(SqliteError)?;
            let session = session_py
                .downcast_into::<ImportSession>()
                .map_err(|e| PythonError(e.into()))?
                .borrow();
            session
                .sender
                .get()
                .ok_or_else(|| PyValueError::new_err("ImportSession sender not initialized"))
                .map_err(PythonError)?
                .send(ImportSessionMessage::Finished(import_failure))
                .map_err(|e| BrokenPipeException::new_err(e.to_string()))
                .map_err(PythonError)?;
        }
        debug!("Finished import session");
        Ok::<(), SqliteOrPythonError>(())
    })
}
