use {
    gdk_pixbuf::Pixbuf,
    gio::{SubprocessFlags, SubprocessLauncher},
    pyo3::{exceptions::PyValueError, prelude::*, types::IntoPyDict},
    rusqlite::{
        functions::{Aggregate, FunctionFlags, WindowAggregate},
        Connection, Error,
    },
    std::{
        fmt::Display,
        fs,
        path::{Path, PathBuf},
    },
    tracing::error,
};

#[derive(Debug)]
pub enum SqliteOrPythonError {
    SqliteError(Error),
    PythonError(PyErr),
}

impl Display for SqliteOrPythonError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            SqliteOrPythonError::SqliteError(error) => error.fmt(f),
            SqliteOrPythonError::PythonError(py_err) => py_err.fmt(f),
        }
    }
}

// When the library is empty a UserError is thrown, this function gets rid of them
fn with_gil_without_user_error(f: impl FnOnce(Python) -> Result<(), PyErr>) -> Result<(), PyErr> {
    Python::with_gil(|py| match f(py) {
        Ok(_) => Ok(()),
        Err(e) => {
            if e.is_instance_bound(py, &py.import_bound("beets.ui")?.getattr("UserError")?) {
                Ok(())
            } else {
                Err(e)
            }
        }
    })
}

pub fn update_library(library_path: &Path) -> Result<(), SqliteOrPythonError> {
    with_gil_without_user_error(|py| {
        let library = new_library(library_path, py)?;
        let commands_module = py.import_bound("beets.ui.commands")?;
        commands_module.getattr("update_items")?.call1((
            &library,
            py.None(), // query
            py.None(), // album
            false,     // move
            false,     // pretend
            py.None(), // fields
        ))?;
        commands_module.getattr("move_items")?.call1((
            &library,
            py.None(), // dest
            py.None(), // query
            py.None(), // album
            false,     // copy
            false,     // pretend
            false,     // confirm
            false,     // export
        ))?;
        Ok::<(), PyErr>(())
    })
    .map_err(SqliteOrPythonError::PythonError)?;
    recreate_database().map_err(SqliteOrPythonError::SqliteError)
}

pub fn clear_library(library_path: &Path) -> Result<(), SqliteOrPythonError> {
    with_gil_without_user_error(|py| {
        let library = new_library(library_path, py)?;
        let commands_module = py.import_bound("beets.ui.commands")?;
        commands_module.getattr("remove_items")?.call1((
            &library,
            py.None(), // query
            py.None(), // album
            false,     // delete
            true,      // force
        ))?;
        Ok::<(), PyErr>(())
    })
    .map_err(SqliteOrPythonError::PythonError)?;
    recreate_database().map_err(SqliteOrPythonError::SqliteError)
}

fn beets_path() -> PathBuf {
    library_directory_path().join("beets")
}

fn beets_db_path() -> PathBuf {
    beets_path().join("library.db")
}

fn encore_db_path() -> PathBuf {
    library_directory_path().join("encore.db")
}

pub fn subprocess_launcher() -> SubprocessLauncher {
    let launcher = SubprocessLauncher::new(SubprocessFlags::empty());
    launcher.setenv("BEETSDIR", beets_path(), true);
    launcher
}

pub(super) fn new_library<'a>(
    library_path: &Path,
    py: Python<'a>,
) -> Result<Bound<'a, PyAny>, PyErr> {
    let db_path = beets_db_path();
    if let Some(parent) = db_path.parent() {
        if let Err(e) = fs::exists(parent).and_then(|exists| {
            if !exists {
                fs::create_dir_all(parent)?;
            }
            Ok(())
        }) {
            error!("Failed to create beets directory: {e}");
        }
    }

    let ui_module = py.import_bound("beets.ui")?;

    let path_str = db_path
        .as_os_str()
        .to_str()
        .ok_or_else(|| PyValueError::new_err("Failed converting library path to string"))?;

    let music_dir_path_str = library_path
        .as_os_str()
        .to_str()
        .ok_or_else(|| PyValueError::new_err("Failed converting Music directory path to string"))?;

    let plugins_module = py.import_bound("beets.plugins")?;

    let import_config = py.import_bound("beets")?.getattr("config")?;
    import_config.get_item("import")?.set_item("timid", true)?;
    import_config
        .get_item("replaygain")?
        .set_item("backend", "ffmpeg")?;
    plugins_module
        .getattr("load_plugins")?
        .call1((["fetchart"],))?;

    let library_module = py.import_bound("beets.library")?;
    let library = library_module.getattr("Library")?.call1((
        path_str,
        music_dir_path_str,
        ui_module.getattr("get_path_formats")?.call0()?,
        ui_module.getattr("get_replacements")?.call0()?,
    ))?;

    plugins_module.getattr("send")?.call(
        ("library_opened,",),
        Some(&[("lib", &library)].into_py_dict_bound(py)),
    )?;

    Ok(library)
}

pub(super) fn library_directory_path() -> PathBuf {
    let mut buf = PathBuf::new();
    buf.push(glib::user_data_dir());
    buf.push("encore");
    buf
}

pub fn create_connection() -> Result<Connection, Error> {
    let db_path = encore_db_path();
    if let Some(parent) = db_path.parent() {
        if let Err(e) = fs::exists(parent).and_then(|exists| {
            if !exists {
                fs::create_dir_all(parent)?;
            }
            Ok(())
        }) {
            error!("Failed to create data directory: {e}");
        }
    }
    let db_exists = fs::exists(&db_path).unwrap_or_else(|e| {
        error!("Failed to check if database exists: {e}");
        false
    });
    let mut conn = Connection::open(&db_path)?;
    conn.execute("PRAGMA foreign_keys = ON", ())?;
    conn.set_prepared_statement_cache_capacity(50);
    struct PositionAfterMoveDown;
    struct Accumulator {
        selected_before: u32,
        current_selected: bool,
    }
    impl Aggregate<Accumulator, i32> for PositionAfterMoveDown {
        fn init(&self, _: &mut rusqlite::functions::Context<'_>) -> rusqlite::Result<Accumulator> {
            Ok(Accumulator {
                selected_before: 0,
                current_selected: false,
            })
        }

        fn step(
            &self,
            ctx: &mut rusqlite::functions::Context<'_>,
            acc: &mut Accumulator,
        ) -> rusqlite::Result<()> {
            if acc.current_selected {
                acc.selected_before += 1;
            } else {
                acc.selected_before = 0;
            }
            acc.current_selected = ctx.get::<bool>(0)?;
            Ok(())
        }

        fn finalize(
            &self,
            _: &mut rusqlite::functions::Context<'_>,
            mut acc: Option<Accumulator>,
        ) -> rusqlite::Result<i32> {
            self.value(acc.as_mut())
        }
    }

    impl WindowAggregate<Accumulator, i32> for PositionAfterMoveDown {
        fn value(&self, acc: Option<&mut Accumulator>) -> rusqlite::Result<i32> {
            let Some(acc) = acc else {
                return Ok(0);
            };
            if acc.current_selected {
                Ok(1)
            } else {
                Ok(-(acc.selected_before as i32))
            }
        }

        fn inverse(
            &self,
            ctx: &mut rusqlite::functions::Context<'_>,
            acc: &mut Accumulator,
        ) -> rusqlite::Result<()> {
            if ctx.get::<bool>(0)? {
                acc.selected_before -= 1;
            }
            Ok(())
        }
    }
    conn.create_window_function(
        "position_after_move_down",
        1,
        FunctionFlags::SQLITE_DETERMINISTIC,
        PositionAfterMoveDown,
    )?;
    if !db_exists {
        conn.transaction()?
            .execute_batch(include_str!("schema.sql"))?;
    }
    Ok(conn)
}

pub fn recreate_database() -> Result<(), Error> {
    let mut conn = Connection::open(encore_db_path())?;
    conn.execute("PRAGMA foreign_keys = ON", ())?;
    conn.create_scalar_function(
        "beets_database_path",
        0,
        FunctionFlags::SQLITE_DETERMINISTIC,
        |_| {
            String::from_utf8(beets_db_path().into_os_string().into_encoded_bytes())
                .map(|s| s.to_owned())
                .map_err(|e| Error::Utf8Error(e.utf8_error()))
        },
    )?;
    let transaction = conn.transaction()?;
    transaction.execute_batch(include_str!("populate.sql"))?;
    {
        let mut stmt = transaction
            .prepare("SELECT position, art_path FROM albums WHERE art_path IS NOT NULL")?;
        let mut rows = stmt.query(())?;
        while let Some(row) = rows.next()? {
            let position = row.get::<_, u32>(0)?;
            let path = row.get(1)?;
            let path = String::from_utf8(path).map_err(|e| Error::Utf8Error(e.utf8_error()))?;
            let path = Path::new(&path);
            let art = Pixbuf::from_file_at_scale(path, 160, 160, true)
                .map_err(|e| error!("Failed loading pixbuf: {e}"))
                .ok()
                .and_then(|pixbuf| {
                    pixbuf
                        .save_to_bufferv("jxl", &[])
                        .map_err(|e| error!("Failed getting buffer out of pixbuf: {e}"))
                        .ok()
                });
            transaction.execute(
                "UPDATE albums SET art = ? WHERE position = ?",
                (&art, position),
            )?;
        }
    }
    transaction.commit()
}
