ATTACH DATABASE beets_database_path() as beets;

DELETE FROM artists;
INSERT INTO artists
SELECT
    row_number() OVER (
        ORDER BY
            albums.albumartist_sort,
            albums.mb_albumartistid
            NULLS FIRST
    ) - 1 AS position,
    albums.albumartist AS title
FROM beets.albums
GROUP BY
    albums.albumartist_sort,
    albums.mb_albumartistid;

DELETE FROM albums;
INSERT INTO albums
SELECT
    row_number() OVER (
        ORDER BY
            albums.albumartist_sort,
            albums.mb_albumartistid,
            albums.year,
            albums.album,
            albums.id
            NULLS FIRST
    ) - 1 AS position,
    dense_rank() OVER (
        ORDER BY
            albums.albumartist_sort,
            albums.mb_albumartistid
            NULLS FIRST
    ) - 1 AS artist_position,
    albums.album AS title,
    albums.year,
    sum(items.length) AS duration,
    NULL AS art,
    albums.artpath AS art_path
FROM beets.items
LEFT JOIN beets.albums
    ON items.album_id = albums.id
GROUP BY
    albums.albumartist_sort,
    albums.mb_albumartistid,
    albums.year,
    albums.album,
    albums.id;

DELETE FROM discs;
INSERT INTO discs
SELECT
    row_number() OVER (
        ORDER BY
            albums.albumartist_sort,
            albums.mb_albumartistid,
            albums.year,
            albums.album,
            albums.id,
            items.disc
            NULLS FIRST
    ) - 1 AS position,
    coalesce(items.disc, 1) AS disc_number,
    NULLIF(items.disctitle, '') AS title
FROM beets.items
LEFT JOIN beets.albums
    ON items.album_id = albums.id
GROUP BY
    albums.albumartist_sort,
    albums.mb_albumartistid,
    albums.year,
    albums.album,
    albums.id,
    items.disc;

DELETE FROM tracks WHERE id NOT IN (SELECT id FROM beets.items);
DELETE FROM queue WHERE track_id NOT IN (SELECT id FROM beets.items);
INSERT INTO tracks
SELECT
    items.id,
    row_number() OVER (
        ORDER BY
            albums.albumartist_sort,
            albums.mb_albumartistid,
            albums.year,
            albums.album,
            albums.id,
            items.disc,
            items.track,
            items.id
            NULLS FIRST
    ) - 1 AS position,
    dense_rank() OVER (
        ORDER BY
            albums.albumartist_sort,
            albums.mb_albumartistid,
            albums.year,
            albums.album,
            albums.id
            NULLS FIRST
    ) - 1 AS album_position,
    dense_rank() OVER (
        ORDER BY
            albums.albumartist_sort,
            albums.mb_albumartistid,
            albums.year,
            albums.album,
            albums.id,
            items.disc
            NULLS FIRST
    ) - 1 AS disc_position,
    items.title,
    items.artist,
    items.path,
    items.track AS track_number,
    items.length AS duration
FROM beets.items
JOIN beets.albums
ON items.album_id = albums.id
ON CONFLICT(id) DO UPDATE SET
    position = excluded.position,
    album_position = excluded.album_position,
    title = excluded.title,
    artist = excluded.artist,
    path = excluded.path,
    track_number = excluded.track_number,
    duration = excluded.duration;

UPDATE queue
SET position = queue_with_new_position.new_position
FROM (
    SELECT position, row_number() OVER (ORDER BY position) - 1 AS new_position
    FROM queue
) AS queue_with_new_position
WHERE queue.position = queue_with_new_position.position;
