CREATE TABLE artists (
    position INTEGER PRIMARY KEY,
    title TEXT
) STRICT;

CREATE TABLE albums (
    position INTEGER PRIMARY KEY,
    artist_position INTEGER NOT NULL,
    title TEXT,
    year INTEGER,
    duration REAL,
    art BLOB,
    art_path BLOB,
    FOREIGN KEY (artist_position) REFERENCES artists (position)
    DEFERRABLE INITIALLY DEFERRED
) STRICT;

CREATE INDEX albums_artist_index ON albums (artist_position);

CREATE TABLE discs (
    position INTEGER PRIMARY KEY,
    disc_number INTEGER NOT NULL,
    title TEXT
) STRICT;

CREATE TABLE tracks (
    id INTEGER PRIMARY KEY,
    position INTEGER NOT NULL,
    album_position INTEGER NOT NULL,
    disc_position INTEGER NOT NULL,
    title TEXT,
    artist TEXT,
    path BLOB NOT NULL,
    track_number INTEGER,
    duration REAL,
    FOREIGN KEY (album_position) REFERENCES albums (position)
    DEFERRABLE INITIALLY DEFERRED
) STRICT;

CREATE INDEX tracks_position_index ON tracks (position);
CREATE INDEX tracks_album_index ON tracks (album_position);
CREATE INDEX tracks_disc_index ON tracks (disc_position);

CREATE TABLE queue (
    position INTEGER NOT NULL,
    track_id INTEGER NOT NULL,
    selected INTEGER NOT NULL,
    playing INTEGER NOT NULL,
    FOREIGN KEY (track_id) REFERENCES tracks (id)
    DEFERRABLE INITIALLY DEFERRED
) STRICT;

CREATE INDEX queue_position_index ON queue (position);

CREATE VIEW search_entries (
    kind, position, title, additional_info, album_art_position
) AS
SELECT
    0 AS kind,
    artists.position AS position,
    artists.title AS title,
    count() AS additional_info,
    NULL AS album_art_position
FROM artists
JOIN albums ON artists.position = albums.artist_position
GROUP BY artists.position
UNION ALL
SELECT
    1 AS kind,
    albums.position AS position,
    albums.title AS title,
    artists.title AS additional_info,
    albums.position AS album_art_position
FROM albums
JOIN artists ON albums.artist_position = artists.position
UNION ALL
SELECT
    2 AS kind,
    tracks.position AS position,
    tracks.title AS title,
    tracks.artist AS additional_info,
    albums.position AS album_art_position
FROM tracks
JOIN albums ON albums.position = tracks.album_position
ORDER BY kind, position;

COMMIT;
