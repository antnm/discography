use {
    gdk::Paintable,
    gettextrs::gettext,
    glib::{prelude::*, subclass::prelude::*, Object, Properties},
    std::{
        cell::{Cell, OnceCell},
        marker::PhantomData,
    },
    tracing::error,
};

mod imp {
    use {super::*, std::cell::RefCell};

    #[derive(Debug, Properties)]
    #[properties(wrapper_type = super::SearchItem)]
    pub struct SearchItem {
        #[property(get)]
        title: OnceCell<String>,
        #[property(get)]
        additional_info: OnceCell<String>,
        #[property(get = |this: &Self| match this.position.get() {
            SearchItemPosition::Artist(_) => gettext("Artists"),
            SearchItemPosition::Album(_) => gettext("Albums"),
            SearchItemPosition::Track(_) => gettext("Tracks"),
        })]
        section_title: PhantomData<String>,
        #[property(get, set)]
        art: RefCell<Option<Paintable>>,
        position: Cell<SearchItemPosition>,
    }

    impl Default for SearchItem {
        fn default() -> Self {
            Self {
                title: Default::default(),
                additional_info: Default::default(),
                section_title: Default::default(),
                position: Cell::new(SearchItemPosition::Track(0)),
                art: Default::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SearchItem {
        const NAME: &'static str = "SearchItem";
        type Type = super::SearchItem;
    }

    #[glib::derived_properties]
    impl ObjectImpl for SearchItem {}

    impl super::SearchItem {
        pub fn new(
            position: SearchItemPosition,
            title: String,
            additional_info: String,
            art: Paintable,
        ) -> Self {
            let obj = Object::new::<Self>();
            let imp = obj.imp();
            imp.position.set(position);
            if imp.title.set(title).is_err() {
                error!("Failed setting search item title");
            }
            if imp.additional_info.set(additional_info).is_err() {
                error!("Failed setting search item additional info");
            }
            imp.art.replace(Some(art));
            obj
        }

        pub fn position(&self) -> SearchItemPosition {
            self.imp().position.get()
        }
    }
}

glib::wrapper! {
    pub struct SearchItem(ObjectSubclass<imp::SearchItem>);
}

#[derive(Clone, Copy, Debug)]
pub enum SearchItemPosition {
    Track(u32),
    Album(u32),
    Artist(u32),
}
