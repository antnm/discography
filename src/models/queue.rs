use {
    super::{queued_track, TrackStore},
    core::str,
    gio::{prelude::*, subclass::prelude::*, ListModel},
    glib::{subclass::Signal, Object, Properties, Type, WeakRef},
    queued_track::QueuedTrack,
    rusqlite::{Connection, Error, OptionalExtension},
    std::{
        cell::{Cell, OnceCell, RefCell},
        marker::PhantomData,
        rc::Rc,
        sync::OnceLock,
    },
    tracing::error,
};

mod imp {
    use super::*;

    #[derive(Debug, Default, Properties)]
    #[properties(wrapper_type = super::Queue)]
    pub struct Queue {
        connection: OnceCell<Rc<Connection>>,
        track_store: OnceCell<WeakRef<TrackStore>>,
        items: RefCell<Vec<WeakRef<QueuedTrack>>>,
        #[property(get = |this: &Self| this.items.borrow().len() as u32)]
        n_items: PhantomData<u32>,
        #[property(get = Self::playing_track)]
        playing_track: PhantomData<Option<QueuedTrack>>,
        #[property(get, set = Self::set_playing_track_position)]
        playing_track_position: Cell<u32>,
        #[property(get = Self::can_move_up)]
        can_move_up: PhantomData<bool>,
        #[property(get = Self::can_move_down)]
        can_move_down: PhantomData<bool>,
        #[property(get = Self::any_selected)]
        any_selected: PhantomData<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Queue {
        const NAME: &'static str = "Queue";
        type Type = super::Queue;
        type Interfaces = (gio::ListModel,);
    }

    #[glib::derived_properties]
    impl ObjectImpl for Queue {
        fn signals() -> &'static [Signal] {
            static SIGNALS: OnceLock<Vec<Signal>> = OnceLock::new();
            SIGNALS.get_or_init(|| vec![Signal::builder("playback-changed").build()])
        }
    }

    impl ListModelImpl for Queue {
        fn item_type(&self) -> Type {
            QueuedTrack::static_type()
        }

        fn n_items(&self) -> u32 {
            self.items.borrow().len() as u32
        }

        fn item(&self, position: u32) -> Option<Object> {
            if position >= self.items.borrow().len() as u32 {
                return None;
            }
            let items = self.items.borrow();
            let weak_ref = items.get(position as usize)?;
            if let Some(queued_track) = weak_ref.upgrade() {
                return Some(queued_track.upcast());
            }
            let Some(connection) = self.connection.get() else {
                error!("Queue connection not set");
                return None;
            };
            let Some(track_store) = self.track_store.get() else {
                error!("Queue track store not set");
                return None;
            };
            let queued_track = match connection
                .prepare_cached(
                    "
                    SELECT tracks.position, queue.selected, queue.playing
                    FROM queue
                    JOIN tracks
                    ON queue.track_id = tracks.id
                    WHERE queue.position = ?
                    ",
                )
                .and_then(|mut s| {
                    s.query_row((position,), |r| {
                        Ok(QueuedTrack::new(
                            track_store.clone(),
                            r.get(0)?,
                            r.get(1)?,
                            r.get(2)?,
                        ))
                    })
                }) {
                Err(e) => {
                    error!("Failed to select queued track: {e}");
                    return None;
                }
                Ok(q) => q,
            };
            weak_ref.set(Some(&queued_track));
            Some(queued_track.upcast())
        }
    }

    impl Queue {
        fn lookup_item_count(&self) -> u32 {
            let Some(connection) = self.connection.get() else {
                error!("Queue connection not set");
                return 0;
            };
            connection
                .prepare_cached("SELECT count(*) FROM queue")
                .and_then(|mut s| s.query_row((), |row| row.get(0)))
                .unwrap_or_else(|e| {
                    tracing::error!("Failed to get the number of queued tracks, {e}");
                    0
                })
        }

        fn lookup_current_playing_track(&self) -> u32 {
            let Some(connection) = self.connection.get() else {
                error!("Queue connection not set");
                return 0;
            };
            connection
                .prepare_cached("SELECT position FROM queue WHERE playing = TRUE")
                .and_then(|mut s| s.query_row((), |row| row.get(0)).optional())
                .unwrap_or_else(|e| {
                    tracing::error!("Failed to get the playing track: {e}");
                    None
                })
                .unwrap_or(gtk::INVALID_LIST_POSITION)
        }

        fn delete_queued_tracks(&self, connection: &Connection) -> Result<(), Error> {
            connection
                .prepare_cached("DELETE FROM queue")?
                .execute(())?;
            self.items.borrow_mut().clear();
            Ok(())
        }

        fn playing_track(&self) -> Option<QueuedTrack> {
            self.item(self.playing_track_position.get()).and_downcast()
        }

        fn set_playing_track_position(&self, position: u32) {
            let old_position = self.playing_track_position.replace(position);
            let Some(connection) = self.connection.get() else {
                error!("Queue connection not set");
                return;
            };
            if old_position != gtk::INVALID_LIST_POSITION {
                if let Err(e) = connection
                    .prepare_cached("UPDATE queue SET playing = FALSE WHERE position = ?")
                    .and_then(|mut s| s.execute((old_position,)))
                {
                    tracing::error!("Failed to update old playing track: {e}");
                    return;
                }
            }
            if let Err(e) = connection
                .prepare_cached("UPDATE queue SET playing = TRUE WHERE position = ?")
                .and_then(|mut s| s.execute((position,)))
            {
                tracing::error!("Failed to update playing track: {e}");
            }
            {
                let items = self.items.borrow();
                if let Some(old) = items.get(old_position as usize).and_then(|q| q.upgrade()) {
                    old.set_playing(false);
                }
                if let Some(new) = items.get(position as usize).and_then(|q| q.upgrade()) {
                    new.set_playing(true);
                }
            }
            self.obj().notify_playing_track_position();
            self.obj().notify_playing_track();
            self.obj().emit_playback_changed();
        }

        fn can_move_up(&self) -> bool {
            let Some(connection) = self.connection.get() else {
                error!("Queue connection not set");
                return false;
            };
            connection
                .prepare_cached(
                    "
                    SELECT EXISTS (
                        SELECT 1
                        FROM (
                            SELECT selected - lead(selected) OVER (ORDER BY position) AS difference
                            FROM queue
                        ) WHERE difference = -1
                    )
                    ",
                )
                .and_then(|mut s| s.query_row((), |r| r.get(0)))
                .unwrap_or_else(|e| {
                    error!("Failed to get if selected queue can move to top: {e}");
                    false
                })
        }

        fn can_move_down(&self) -> bool {
            let Some(connection) = self.connection.get() else {
                error!("Queue connection not set");
                return false;
            };
            connection
                .prepare_cached(
                    "
                    SELECT EXISTS (
                        SELECT 1
                        FROM (
                            SELECT selected - lead(selected) OVER (ORDER BY position DESC) AS difference
                            FROM queue
                        ) WHERE difference = -1
                    )
                    ",
                )
                .and_then(|mut s| s.query_row((), |r| r.get(0)))
                .unwrap_or_else(|e| {
                    error!("Failed to get if selected queue can move to bottom: {e}");
                    false
                })
        }

        fn any_selected(&self) -> bool {
            let Some(connection) = self.connection.get() else {
                error!("Queue connection not set");
                return false;
            };
            connection
                .prepare_cached(
                    "
                    SELECT EXISTS (
                        SELECT 1
                        FROM queue
                        WHERE selected = TRUE
                    )
                    ",
                )
                .and_then(|mut s| s.query_row((), |r| r.get(0)))
                .unwrap_or_else(|e| {
                    error!("Failed to get if there is any item selected: {e}");
                    false
                })
        }
    }

    impl super::Queue {
        pub fn new(conn: Rc<Connection>, track_store: WeakRef<TrackStore>) -> Self {
            let obj = Object::new::<Self>();
            let imp = obj.imp();
            if imp.connection.set(conn).is_err() {
                error!("Failed setting queue connection");
            }
            if imp.track_store.set(track_store).is_err() {
                error!("Failed setting queue track_store");
            }
            let n_items = imp.lookup_item_count();
            imp.items
                .borrow_mut()
                .resize(n_items as usize, Default::default());
            imp.playing_track_position
                .set(imp.lookup_current_playing_track());
            obj
        }

        pub fn reload(&self) {
            let imp = self.imp();
            let old_n_items = imp.items.borrow().len() as u32;
            let new_n_items = imp.lookup_item_count();
            imp.items.borrow_mut().clear();
            self.items_changed(0, old_n_items, 0);
            imp.items
                .borrow_mut()
                .resize(new_n_items as usize, Default::default());
            self.items_changed(0, 0, new_n_items);
            let playing_track_position = imp.lookup_current_playing_track();
            imp.playing_track_position.set(playing_track_position);
            self.notify_playing_track();
            self.notify_playing_track_position();
            self.emit_playback_changed();
            self.notify_can_move_up();
            self.notify_can_move_down();
            self.notify_any_selected();
            self.notify_n_items();
        }

        pub fn move_tracks(&self, move_tracks: MoveTracks) {
            let imp = self.imp();
            let Some(connection) = imp.connection.get() else {
                error!("Queue connection not set");
                return;
            };
            let sql = match move_tracks{
                MoveTracks::Up =>
                    "
                    WITH first_unselected AS (
                        SELECT min(position) AS position
                        FROM queue
                        WHERE selected = FALSE
                    )
                    UPDATE queue
                    SET position = source.position - position_after_move_down(source.selected) OVER (ORDER BY source.position DESC)
                    FROM queue AS source
                    JOIN tracks
                    ON source.track_id = tracks.id
                    WHERE queue.rowid = source.rowid
                    AND queue.position >= (SELECT position FROM first_unselected)
                    RETURNING
                        queue.position,
                        (
                            SELECT inner_tracks.position
                            FROM tracks AS inner_tracks
                            WHERE inner_tracks.id = queue.track_id
                        ),
                        queue.selected,
                        queue.playing
                    ",
                MoveTracks::Down =>
                    "
                    WITH last_unselected AS (
                        SELECT max(position) AS position
                        FROM queue
                        WHERE selected = FALSE
                    )
                    UPDATE queue
                    SET position = source.position + position_after_move_down(source.selected) OVER (ORDER BY source.position)
                    FROM queue AS source
                    JOIN tracks
                    ON source.track_id = tracks.id
                    WHERE queue.rowid = source.rowid
                    AND queue.position <= (SELECT position FROM last_unselected)
                    RETURNING
                        queue.position,
                        (
                            SELECT inner_tracks.position
                            FROM tracks AS inner_tracks
                            WHERE inner_tracks.id = queue.track_id
                        ),
                        queue.selected,
                        queue.playing
                    ",
                MoveTracks::Top =>
                    "
                    UPDATE queue
                    SET position = row_number() OVER (ORDER BY 1 - source.selected, source.position) - 1
                    FROM queue AS source
                    JOIN tracks
                    ON source.track_id = tracks.id
                    WHERE queue.rowid = source.rowid
                    RETURNING
                        queue.position,
                        (
                            SELECT inner_tracks.position
                            FROM tracks AS inner_tracks
                            WHERE inner_tracks.id = queue.track_id
                        ),
                        queue.selected,
                        queue.playing
                    ",
                MoveTracks::Bottom =>
                    "
                    UPDATE queue
                    SET position = row_number() OVER (ORDER BY source.selected, source.position) - 1
                    FROM queue AS source
                    JOIN tracks
                    ON source.track_id = tracks.id
                    WHERE queue.rowid = source.rowid
                    RETURNING
                        queue.position,
                        (
                            SELECT inner_tracks.position
                            FROM tracks AS inner_tracks
                            WHERE inner_tracks.id = queue.track_id
                        ),
                        queue.selected,
                        queue.playing
                    ",
            };
            if let Err(e) = connection.prepare_cached(sql).and_then(|mut s| {
                let mut rows = s.query(())?;
                let items = imp.items.borrow();
                while let Some(row) = rows.next()? {
                    let position = row.get(0)?;
                    if let Some(item) = items.get(position as usize).and_then(|q| q.upgrade()) {
                        item.set_track_position(row.get::<_, u32>(1)?);
                        item.set_selected(row.get::<_, bool>(2)?);
                        let playing = row.get(3)?;
                        item.set_playing(playing);
                        if playing {
                            imp.playing_track_position.set(position);
                            self.notify_playing_track();
                            self.notify_playing_track_position();
                        }
                    }
                }
                Ok(())
            }) {
                error!("Failed to move tracks: {e}");
            }
        }

        fn play(
            &self,
            enqueue: Enqueue,
            insert: impl FnOnce(&Connection, u32) -> rusqlite::Result<usize>,
        ) {
            let imp = self.imp();
            let Some(connection) = imp.connection.get() else {
                error!("Queue connection not set");
                return;
            };
            let old_n_items = imp.items.borrow().len() as u32;
            if enqueue == Enqueue::Replace {
                if let Err(e) = imp.delete_queued_tracks(connection) {
                    error!("Failed deleting tracks from queue: {e}");
                    return;
                }
            }
            let n_items = if enqueue == Enqueue::Replace {
                0
            } else {
                old_n_items
            };
            let new_n_items = match insert(connection, n_items) {
                Err(e) => {
                    error!("Failed to play items: {e}");
                    return;
                }
                Ok(added_items) => added_items as u32 + n_items,
            };
            match enqueue {
                Enqueue::Add => {
                    imp.items
                        .borrow_mut()
                        .resize(new_n_items as usize, Default::default());
                    self.items_changed(old_n_items, 0, new_n_items - old_n_items);
                    self.notify_n_items();
                }
                Enqueue::Replace => {
                    imp.items.borrow_mut().clear();
                    imp.playing_track_position.set(gtk::INVALID_LIST_POSITION);
                    self.notify_playing_track();
                    self.notify_playing_track_position();
                    self.items_changed(0, old_n_items, 0);
                    imp.items
                        .borrow_mut()
                        .resize(new_n_items as usize, Default::default());
                    self.items_changed(0, 0, new_n_items);
                    self.notify_n_items();
                    self.set_playing_track_position(0);
                }
            }
        }

        pub fn play_all_artists(&self, shuffle: ArtistShuffle, enqueue: Enqueue) {
            self.play(enqueue, |connection, n_items| {
                match shuffle {
                    ArtistShuffle::None => connection
                        .prepare_cached(
                            "
                            INSERT INTO queue(position, track_id, selected, playing)
                            SELECT
                              ? + rank() OVER (ORDER BY tracks.position) - 1,
                              tracks.id,
                              FALSE,
                              FALSE
                            FROM tracks
                            ",
                        )
                        .and_then(|mut s| s.execute((n_items,))),
                    ArtistShuffle::Track => connection
                        .prepare_cached(
                            "
                            INSERT INTO queue(position, track_id, selected, playing)
                            SELECT
                              ? + rank() OVER (ORDER BY random()) - 1,
                              tracks.id,
                              FALSE,
                              FALSE
                            FROM tracks
                            ",
                        )
                        .and_then(|mut s| s.execute((n_items,))),
                    ArtistShuffle::Album => connection
                        .prepare_cached(
                            "
                            INSERT INTO queue(position, track_id, selected, playing)
                            SELECT
                              ? + rank() OVER (ORDER BY shuffled_albums.random_position, tracks.position) - 1,
                              tracks.id,
                              FALSE,
                              FALSE
                            FROM (
                                SELECT
                                  albums.position AS position,
                                  row_number() OVER (ORDER BY random()) AS random_position
                                FROM albums
                            ) AS shuffled_albums
                            JOIN tracks ON shuffled_albums.position = tracks.album_position
                            ",
                        )
                        .and_then(|mut s| s.execute((n_items,))),
                }
            })
        }

        pub fn play_artist(&self, position: u32, shuffle: ArtistShuffle, enqueue: Enqueue) {
            self.play(enqueue, |connection, n_items| {
                match shuffle {
                    ArtistShuffle::None => connection
                        .prepare_cached(
                            "
                            INSERT INTO queue(position, track_id, selected, playing)
                            SELECT
                              ? + rank() OVER (ORDER BY tracks.position) - 1,
                              tracks.id,
                              FALSE,
                              FALSE
                            FROM tracks
                            JOIN albums ON tracks.album_position = albums.position
                            WHERE albums.artist_position = ?
                            ",
                        )
                        .and_then(|mut s| s.execute((n_items, position))),
                    ArtistShuffle::Track => connection
                        .prepare_cached(
                            "
                            INSERT INTO queue(position, track_id, selected, playing)
                            SELECT
                              ? + rank() OVER (ORDER BY random()) - 1,
                              tracks.id,
                              FALSE,
                              FALSE
                            FROM tracks
                            JOIN albums ON tracks.album_position = albums.position
                            WHERE albums.artist_position = ?
                            ",
                        )
                        .and_then(|mut s| s.execute((n_items, position))),
                    ArtistShuffle::Album => connection
                        .prepare_cached(
                            "
                            INSERT INTO queue(position, track_id, selected, playing)
                            SELECT
                              ? + rank() OVER (ORDER BY shuffled_albums.random_position, tracks.position) - 1,
                              tracks.id,
                              FALSE,
                              FALSE
                            FROM (
                                SELECT
                                  albums.position AS position,
                                  row_number() OVER (ORDER BY random()) AS random_position
                                FROM albums
                                WHERE albums.artist_position = ?
                            ) AS shuffled_albums
                            JOIN tracks ON shuffled_albums.position = tracks.album_position
                            ",
                        )
                        .and_then(|mut s| s.execute((n_items, position))),
                }
            })
        }

        pub fn play_album(&self, position: u32, shuffle: AlbumShuffle, enqueue: Enqueue) {
            self.play(enqueue, |connection, n_items| match shuffle {
                AlbumShuffle::None => connection
                    .prepare_cached(
                        "
                            INSERT INTO queue(position, track_id, selected, playing)
                            SELECT
                              ? + rank() OVER (ORDER BY tracks.position) - 1,
                              tracks.id,
                              FALSE,
                              FALSE
                            FROM tracks
                            WHERE tracks.album_position = ?
                            ",
                    )
                    .and_then(|mut s| s.execute((n_items, position))),
                AlbumShuffle::Track => connection
                    .prepare_cached(
                        "
                            INSERT INTO queue(position, track_id, selected, playing)
                            SELECT
                              ? + rank() OVER (ORDER BY random()) - 1,
                              tracks.id,
                              FALSE,
                              FALSE
                            FROM tracks
                            WHERE tracks.album_position = ?
                            ",
                    )
                    .and_then(|mut s| s.execute((n_items, position))),
            })
        }

        pub fn add_to_queue(&self, position: u32) {
            let imp = self.imp();
            let Some(connection) = imp.connection.get() else {
                error!("Queue connection not set");
                return;
            };
            let old_n_items = imp.items.borrow().len() as u32;
            if let Err(e) = connection
                .prepare_cached(
                    "
                    INSERT INTO queue(position, track_id, selected, playing)
                    SELECT
                        ?,
                        tracks.id,
                        FALSE,
                        FALSE
                    FROM tracks
                    WHERE tracks.position = ?
                    ",
                )
                .and_then(|mut s| s.execute((old_n_items, position)))
            {
                error!("Failed to queue track: {e}");
                return;
            }
            imp.items.borrow_mut().push(Default::default());
            self.items_changed(old_n_items, 0, 1);
            self.notify_n_items();
        }

        pub fn toggle_select(&self, position: u32) {
            let imp = self.imp();
            let Some(connection) = imp.connection.get() else {
                error!("Queue connection not set");
                return;
            };
            if let Err(e) = connection
                .prepare_cached(
                    "
                    UPDATE queue
                    SET selected = NOT selected
                    WHERE position = ?
                    ",
                )
                .and_then(|mut s| s.execute((position,)))
            {
                error!("Failed to toggle select: {e}");
                return;
            }
            {
                let items = imp.items.borrow();
                if let Some(queued_track) = items.get(position as usize).and_then(|q| q.upgrade()) {
                    queued_track.set_selected(!queued_track.selected());
                }
            }
            self.notify_can_move_up();
            self.notify_can_move_down();
            self.notify_any_selected();
        }

        pub fn set_select_all_from_queue(&self, selected: bool) {
            let imp = self.imp();
            let Some(connection) = imp.connection.get() else {
                error!("Queue connection not set");
                return;
            };
            if let Err(e) = connection
                .prepare_cached(
                    "
                    UPDATE queue
                    SET selected = ?
                    ",
                )
                .and_then(|mut s| s.execute((selected,)))
            {
                error!("Failed to unselect all from queue: {e}");
                return;
            }
            for item in imp.items.borrow().iter().filter_map(|w| w.upgrade()) {
                item.set_selected(selected);
            }
            self.notify_can_move_up();
            self.notify_can_move_down();
            self.notify_any_selected();
        }

        pub fn remove_selected(&self) {
            let imp = self.imp();
            let Some(connection) = imp.connection.get() else {
                error!("Queue connection not set");
                return;
            };
            if let Err(e) = (|| {
                loop {
                    let mut s = connection.prepare_cached(
                        "
                        SELECT position
                        FROM queue
                        WHERE selected = TRUE
                        ORDER BY position
                        LIMIT 1
                        ",
                    )?;
                    let Some(begin_position) = s.query_row((), |r| r.get(0)).optional()? else {
                        break;
                    };
                    let mut s = connection.prepare_cached(
                        "
                        SELECT position
                        FROM queue
                        WHERE selected = FALSE AND position > ?
                        ORDER BY position
                        LIMIT 1
                        ",
                    )?;
                    let n_items = imp.items.borrow().len();
                    let end_position = s
                        .query_row((begin_position,), |r| r.get(0))
                        .optional()?
                        .unwrap_or(n_items);
                    imp.items.borrow_mut().drain(begin_position..end_position);
                    let mut s = connection.prepare_cached(
                        "
                        DELETE FROM queue
                        WHERE position BETWEEN ? AND ?
                        ",
                    )?;
                    s.execute((begin_position, end_position - 1))?;
                    let removed = end_position - begin_position;
                    if n_items > end_position {
                        let mut s = connection.prepare_cached(
                            "
                            UPDATE queue
                            SET position = position - ?
                            WHERE position > ?
                            ",
                        )?;
                        s.execute((removed, end_position - 1))?;
                    }
                    self.items_changed(begin_position as u32, removed as u32, 0);
                    self.notify_n_items();
                    let playing_track_position = imp.playing_track_position.get();
                    if (begin_position..end_position).contains(&(playing_track_position as usize)) {
                        if end_position < n_items {
                            let position = end_position - removed;
                            if let Some(queued_track) = imp
                                .items
                                .borrow_mut()
                                .get(position)
                                .and_then(|q| q.upgrade())
                            {
                                queued_track.set_playing(true);
                            }
                            if let Err(e) = connection
                                .prepare_cached(
                                    "
                                UPDATE queue
                                SET playing = TRUE
                                WHERE position = ?
                                ",
                                )
                                .and_then(|mut s| s.execute((position,)))
                            {
                                error!("Failed to update playing track: {e}");
                            }
                            imp.playing_track_position.set(position as u32);
                        } else if begin_position > 0 {
                            let position = begin_position - 1;
                            if let Some(queued_track) = imp
                                .items
                                .borrow_mut()
                                .get(position)
                                .and_then(|q| q.upgrade())
                            {
                                queued_track.set_playing(true);
                            }
                            if let Err(e) = connection
                                .prepare_cached(
                                    "
                                UPDATE queue
                                SET playing = TRUE
                                WHERE position = ?
                                ",
                                )
                                .and_then(|mut s| s.execute((position,)))
                            {
                                error!("Failed to update playing track: {e}");
                            }
                            imp.playing_track_position.set(position as u32);
                        } else {
                            imp.playing_track_position.set(gtk::INVALID_LIST_POSITION);
                        }
                        self.emit_playback_changed();
                        self.notify_playing_track();
                        self.notify_playing_track_position();
                    } else if playing_track_position >= end_position as u32 {
                        imp.playing_track_position
                            .set(playing_track_position - removed as u32);
                        self.notify_playing_track();
                        self.notify_playing_track_position();
                    }
                }
                Ok::<_, rusqlite::Error>(())
            })() {
                error!("Failed to remove rows: {e}");
            }
            self.notify_can_move_up();
            self.notify_can_move_down();
            self.notify_any_selected();
        }

        fn emit_playback_changed(&self) {
            self.emit_by_name_with_values("playback-changed", &[]);
        }
    }
}

glib::wrapper! {
    pub struct Queue(ObjectSubclass<imp::Queue>)
        @implements ListModel;
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum ArtistShuffle {
    None,
    Track,
    Album,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum AlbumShuffle {
    None,
    Track,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Enqueue {
    Add,
    Replace,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum MoveTracks {
    Up,
    Down,
    Top,
    Bottom,
}
