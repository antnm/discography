use {
    super::{Artist, SidebarStore},
    crate::i18n::fgettext,
    adw::prelude::*,
    gdk::Paintable,
    gettextrs::gettext,
    glib::{self, subclass::prelude::*, Object, Properties, WeakRef},
    gstreamer_play::gst::ClockTime,
    std::{
        cell::{Cell, OnceCell, RefCell},
        marker::PhantomData,
        num::NonZeroU32,
    },
    tracing::error,
};

mod imp {
    use super::*;

    #[derive(Debug, Properties, Default)]
    #[properties(wrapper_type = super::Album)]
    pub struct Album {
        #[property(get)]
        position: Cell<u32>,
        #[property(get)]
        title: OnceCell<Option<String>>,
        #[property(get = |this: &Self| this.title
            .get()
            .and_then(|o| o.as_ref())
            .map(|s| glib::markup_escape_text(s).to_string())
            .unwrap_or_else(|| format!("<i>{}</i>", gettext("Unknown Album")))
        )]
        formatted_title: PhantomData<String>,
        #[property(get = |this: &Self| this.year.get().as_ref().map(NonZeroU32::to_string))]
        search_info: PhantomData<Option<String>>,
        year: Cell<Option<NonZeroU32>>,
        #[property(get = |this: &Self|
            this.year
                .get()
                .as_ref()
                .map(NonZeroU32::to_string)
                .unwrap_or_default()
        )]
        _year: PhantomData<String>,
        duration: Cell<Option<ClockTime>>,
        #[property(get = |this: &Self| {
            let Some(duration) = this.duration.get()
            else {
                return String::new();
            };
            let hours = duration.hours();
            let minutes = duration.minutes() % 60;
            if hours > 0 && minutes > 0 {
                fgettext(
                    // Translators: Do not translate the contents between '{' and '}', this is a variable name
                    "{hours}h {minutes}m",
                    &[
                        ("hours", &hours.to_string()),
                        ("minutes", &minutes.to_string()),
                    ],
                )
            } else if hours > 0 {
                fgettext(
                    // Translators: Do not translate the contents between '{' and '}', this is a variable name
                    "{hours}h",
                    &[("hours", &hours.to_string())],
                )
            } else {
                fgettext(
                    // Translators: Do not translate the contents between '{' and '}', this is a variable name
                    "{minutes}m",
                    &[("minutes", &minutes.to_string())],
                )
            }
        })]
        _duration: PhantomData<String>,
        #[property(get, set)]
        art: RefCell<Option<Paintable>>,
        #[property(get)]
        art_path: OnceCell<Option<String>>,
        sidebar_store: OnceCell<WeakRef<SidebarStore>>,
        artist_position: Cell<u32>,
        #[property(get = |this: &Self| {
            this.sidebar_store.get()
                .and_then(WeakRef::upgrade)
                .and_then(|store| store.artist(this.artist_position.get()))
        })]
        artist: PhantomData<Option<Artist>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Album {
        const NAME: &'static str = "Album";
        type Type = super::Album;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Album {}

    impl super::Album {
        pub fn new(
            position: u32,
            title: Option<String>,
            year: Option<NonZeroU32>,
            duration: Option<ClockTime>,
            art: Paintable,
            art_path: Option<String>,
            sidebar_store: WeakRef<SidebarStore>,
            artist_position: u32,
        ) -> Self {
            let obj = Object::new::<Self>();
            let imp = obj.imp();
            imp.position.set(position);
            if imp.title.set(title).is_err() {
                error!("Album title already set");
            }
            imp.year.set(year);
            imp.duration.set(duration);
            if imp.sidebar_store.set(sidebar_store).is_err() {
                error!("Album sidebar store already set");
            }
            imp.artist_position.set(artist_position);
            imp.art.replace(Some(art));
            if imp.art_path.set(art_path).is_err() {
                error!("Album art path already set");
            }
            obj
        }
    }
}

glib::wrapper! {
    pub struct Album(ObjectSubclass<imp::Album>);
}
