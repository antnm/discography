use {
    super::Artist,
    crate::models::{InitialSidebarItem, InitialSidebarItemType, SidebarItem},
    gio::ListModel,
    glib::{Object, Type, WeakRef},
    gtk::{prelude::*, subclass::prelude::*},
    rusqlite::Connection,
    std::{
        cell::{OnceCell, RefCell},
        rc::Rc,
    },
    tracing::error,
};

mod imp {
    use {super::*, gtk::SectionModel};

    #[derive(Debug)]
    pub struct SidebarStore {
        pub(super) connection: OnceCell<Rc<Connection>>,
        initial_items: Vec<InitialSidebarItem>,
        artists: RefCell<Vec<WeakRef<Artist>>>,
    }

    impl Default for SidebarStore {
        fn default() -> Self {
            Self {
                connection: Default::default(),
                artists: Default::default(),
                initial_items: vec![
                    InitialSidebarItem::new(InitialSidebarItemType::AllArtists),
                    InitialSidebarItem::new(InitialSidebarItemType::Playlists),
                ],
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SidebarStore {
        const NAME: &'static str = "SidebarStore";
        type Type = super::SidebarStore;
        type Interfaces = (ListModel, SectionModel);
    }

    impl ObjectImpl for SidebarStore {}

    impl SectionModelImpl for SidebarStore {
        fn section(&self, position: u32) -> (u32, u32) {
            let initial_items = self.initial_items.len() as u32;
            if position < initial_items {
                (0, initial_items)
            } else {
                (
                    initial_items,
                    initial_items + self.artists.borrow().len() as u32,
                )
            }
        }
    }

    impl ListModelImpl for SidebarStore {
        fn item_type(&self) -> Type {
            SidebarItem::static_type()
        }

        fn n_items(&self) -> u32 {
            (self.initial_items.len() + self.artists.borrow().len()) as u32
        }

        fn item(&self, position: u32) -> Option<Object> {
            if let Some(initial_item) = self.initial_items.get(position as usize) {
                Some(initial_item.clone().upcast())
            } else {
                self.obj()
                    .artist(position - self.initial_items.len() as u32)
                    .and_upcast()
            }
        }
    }

    impl SidebarStore {
        fn lookup_artist_count(&self) -> u32 {
            let Some(connection) = self.connection.get() else {
                error!("Artist store connection not set");
                return 0;
            };
            connection
                .prepare_cached("SELECT count(*) FROM artists")
                .and_then(|mut s| s.query_row((), |row| row.get(0)))
                .unwrap_or_else(|e| {
                    tracing::error!("Failed to get the number of artists, {e}");
                    0
                })
        }
    }

    impl super::SidebarStore {
        pub fn new(conn: Rc<Connection>) -> Self {
            let sidebar_store = Object::new::<Self>();
            let imp = sidebar_store.imp();
            if imp.connection.set(conn).is_err() {
                error!("Failed setting sidebar store connection");
            }
            imp.artists
                .borrow_mut()
                .resize(imp.lookup_artist_count() as usize, Default::default());
            sidebar_store
        }

        pub fn reload(&self) {
            let imp = self.imp();
            let old_n_items = imp.artists.borrow().len() as u32;
            let new_n_items = imp.lookup_artist_count();
            imp.artists.borrow_mut().clear();
            imp.artists
                .borrow_mut()
                .resize(new_n_items as usize, Default::default());
            self.items_changed(imp.initial_items.len() as u32, old_n_items, new_n_items);
        }

        pub fn artist_count(&self) -> u32 {
            self.imp().artists.borrow().len() as u32
        }

        pub fn initial_items_count(&self) -> u32 {
            self.imp().initial_items.len() as u32
        }

        pub fn artist(&self, position: u32) -> Option<Artist> {
            let imp = self.imp();
            let artists = imp.artists.borrow();
            let weak_ref = artists.get(position as usize)?;
            if let Some(artist) = weak_ref.upgrade() {
                return Some(artist.upcast());
            }
            let Some(connection) = imp.connection.get() else {
                error!("Artist store connection not set");
                return None;
            };
            let artist = match connection
                .prepare_cached("SELECT title FROM artists WHERE position = ?")
                .and_then(|mut s| {
                    s.query_row((position,), |row| Ok(Artist::new(position, row.get(0)?)))
                }) {
                Ok(artist) => artist,
                Err(e) => {
                    error!("Failed retrieving artist: {e}");
                    return None;
                }
            };
            weak_ref.set(Some(&artist));
            Some(artist)
        }
    }
}

glib::wrapper! {
    pub struct SidebarStore(ObjectSubclass<imp::SidebarStore>)
        @implements ListModel;
}
