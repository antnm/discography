use {glib::Enum, tracing::error};

#[derive(Default, Debug, Copy, Clone, PartialEq, Eq, Enum)]
#[enum_type(name = "Repeat")]
pub enum Repeat {
    #[default]
    None,
    Track,
    All,
}

impl From<Repeat> for i32 {
    fn from(value: Repeat) -> Self {
        value as i32
    }
}

impl From<i32> for Repeat {
    fn from(value: i32) -> Self {
        match value {
            0 => Self::None,
            1 => Self::Track,
            2 => Self::All,
            n => {
                error!("Invalid repeat value: {n}");
                Self::None
            }
        }
    }
}

#[derive(Default, Debug, Copy, Clone, PartialEq, Eq, Enum)]
#[enum_type(name = "ReplayGain")]
pub enum ReplayGain {
    #[default]
    None,
    Track,
    Album,
}

impl From<ReplayGain> for i32 {
    fn from(value: ReplayGain) -> Self {
        value as i32
    }
}

impl From<i32> for ReplayGain {
    fn from(value: i32) -> Self {
        match value {
            0 => Self::None,
            1 => Self::Track,
            2 => Self::Album,
            n => {
                error!("Invalid ReplayGain value: {n}");
                Self::None
            }
        }
    }
}
