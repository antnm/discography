use {
    super::{Album, ArtStore, SidebarStore},
    gio::{prelude::*, subclass::prelude::*, ListModel},
    glib::{Object, Type, WeakRef},
    gstreamer_play::gst::ClockTime,
    rusqlite::{Connection, Error},
    std::{
        cell::{OnceCell, RefCell},
        ops::Range,
        rc::Rc,
        str,
    },
    tracing::error,
};

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct AlbumStore {
        connection: OnceCell<Rc<Connection>>,
        sidebar_store: OnceCell<WeakRef<SidebarStore>>,
        items: RefCell<Vec<WeakRef<Album>>>,
        art_store: OnceCell<ArtStore>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AlbumStore {
        const NAME: &'static str = "AlbumStore";
        type Type = super::AlbumStore;
        type Interfaces = (ListModel,);
    }

    impl ObjectImpl for AlbumStore {}

    impl ListModelImpl for AlbumStore {
        fn item_type(&self) -> Type {
            Album::static_type()
        }

        fn n_items(&self) -> u32 {
            self.items.borrow().len() as u32
        }

        fn item(&self, position: u32) -> Option<glib::Object> {
            let items = self.items.borrow();
            let weak_ref = items.get(position as usize)?;
            if let Some(album) = weak_ref.upgrade() {
                return Some(album.upcast());
            }
            let Some(connection) = self.connection.get() else {
                error!("Album store connection not set");
                return None;
            };
            let Some(art_store) = self.art_store.get() else {
                error!("Album store art store not set");
                return None;
            };
            let Some(sidebar_store) = self.sidebar_store.get() else {
                error!("Album store sidebar store not set");
                return None;
            };
            let album = connection
                .prepare_cached(
                    "
                        SELECT title, year, duration, art_path, artist_position
                        FROM albums
                        WHERE position = ?
                        ",
                )
                .and_then(|mut s| {
                    s.query_row((position,), |row| {
                        Ok(Album::new(
                            position,
                            row.get(0)?,
                            row.get(1)?,
                            row.get::<_, Option<f64>>(2)?
                                .map(ClockTime::from_seconds_f64),
                            art_store.loading_album_art(),
                            match row.get::<_, Option<Vec<u8>>>(3)? {
                                Some(s) => {
                                    Some(str::from_utf8(&s).map_err(Error::Utf8Error)?.to_string())
                                }
                                None => None,
                            },
                            sidebar_store.clone(),
                            row.get::<usize, u32>(4)?,
                        ))
                    })
                });
            match album {
                Err(e) => {
                    error!("Failed retrieving album: {e}");
                    None
                }
                Ok(album) => {
                    weak_ref.set(Some(&album));
                    glib::spawn_future_local(glib::clone!(
                        #[strong]
                        album,
                        #[strong]
                        art_store,
                        async move {
                            album.set_art(art_store.album_art(album.position()).await);
                        }
                    ));
                    Some(album.upcast())
                }
            }
        }
    }

    impl AlbumStore {
        fn lookup_item_count(&self) -> u32 {
            let Some(connection) = self.connection.get() else {
                error!("Album store connection not set");
                return 0;
            };
            connection
                .prepare_cached("SELECT count(*) FROM albums")
                .and_then(|mut s| s.query_row((), |row| row.get(0)))
                .unwrap_or_else(|e| {
                    tracing::error!("Failed to get the number of albums, {e}");
                    0
                })
        }
    }

    impl super::AlbumStore {
        pub fn new(
            conn: Rc<Connection>,
            sidebar_store: WeakRef<SidebarStore>,
            art_store: ArtStore,
        ) -> Self {
            let obj = Object::new::<Self>();
            let imp = obj.imp();
            if imp.connection.set(conn).is_err() {
                error!("Album store connection already set");
            }
            if imp.sidebar_store.set(sidebar_store).is_err() {
                error!("Album store artist store already store");
            }
            if imp.art_store.set(art_store).is_err() {
                error!("Album store art store already set");
            }
            imp.items
                .borrow_mut()
                .resize(imp.lookup_item_count() as usize, Default::default());
            obj
        }

        pub fn reload(&self) {
            let imp = self.imp();
            let old_n_items = imp.items.borrow().len() as u32;
            let new_n_items = imp.lookup_item_count();
            imp.items.borrow_mut().clear();
            imp.items
                .borrow_mut()
                .resize(new_n_items as usize, Default::default());
            self.items_changed(0, old_n_items, new_n_items);
        }

        pub fn artist_range(&self, artist_position: u32) -> Range<u32> {
            let imp = self.imp();
            let Some(connection) = imp.connection.get() else {
                error!("Album store connection not set");
                return Default::default();
            };
            let range = connection
                .prepare_cached(
                    "
                        SELECT min(position), max(position) + 1
                        FROM albums
                        WHERE artist_position = ?
                        ",
                )
                .and_then(|mut s| {
                    s.query_row((artist_position,), |row| Ok(row.get(0)?..row.get(1)?))
                });
            range.unwrap_or_else(|e| {
                error!("Failed to query artist range: {e}");
                Default::default()
            })
        }
    }
}

glib::wrapper! {
    pub struct AlbumStore(ObjectSubclass<imp::AlbumStore>)
        @implements ListModel;
}
