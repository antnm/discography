use {
    serde::{Deserialize, Serialize},
    std::path::PathBuf,
};

#[derive(Debug, Serialize, Deserialize)]
pub enum ImportSessionMessage {
    Finished(Option<ImportFailure>),
    StartedServer {
        name: String,
    },
    Resume {
        path: PathBuf,
    },
    ResolveConflict {
        old: Vec<ImportItem>,
        new: ImportItem,
    },
    Import {
        item: ImportItem,
        candidates: Vec<ImportItem>,
        can_group: bool,
    },
}

impl Default for ImportSessionMessage {
    fn default() -> Self {
        Self::Finished(None)
    }
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize, Default)]
pub enum ImportFailure {
    #[default]
    Unknown,
    MusicBrainzNotReachable,
}

#[derive(Debug, Serialize, Deserialize)]
pub enum ImportItem {
    Single {
        description: ImportItemDescription,
    },
    Multiple {
        description: ImportItemDescription,
        children: Vec<ImportItemDescription>,
    },
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ImportItemDescription {
    pub title: String,
    pub subtitle: Option<String>,
}

#[derive(Debug, Default, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum ImportDialogMessage {
    #[default]
    StartedServer,
    Abort,
    Resume,
    NoResume,
    Choice(ImportDialogChoice),
}

#[derive(Debug, Default, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum ImportDialogChoice {
    #[default]
    Skip,
    Candidate(u32),
    ImportAsIs,
    SearchById(String),
    SearchByTitle {
        artist: String,
        title: String,
    },
    Group,
    AsTracks,
    Keep,
    Remove,
    Merge,
}
