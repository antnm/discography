use {
    super::SidebarItem,
    crate::models::sidebar_item::SidebarItemImpl,
    gettextrs::gettext,
    glib::{prelude::*, subclass::prelude::*, Enum, Object, Properties},
    std::{cell::Cell, marker::PhantomData},
};

mod imp {
    use super::*;

    #[derive(Debug, Properties, Default)]
    #[properties(wrapper_type = super::InitialSidebarItem)]
    pub struct InitialSidebarItem {
        #[property(get, builder(InitialSidebarItemType::AllArtists))]
        item_type: Cell<InitialSidebarItemType>,
        #[property(
            get = |this: &Self| match this.item_type.get() {
                InitialSidebarItemType::AllArtists => gettext("All Artists"),
                InitialSidebarItemType::Playlists => gettext("Playlists"),
            },
            override_interface = SidebarItem,
        )]
        formatted_title: PhantomData<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for InitialSidebarItem {
        const NAME: &'static str = "InitialSidebarItem";
        type Type = super::InitialSidebarItem;
        type Interfaces = (SidebarItem,);
    }

    #[glib::derived_properties]
    impl ObjectImpl for InitialSidebarItem {}

    impl SidebarItemImpl for InitialSidebarItem {}

    impl super::InitialSidebarItem {
        pub fn new(item_type: InitialSidebarItemType) -> Self {
            let obj = Object::new::<Self>();
            obj.imp().item_type.set(item_type);
            obj
        }
    }
}

glib::wrapper! {
    pub struct InitialSidebarItem(ObjectSubclass<imp::InitialSidebarItem>)
        @implements SidebarItem;
}

#[derive(Default, Debug, Copy, Clone, PartialEq, Eq, Enum)]
#[enum_type(name = "InitialSidebarItemType")]
pub enum InitialSidebarItemType {
    #[default]
    AllArtists,
    Playlists,
}
