use {
    gdk::{Paintable, Texture},
    gio::{prelude::*, File},
    glib::{Bytes, ThreadPool, WeakRef},
    gtk::IconPaintable,
    rusqlite::{Connection, OptionalExtension},
    std::{cell::RefCell, collections::HashMap, rc::Rc},
    tracing::error,
};

#[derive(Clone, Debug)]
pub struct ArtStore {
    connection: Rc<Connection>,
    album_arts: Rc<RefCell<HashMap<u32, WeakRef<Paintable>>>>,
    playlist_arts: Rc<RefCell<HashMap<u32, WeakRef<Paintable>>>>,
    artist_art: Paintable,
    default_album_art: Paintable,
    loading_album_art: Paintable,
    thread_pool: Rc<ThreadPool>,
}

impl ArtStore {
    pub fn new(connection: Rc<Connection>) -> Self {
        Self {
            connection,
            album_arts: Default::default(),
            playlist_arts: Default::default(),
            artist_art: IconPaintable::for_file(
                &File::for_uri("resource:///eu/a11s/Encore/icons/scalable/actions/person-symbolic.svg"),
                40,
                1,
            ).upcast(),
            default_album_art: IconPaintable::for_file(
                &File::for_uri("resource:///eu/a11s/Encore/icons/scalable/actions/music-note-outline-symbolic.svg"),
                40,
                1,
            ).upcast(),
            loading_album_art: IconPaintable::for_file(
                &File::for_uri("resource:///eu/a11s/Encore/icons/scalable/actions/content-loading-symbolic.svg"),
                40,
                1,
            ).upcast(),
            thread_pool: Rc::new(ThreadPool::shared(Some(1)).unwrap()),
        }
    }

    pub async fn playlist_art(&self, playlist_position: u32) -> Paintable {
        self.art(
            self.playlist_arts.clone(),
            playlist_position,
            "
            SELECT art
            FROM playlists
            WHERE position = ? AND art IS NOT NULL
            ",
        )
        .await
    }

    pub async fn album_art(&self, album_position: u32) -> Paintable {
        self.art(
            self.album_arts.clone(),
            album_position,
            "
            SELECT art
            FROM albums
            WHERE position = ? AND art IS NOT NULL
            ",
        )
        .await
    }

    async fn art(
        &self,
        items: Rc<RefCell<HashMap<u32, WeakRef<Paintable>>>>,
        position: u32,
        query: &'static str,
    ) -> Paintable {
        items
            .borrow_mut()
            .retain(|_, item| item.upgrade().is_some());
        if let Some(item) = items.borrow().get(&position).and_then(WeakRef::upgrade) {
            return item;
        }
        let default_album_art = self.default_album_art.clone();
        let connection = self.connection.clone();
        let art = connection
            .prepare_cached(query)
            .and_then(|mut s| s.query_row((position,), |r| r.get::<_, Vec<u8>>(0)))
            .optional();
        let paintable = match art {
            Ok(Some(art)) => match Self::create_texture(&self.thread_pool, art).await {
                Some(texture) => texture.upcast(),
                None => default_album_art.clone(),
            },
            Ok(None) => default_album_art.clone(),
            Err(e) => {
                error!("Failed to get art: {e}");
                default_album_art.clone()
            }
        };
        items.borrow_mut().insert(position, paintable.downgrade());
        paintable
    }

    async fn create_texture(thread_pool: &ThreadPool, art: Vec<u8>) -> Option<Texture> {
        match thread_pool.push_future(|| match Texture::from_bytes(&Bytes::from_owned(art)) {
            Ok(texture) => Some(texture),
            Err(e) => {
                error!("Failed to create album art texture: {e}");
                None
            }
        }) {
            Ok(f) => match f.await {
                Ok(r) => r,
                Err(_) => {
                    error!("Failed to await future");
                    None
                }
            },
            Err(e) => {
                error!("Failed to push future: {e}");
                None
            }
        }
    }

    pub fn loading_album_art(&self) -> Paintable {
        self.loading_album_art.clone()
    }

    pub fn reload(&self) {
        self.album_arts.borrow_mut().clear();
        self.playlist_arts.borrow_mut().clear();
    }

    pub fn artist_art(&self) -> Paintable {
        self.artist_art.clone()
    }
}
