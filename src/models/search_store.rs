use {
    super::{ArtStore, SearchItem, SearchItemPosition},
    crate::i18n::nfgettext,
    core::str,
    gio::{prelude::*, ListModel},
    glib::{object::Cast, types::StaticType, Object, Properties},
    gtk::{subclass::prelude::*, SectionModel},
    rusqlite::{Connection, Error},
    std::{
        cell::{Cell, OnceCell, RefCell},
        marker::PhantomData,
        rc::Rc,
    },
    tracing::error,
};

mod imp {
    use super::*;

    #[derive(Clone, Copy, Debug, Default)]
    struct SectionCounts {
        artist_count: u32,
        album_count: u32,
        track_count: u32,
    }

    #[derive(Debug, Default, Properties)]
    #[properties(wrapper_type = super::SearchStore)]
    pub struct SearchStore {
        connection: OnceCell<Rc<Connection>>,
        items: RefCell<Vec<SearchItem>>,
        #[property(get = |this: &Self| this.items.borrow().len() as u32)]
        n_items: PhantomData<u32>,
        #[property(get, set = Self::set_query)]
        query: RefCell<String>,
        section_counts: Cell<SectionCounts>,
        art_store: OnceCell<ArtStore>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SearchStore {
        const NAME: &'static str = "SearchStore";
        type Type = super::SearchStore;
        type Interfaces = (ListModel, SectionModel);
    }

    #[glib::derived_properties]
    impl ObjectImpl for SearchStore {}

    impl ListModelImpl for SearchStore {
        fn item_type(&self) -> glib::Type {
            SearchItem::static_type()
        }

        fn n_items(&self) -> u32 {
            self.items.borrow().len() as u32
        }

        fn item(&self, position: u32) -> Option<glib::Object> {
            self.items
                .borrow()
                .get(position as usize)
                .map(|item| item.clone().upcast())
        }
    }

    impl SectionModelImpl for SearchStore {
        fn section(&self, position: u32) -> (u32, u32) {
            let section_counts = self.section_counts.get();
            if position < section_counts.artist_count {
                return (0, section_counts.artist_count);
            }
            let artists_and_albums_count = section_counts.artist_count + section_counts.album_count;
            if position < artists_and_albums_count {
                return (section_counts.artist_count, artists_and_albums_count);
            }
            let items_count = artists_and_albums_count + section_counts.track_count;
            if position < items_count {
                return (artists_and_albums_count, items_count);
            }
            (items_count, u32::MAX)
        }
    }

    impl SearchStore {
        pub fn set_query(&self, query: String) {
            let obj = self.obj();
            let query_empty = query.is_empty();
            let subset_of_initial_query = {
                let mut initial_query = self.query.borrow_mut();
                let result = !initial_query.is_empty() && query.contains(initial_query.as_str());
                *initial_query = query;
                result
            };

            let initial_n_items = self.n_items();
            self.items.borrow_mut().clear();
            obj.items_changed(0, initial_n_items, 0);

            if query_empty || (subset_of_initial_query && initial_n_items == 0) {
                return;
            }

            let art_store = if let Some(art_store) = self.art_store.get() {
                art_store
            } else {
                error!("ArtStore not set");
                return;
            };
            let connection = if let Some(connection) = self.connection.get() {
                connection
            } else {
                error!("Connection not set");
                return;
            };
            self.section_counts
                .set(match self.search(connection, art_store) {
                    Ok(section_counts) => section_counts,
                    Err(e) => {
                        error!("Failed searching for entries: {e}");
                        SectionCounts {
                            artist_count: 0,
                            album_count: 0,
                            track_count: 0,
                        }
                    }
                });
            obj.items_changed(0, 0, self.n_items());
            obj.notify_n_items();
        }

        fn search(
            &self,
            connection: &Connection,
            art_store: &ArtStore,
        ) -> Result<SectionCounts, Error> {
            let query = self
                .query
                .borrow()
                .replace(r"%", r"\%")
                .replace(r"_", r"\_")
                .replace(r" ", r"%")
                .replace(r"\", r"\\");
            let query = format!("%{}%", query);
            let mut stmt = connection.prepare_cached(
                "
                SELECT kind, position, title, additional_info, album_art_position
                FROM search_entries
                WHERE title LIKE ?
                LIMIT 200
                ",
            )?;
            let mut rows = stmt.query((&query,))?;
            let mut items = self.items.borrow_mut();
            let mut artist_count = 0;
            let mut album_count = 0;
            let mut track_count = 0;
            while let Some(row) = rows.next()? {
                let position = match row.get(0)? {
                    0 => SearchItemPosition::Artist(row.get(1)?),
                    1 => SearchItemPosition::Album(row.get(1)?),
                    2 => SearchItemPosition::Track(row.get(1)?),
                    other => {
                        error!("Unknown search entry kind: {other}");
                        continue;
                    }
                };
                let search_item = SearchItem::new(
                    position,
                    row.get(2)?,
                    match position {
                        SearchItemPosition::Artist(_) => {
                            let n = row.get::<_, u32>(3)?;
                            nfgettext(
                                // Translators: Do not translate the contents between '{' and '}', this is a variable name
                                "{number_of_albums} album",
                                "{number_of_albums} albums",
                                n,
                                &[("number_of_albums", &n.to_string())],
                            )
                        }
                        _ => row.get(3)?,
                    },
                    match position {
                        SearchItemPosition::Artist(_) => art_store.artist_art(),
                        _ => art_store.loading_album_art(),
                    },
                );
                match position {
                    SearchItemPosition::Artist(_) => (),
                    _ => {
                        let album_position = row.get(4)?;
                        glib::spawn_future_local(glib::clone!(
                            #[strong]
                            art_store,
                            #[strong]
                            search_item,
                            async move {
                                search_item.set_art(art_store.album_art(album_position).await);
                            }
                        ));
                    }
                }
                items.push(search_item);
                match position {
                    SearchItemPosition::Track(_) => track_count += 1,
                    SearchItemPosition::Album(_) => album_count += 1,
                    SearchItemPosition::Artist(_) => artist_count += 1,
                };
            }
            Ok(SectionCounts {
                track_count,
                album_count,
                artist_count,
            })
        }
    }

    impl super::SearchStore {
        pub fn new(conn: Rc<Connection>, art_store: ArtStore) -> Self {
            let obj = Object::new::<Self>();
            let imp = obj.imp();
            if imp.connection.set(conn).is_err() {
                error!("Connection already set");
            }
            if imp.art_store.set(art_store).is_err() {
                error!("ArtStore already set");
            }
            obj
        }

        pub fn reload(&self) {
            self.set_query("");
        }
    }
}

glib::wrapper! {
    pub struct SearchStore(ObjectSubclass<imp::SearchStore>)
        @implements ListModel, SectionModel;
}
