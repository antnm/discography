use {
    super::Album,
    crate::utils::format_clock_time,
    gettextrs::gettext,
    glib::{prelude::*, subclass::prelude::*, Object, Properties},
    gstreamer_play::gst::ClockTime,
    std::{
        cell::{Cell, OnceCell},
        marker::PhantomData,
    },
    tracing::error,
};

mod imp {
    use super::*;

    #[derive(Debug, Properties, Default)]
    #[properties(wrapper_type = super::Track)]
    pub struct Track {
        #[property(get)]
        pub position: Cell<u32>,
        #[property(get)]
        pub position_in_album: Cell<u32>,
        #[property(get)]
        title: OnceCell<Option<String>>,
        #[property(get = |this: &Self| this.title
            .get()
            .and_then(|o| o.as_ref())
            .map(|s| glib::markup_escape_text(s).to_string())
            .unwrap_or_else(|| format!("<i>{}</i>", gettext("Unknown Track")))
        )]
        formatted_title: PhantomData<String>,
        #[property(get)]
        album: OnceCell<Option<Album>>,
        #[property(get)]
        artist: OnceCell<Option<String>>,
        #[property(get = |this: &Self| this.artist
            .get()
            .and_then(|o| o.as_ref())
            .map(|s| glib::markup_escape_text(s).to_string())
            .unwrap_or_else(|| format!("<i>{}</i>", gettext("Unknown Artist")))
        )]
        formatted_artist: PhantomData<String>,
        #[property(get)]
        pub path: OnceCell<String>,
        track_number: OnceCell<Option<u32>>,
        #[property(get = |this: &Self| this.track_number
            .get()
            .and_then(|o| o.as_ref())
            .map(|s| s.to_string())
            .unwrap_or_default()
        )]
        formatted_track_number: PhantomData<String>,
        #[property(get)]
        duration: Cell<Option<ClockTime>>,
        #[property(get = |this: &Self| this.duration
            .get()
            .map(format_clock_time)
            .unwrap_or_default()
        )]
        formatted_duration: PhantomData<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Track {
        const NAME: &'static str = "Track";
        type Type = super::Track;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Track {}

    impl super::Track {
        pub fn new(
            position: u32,
            position_in_album: u32,
            title: Option<String>,
            artist: Option<String>,
            path: String,
            track_number: Option<u32>,
            duration: Option<ClockTime>,
            album: Option<Album>,
        ) -> Self {
            let obj = Object::new::<Self>();
            let imp = obj.imp();
            imp.position.set(position);
            imp.position_in_album.set(position_in_album);
            if imp.title.set(title).is_err() {
                error!("Track title already set");
            }
            if imp.artist.set(artist).is_err() {
                error!("Track artist already set");
            }
            if imp.path.set(path).is_err() {
                error!("Track path already set");
            }
            if imp.track_number.set(track_number).is_err() {
                error!("Track number already set");
            }
            imp.duration.set(duration);
            if imp.album.set(album).is_err() {
                error!("Album already set");
            }
            obj
        }

        pub fn track_number(&self) -> Option<u32> {
            self.imp().track_number.get().and_then(|o| *o)
        }
    }
}

glib::wrapper! {
    pub struct Track(ObjectSubclass<imp::Track>);
}
