use {glib::Enum, tracing::error};

#[derive(Default, Debug, Copy, Clone, PartialEq, Eq, Enum)]
#[enum_type(name = "TransferMode")]
pub enum TransferMode {
    #[default]
    Copy,
    Move,
}

impl From<TransferMode> for i32 {
    fn from(value: TransferMode) -> Self {
        value as i32
    }
}

impl From<i32> for TransferMode {
    fn from(value: i32) -> Self {
        match value {
            0 => Self::Copy,
            1 => Self::Move,
            n => {
                error!("Invalid transfer mode value: {n}");
                Self::Copy
            }
        }
    }
}

#[derive(Default, Debug, Copy, Clone, PartialEq, Eq, Enum)]
#[enum_type(name = "TaggingMode")]
pub enum TaggingMode {
    #[default]
    Update,
    Keep,
}

impl From<TaggingMode> for i32 {
    fn from(value: TaggingMode) -> Self {
        value as i32
    }
}

impl From<i32> for TaggingMode {
    fn from(value: i32) -> Self {
        match value {
            0 => Self::Update,
            1 => Self::Keep,
            n => {
                error!("Invalid tagging mode value: {n}");
                Self::Update
            }
        }
    }
}
