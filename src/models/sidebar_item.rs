use {
    glib::{
        gobject_ffi::GTypeInterface,
        prelude::*,
        subclass::{prelude::*, types::InterfaceStruct},
        ParamSpec, ParamSpecString,
    },
    std::{ffi::c_void, sync::OnceLock},
};

#[allow(dead_code)]
pub mod ffi {
    use super::*;

    #[repr(C)]
    pub struct Instance(c_void);
    #[derive(Copy, Clone)]
    #[repr(C)]
    pub struct Interface {
        parent_type: GTypeInterface,
    }

    unsafe impl InterfaceStruct for Interface {
        type Type = super::iface::SidebarItem;
    }
}

mod iface {
    use super::*;

    pub enum SidebarItem {}

    #[glib::object_interface]
    impl ObjectInterface for SidebarItem {
        const NAME: &'static str = "SidebarItem";

        type Instance = super::ffi::Instance;
        type Interface = super::ffi::Interface;

        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: OnceLock<Vec<ParamSpec>> = OnceLock::new();
            PROPERTIES.get_or_init(|| vec![ParamSpecString::builder("formatted-title").build()])
        }
    }
}

glib::wrapper! {
    pub struct SidebarItem(ObjectInterface<iface::SidebarItem>);
}

pub trait SidebarItemImpl: ObjectImpl + ObjectSubclass<Type: IsA<SidebarItem>> {}

unsafe impl<Obj: SidebarItemImpl> IsImplementable<Obj> for SidebarItem {}
