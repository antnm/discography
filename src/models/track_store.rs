use {
    super::{AlbumStore, Track},
    crate::i18n::fgettext,
    core::str,
    gio::ListModel,
    glib::{Object, Type, WeakRef},
    gstreamer_play::gst::ClockTime,
    gtk::{prelude::*, subclass::prelude::*, SectionModel},
    rusqlite::{Connection, Error},
    std::{
        cell::{OnceCell, RefCell},
        ops::Range,
        rc::Rc,
    },
    tracing::error,
};

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct TrackStore {
        connection: OnceCell<Rc<Connection>>,
        album_store: OnceCell<WeakRef<AlbumStore>>,
        items: RefCell<Vec<WeakRef<Track>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TrackStore {
        const NAME: &'static str = "TrackStore";
        type Type = super::TrackStore;
        type Interfaces = (ListModel, SectionModel);
    }

    impl ObjectImpl for TrackStore {}

    impl SectionModelImpl for TrackStore {
        fn section(&self, position: u32) -> (u32, u32) {
            let len = self.items.borrow().len() as u32;
            if position >= len {
                return (len, u32::MAX);
            }
            let Some(connection) = self.connection.get() else {
                error!("Track store connection not set");
                return (position, position + 1);
            };
            connection
                .prepare_cached(
                    "
                WITH disc AS (
                    SELECT disc_position AS position
                    FROM tracks
                    WHERE position = ?
                )
                SELECT min(tracks.position), max(tracks.position) + 1
                FROM tracks
                WHERE disc_position = (SELECT position FROM disc)
                ",
                )
                .and_then(|mut s| s.query_row((position,), |r| Ok((r.get(0)?, r.get(1)?))))
                .unwrap_or_else(|e| {
                    error!("Failed to query disc positions: {e}");
                    (position, position + 1)
                })
        }
    }

    impl ListModelImpl for TrackStore {
        fn item_type(&self) -> Type {
            Track::static_type()
        }

        fn n_items(&self) -> u32 {
            self.items.borrow().len() as u32
        }

        fn item(&self, position: u32) -> Option<Object> {
            let items = self.items.borrow();
            let weak_ref = items.get(position as usize)?;
            if let Some(track) = weak_ref.upgrade() {
                return Some(track.upcast());
            }
            let Some(connection) = self.connection.get() else {
                error!("Track store connection not set");
                return None;
            };
            let Some(album_store) = self.album_store.get() else {
                error!("Track store album store not set");
                return None;
            };
            let track =connection
                        .prepare_cached(
                            "
                            SELECT
                                (SELECT min(position) FROM tracks WHERE album_position = albums.position),
                                tracks.title,
                                tracks.artist,
                                tracks.path,
                                tracks.track_number,
                                tracks.duration,
                                albums.position
                            FROM tracks
                            JOIN albums ON tracks.album_position = albums.position
                            WHERE tracks.position = ?
                            ").and_then(|mut s| s.query_row(
                            (position,),
                            |row|{
                                let album_position = row.get(6)?;
                                Ok(Track::new(
                                    position,
                                    position - row.get::<_, u32>(0)?,
                                    row.get::<>(1)?,
                                    row.get(2)?,
                                    str::from_utf8(&row.get::<_, Vec<u8>>(3)?).map_err(Error::Utf8Error)?.to_string(),
                                    row.get(4)?,
                                    row.get::<_, Option<f64>>(5)?.map(ClockTime::from_seconds_f64),
                                    album_store.upgrade()
                                        .and_then(|store| store.item(album_position).and_downcast())
                                ))
                            }
                        ));
            match track {
                Err(e) => {
                    error!("Failed retrieving track: {e}");
                    None
                }
                Ok(track) => {
                    weak_ref.set(Some(&track));
                    Some(track.upcast())
                }
            }
        }
    }

    impl TrackStore {
        fn lookup_item_count(&self) -> u32 {
            let Some(connection) = self.connection.get() else {
                error!("Track store connection not set");
                return 0;
            };
            connection
                .prepare_cached("SELECT count(*) FROM tracks")
                .and_then(|mut s| s.query_row((), |row| row.get(0)))
                .unwrap_or_else(|e| {
                    tracing::error!("Failed to get the number of tracks, {e}");
                    0
                })
        }
    }

    impl super::TrackStore {
        pub fn new(conn: Rc<Connection>, album_store: WeakRef<AlbumStore>) -> Self {
            let obj = Object::new::<Self>();
            let imp = obj.imp();
            if imp.connection.set(conn).is_err() {
                error!("Track store connection already set");
            }
            if imp.album_store.set(album_store).is_err() {
                error!("Track store album store already set");
            }
            imp.items
                .borrow_mut()
                .resize(imp.lookup_item_count() as usize, Default::default());
            obj
        }

        pub fn reload(&self) {
            let imp = self.imp();
            imp.items.borrow_mut().clear();
            let old_n_items = imp.items.borrow().len() as u32;
            let new_n_items = imp.lookup_item_count();
            imp.items.borrow_mut().clear();
            imp.items
                .borrow_mut()
                .resize(new_n_items as usize, Default::default());
            self.items_changed(0, old_n_items, new_n_items);
        }

        pub fn disc_title(&self, position: u32) -> Option<String> {
            let imp = self.imp();
            if position >= imp.items.borrow().len() as u32 {
                return None;
            }
            let Some(connection) = imp.connection.get() else {
                error!("Track store connection not set");
                return None;
            };
            let query_result = connection
                .prepare_cached(
                    "
                    SELECT
                        discs.disc_number,
                        discs.title,
                        NOT EXISTS (
                            SELECT 1
                            FROM tracks AS inner_tracks
                            WHERE inner_tracks.album_position = tracks.album_position
                            AND inner_tracks.disc_position != discs.position
                        )
                    FROM tracks
                    JOIN discs
                    ON tracks.disc_position = discs.position
                    WHERE tracks.position = ?
                    ",
                )
                .and_then(|mut s| {
                    s.query_row((position,), |r| {
                        Ok((r.get::<_, u32>(0)?, r.get(1)?, r.get::<_, bool>(2)?))
                    })
                });
            let (number, title, only_disc) = match query_result {
                Err(e) => {
                    error!("Failed to query disc title: {e}");
                    return None;
                }
                Ok(r) => r,
            };
            match title {
                Some(title) => Some(title),
                None if only_disc => None,
                None => Some(fgettext(
                    // Translators: Do not translate the contents between '{' and '}', this is a variable name
                    "Disc {disc_number}",
                    &[("disc_number", &(number).to_string())],
                )),
            }
        }

        pub fn album_range(&self, album_position: u32) -> Range<u32> {
            let imp = self.imp();
            if album_position == gtk::INVALID_LIST_POSITION {
                return Default::default();
            }
            let Some(connection) = imp.connection.get() else {
                error!("Track store connection not set");
                return Default::default();
            };
            let range = connection
                .prepare_cached(
                    "
                        SELECT min(position), max(position) + 1
                        FROM tracks
                        WHERE album_position = ?
                        ",
                )
                .and_then(|mut s| {
                    s.query_row((album_position,), |row| Ok(row.get(0)?..row.get(1)?))
                });
            range.unwrap_or_else(|e| {
                error!("Failed to query album range: {e}");
                Default::default()
            })
        }
    }
}

glib::wrapper! {
    pub struct TrackStore(ObjectSubclass<imp::TrackStore>)
        @implements ListModel, SectionModel;
}
