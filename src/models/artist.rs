use {
    super::SidebarItem,
    crate::models::sidebar_item::SidebarItemImpl,
    gettextrs::gettext,
    glib::{prelude::*, subclass::prelude::*, Object, Properties},
    std::{
        cell::{Cell, OnceCell},
        marker::PhantomData,
    },
    tracing::error,
};

mod imp {
    use super::*;

    #[derive(Debug, Properties, Default)]
    #[properties(wrapper_type = super::Artist)]
    pub struct Artist {
        #[property(get)]
        position: Cell<u32>,
        #[property(get)]
        title: OnceCell<Option<String>>,
        #[property(
            get = |this: &Self| this.title
                .get()
                .and_then(|o| o.as_ref())
                .map(|s| glib::markup_escape_text(s).to_string())
                .unwrap_or_else(|| format!("<i>{}</i>", gettext("Unknown Artist"))),
            override_interface = SidebarItem,
        )]
        formatted_title: PhantomData<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Artist {
        const NAME: &'static str = "Artist";
        type Type = super::Artist;
        type Interfaces = (SidebarItem,);
    }

    #[glib::derived_properties]
    impl ObjectImpl for Artist {}

    impl SidebarItemImpl for Artist {}

    impl super::Artist {
        pub fn new(position: u32, title: Option<String>) -> Self {
            let obj = Object::new::<Self>();
            let imp = obj.imp();
            imp.position.set(position);
            if imp.title.set(title).is_err() {
                error!("Failed setting artist title");
            }
            obj
        }
    }
}

glib::wrapper! {
    pub struct Artist(ObjectSubclass<imp::Artist>)
        @implements SidebarItem;
}
