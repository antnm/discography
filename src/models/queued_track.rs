use {
    super::Track,
    crate::models::TrackStore,
    adw::prelude::*,
    glib::{subclass::prelude::*, Object, Properties, WeakRef},
    std::{
        cell::{Cell, OnceCell},
        marker::PhantomData,
    },
    tracing::error,
};

mod imp {
    use super::*;

    #[derive(Debug, Properties, Default)]
    #[properties(wrapper_type = super::QueuedTrack)]
    pub struct QueuedTrack {
        track_store: OnceCell<WeakRef<TrackStore>>,
        #[property(get, set = Self::set_track_position)]
        track_position: Cell<u32>,
        #[property(get, set)]
        selected: Cell<bool>,
        #[property(get, set)]
        playing: Cell<bool>,
        #[property(get = Self::track)]
        track: PhantomData<Option<Track>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for QueuedTrack {
        const NAME: &'static str = "QueuedTrack";
        type Type = super::QueuedTrack;
    }

    #[glib::derived_properties]
    impl ObjectImpl for QueuedTrack {}

    impl QueuedTrack {
        fn track(&self) -> Option<Track> {
            self.track_store
                .get()
                .and_then(|r| r.upgrade())
                .and_then(|s| s.item(self.track_position.get()).and_downcast())
        }

        fn set_track_position(&self, track_position: u32) {
            self.track_position.set(track_position);
            self.obj().notify_track();
        }
    }

    impl super::QueuedTrack {
        pub fn new(
            track_store: WeakRef<TrackStore>,
            track_position: u32,
            selected: bool,
            playing: bool,
        ) -> Self {
            let obj = Object::new::<Self>();
            let imp = obj.imp();
            if imp.track_store.set(track_store).is_err() {
                error!("Queued track track store already set");
            }
            imp.track_position.set(track_position);
            imp.selected.set(selected);
            imp.playing.set(playing);
            obj
        }
    }
}

glib::wrapper! {
    pub struct QueuedTrack(ObjectSubclass<imp::QueuedTrack>);
}
