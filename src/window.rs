use {
    crate::{
        application::Application,
        config::{APP_ID, PROFILE},
        import_dialog::ImportDialog,
        logs,
        models::{Repeat, ReplayGain, TaggingMode, TransferMode},
        pages::TrackPage,
        player::Player,
        preferences_dialog::PreferencesDialog,
        widgets::QueueRow,
    },
    adw::{
        prelude::*, subclass::prelude::*, AboutDialog, ApplicationWindow, BottomSheet,
        NavigationSplitView, NavigationView, OverlaySplitView, Toast, ToastOverlay,
    },
    gettextrs::gettext,
    gio::{ActionGroup, ActionMap, Settings},
    glib::{
        subclass::InitializingObject, BoolError, MainContext, Propagation, Properties,
        UserDirectory,
    },
    gstreamer_play::gst::ClockTime,
    gtk::{CompositeTemplate, FileDialog, ListItem, Root, SingleSelection, ToggleButton, Widget},
    std::{
        cell::{Cell, RefCell},
        path::Path,
    },
    tracing::{debug, error},
};

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate, Properties)]
    #[template(resource = "/eu/a11s/Encore/window.ui")]
    #[properties(wrapper_type = super::Window)]
    pub struct Window {
        pub settings: Settings,
        #[property(get, set = Self::set_player)]
        player: RefCell<Option<Player>>,
        #[property(get, set)]
        restore_state: Cell<bool>,
        #[property(get, set, builder(TransferMode::Copy))]
        transfer_mode: Cell<TransferMode>,
        #[property(get, set, builder(TaggingMode::Update))]
        tagging_mode: Cell<TaggingMode>,
        #[template_child]
        artists_and_albums_split_view: TemplateChild<OverlaySplitView>,
        #[template_child]
        albums_and_tracks_split_view: TemplateChild<NavigationSplitView>,
        #[template_child]
        bottom_sheet: TemplateChild<BottomSheet>,
        #[template_child]
        toast_overlay: TemplateChild<ToastOverlay>,
        #[template_child]
        navigation_view: TemplateChild<NavigationView>,
        #[template_child]
        track_page: TemplateChild<TrackPage>,
        #[template_child]
        selection_toggle_button: TemplateChild<ToggleButton>,
    }

    impl Default for Window {
        fn default() -> Self {
            Self {
                settings: Settings::new(APP_ID),
                restore_state: Default::default(),
                transfer_mode: Default::default(),
                tagging_mode: Default::default(),
                player: Default::default(),
                artists_and_albums_split_view: Default::default(),
                albums_and_tracks_split_view: Default::default(),
                bottom_sheet: Default::default(),
                toast_overlay: Default::default(),
                navigation_view: Default::default(),
                track_page: Default::default(),
                selection_toggle_button: Default::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type Type = super::Window;
        type ParentType = ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
            klass.install_property_action("win.transfer-mode", "transfer-mode");
            klass.install_property_action("win.tagging-mode", "tagging-mode");
            klass.install_action("win.artist-sidebar-show", None, |win, _, _| {
                win.imp()
                    .artists_and_albums_split_view
                    .set_show_sidebar(true)
            });
            klass.install_action("win.artist-sidebar-show", None, |win, _, _| {
                win.imp()
                    .artists_and_albums_split_view
                    .set_show_sidebar(true)
            });
            klass.install_action(
                "win.show-toast",
                Some(&String::static_variant_type()),
                |win, _, variant| {
                    if let Some(message) = variant.and_then(|v| v.get::<String>()) {
                        win.imp().toast_overlay.add_toast(Toast::new(&message));
                    }
                },
            );
            klass.install_action("win.about", None, |win, _, _| {
                let dialog = AboutDialog::from_appdata(
                    "/eu/a11s/Encore/eu.a11s.Encore.metainfo.xml",
                    Some("0.1.0"),
                );
                dialog.set_translator_credits(&gettext("translator-credits"));
                dialog.set_debug_info(&logs::read_logs_file());
                dialog.present(Some(win));
            });
            klass.install_action("win.import", None, |win, _, _| {
                let win = win.clone();
                MainContext::default().spawn_local(async move {
                    let file = FileDialog::new().select_folder_future(Some(&win)).await;
                    if let Some(path) = file.ok().and_then(|file| file.path()) {
                        let dialog = ImportDialog::new();
                        let player = win.player();
                        let Some(player) = player else {
                            error!("Player not set");
                            return;
                        };
                        dialog.import(
                            Path::new(&player.library_path()),
                            &path,
                            win.transfer_mode(),
                            win.tagging_mode(),
                        );
                        dialog.present(Some(&win));
                        dialog.connect_closed(move |_| {
                            player.reload_library();
                        });
                    }
                });
            });
            klass.install_action("win.preferences", None, |win, _, _| {
                if let Some(player) = win.imp().player.borrow().as_ref() {
                    let dialog = PreferencesDialog::new(player);
                    player
                        .bind_property("replay-gain", &dialog, "replay-gain")
                        .sync_create()
                        .bidirectional()
                        .build();
                    player
                        .bind_property("library-path", &dialog, "library-path")
                        .sync_create()
                        .transform_to(|_, v| {
                            Some(
                                Path::new(&v)
                                    .strip_prefix(glib::home_dir())
                                    .ok()
                                    .and_then(|f| {
                                        String::from_utf8(f.as_os_str().as_encoded_bytes().into())
                                            .ok()
                                    })
                                    .unwrap_or(v),
                            )
                        })
                        .build();
                    win.bind_property("restore-state", &dialog, "restore-state")
                        .sync_create()
                        .bidirectional()
                        .build();
                    dialog.present(Some(win));
                } else {
                    error!("Player not set");
                }
            });
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for Window {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();

            if PROFILE == "Devel" {
                obj.add_css_class("devel");
            }

            obj.load_window_size();
            obj.load_import_options();

            self.bottom_sheet
                .connect_bottom_bar_height_notify(glib::clone!(
                    #[weak(rename_to = this)]
                    self,
                    move |bottom_sheet| {
                        let margin = bottom_sheet.bottom_bar_height();
                        glib::idle_add_local_once(move || {
                            if let Some(sidebar) = this.artists_and_albums_split_view.sidebar() {
                                sidebar.set_margin_bottom(margin);
                            } else {
                                error!("Artists and albums split view sidebar not set");
                            }
                            if let Some(sidebar) = this.albums_and_tracks_split_view.sidebar() {
                                sidebar.set_margin_bottom(margin);
                            } else {
                                error!("Albums and tracks split view sidebar not set");
                            }
                            if let Some(content) = this.albums_and_tracks_split_view.content() {
                                content.set_margin_bottom(margin);
                            } else {
                                error!("Albums and tracks split view content not set");
                            }
                        });
                    }
                ));

            obj.connect_player_notify(|win| {
                let player = win.imp().player.borrow();
                let Some(player) = player.as_ref() else {
                    error!("Player not set");
                    return;
                };
                win.load_player_settings();
                let sidebar_selection = player.sidebar_selection();
                sidebar_selection.connect_selected_notify(glib::clone!(
                    #[weak]
                    win,
                    move |_| {
                        let view = &win.imp().artists_and_albums_split_view;
                        if view.is_collapsed() {
                            view.set_show_sidebar(false);
                        }
                    }
                ));
                let sidebar_store = player.sidebar_store();
                if sidebar_store.artist_count() > 0 {
                    win.imp().navigation_view.push_by_tag("browse");
                }
                sidebar_store.connect_items_changed(glib::clone!(
                    #[weak]
                    win,
                    move |store, _, _, _| {
                        let navigation_view = &*win.imp().navigation_view;
                        navigation_view.pop_to_tag("empty");
                        if store.artist_count() > 0 {
                            navigation_view.push_by_tag("browse");
                        }
                    }
                ));
            });
        }
    }

    #[gtk::template_callbacks]
    impl Window {
        #[template_callback]
        fn is_valid(&self, n_items: u32) -> bool {
            n_items > 0
        }

        #[template_callback]
        fn bottom_sheet_open(&self, n_items: u32, prev_value: bool) -> bool {
            prev_value && n_items > 0
        }

        fn set_player(&self, player: Player) {
            self.albums_and_tracks_split_view
                .get()
                .bind_property(
                    "show-content",
                    &player.current_albums_selection(),
                    "selected",
                )
                .bidirectional()
                .sync_create()
                .transform_to(|binding, show_content: bool| {
                    if !show_content {
                        if let Some(selection) = binding.target().and_downcast::<SingleSelection>()
                        {
                            selection.set_autoselect(false);
                        } else {
                            error!("Binding selection target not set");
                        }
                        Some(gtk::INVALID_LIST_POSITION)
                    } else {
                        None
                    }
                })
                .transform_from(|_, selected: u32| Some(selected != gtk::INVALID_LIST_POSITION))
                .build();
            self.albums_and_tracks_split_view
                .get()
                .bind_property(
                    "collapsed",
                    &player.current_albums_selection(),
                    "autoselect",
                )
                .invert_boolean()
                .build();
            self.player.replace(Some(player));
        }

        #[template_callback]
        fn setup_queue_row(&self, list_item: ListItem) {
            let child = QueueRow::new();
            list_item
                .bind_property("item", &child, "item")
                .sync_create()
                .build();
            list_item
                .bind_property("position", &child, "position")
                .transform_to(|_, u: u32| Some(u.to_variant()))
                .sync_create()
                .build();
            self.selection_toggle_button
                .bind_property("active", &child, "selection-mode")
                .sync_create()
                .build();
            list_item.set_child(Some(&child));
        }

        #[template_callback]
        fn activate_queue_row(&self, position: u32) {
            if self.selection_toggle_button.is_active() {
                if let Err(e) = self
                    .selection_toggle_button
                    .activate_action("app.select-from-queue", Some(&position.to_variant()))
                {
                    error!("Failed to activate app.select-from-queue: {e}");
                }
            } else if let Err(e) = self
                .selection_toggle_button
                .activate_action("app.play-from-queue", Some(&position.to_variant()))
            {
                error!("Failed to activate app.play-from-queue: {e}");
            }
        }

        #[template_callback]
        fn toggle_selection_mode(&self) {
            if self.selection_toggle_button.is_active() {
                if let Err(e) = self
                    .selection_toggle_button
                    .activate_action("app.set-select-all-from-queue", Some(&false.to_variant()))
                {
                    error!("Failed to activate app.unselect-all-from-queue: {e}");
                }
            }
        }

        #[template_callback]
        fn bottom_sheet_open_notify(&self) {
            if !self.bottom_sheet.is_open() {
                self.selection_toggle_button.set_active(false);
            }
        }
    }

    impl WidgetImpl for Window {}
    impl WindowImpl for Window {
        fn close_request(&self) -> Propagation {
            if let Err(err) = self.obj().save_settings() {
                tracing::warn!("Failed to save window state, {}", &err);
            }

            self.parent_close_request()
        }
    }

    impl ApplicationWindowImpl for Window {}
    impl AdwApplicationWindowImpl for Window {}

    impl super::Window {
        pub fn new(app: &Application) -> Self {
            let player: Player = app.player();
            glib::Object::builder()
                .property("application", app)
                .property("player", player)
                .build()
        }

        fn save_settings(&self) -> Result<(), BoolError> {
            let imp = self.imp();

            let (width, height) = self.default_size();
            let is_maximized = self.is_maximized();
            debug!(width, height, is_maximized);

            imp.settings.set_int("window-width", width)?;
            imp.settings.set_int("window-height", height)?;
            imp.settings.set_boolean("is-maximized", is_maximized)?;
            imp.settings
                .set_enum("transfer-mode", self.transfer_mode().into())?;
            imp.settings
                .set_enum("tagging-mode", self.tagging_mode().into())?;
            let restore_state = imp.restore_state.get();
            imp.settings.set_boolean("restore-state", restore_state)?;
            let player = imp.player.borrow();
            let Some(player) = player.as_ref() else {
                error!("Player not set");
                return Ok(());
            };
            if restore_state {
                imp.settings.set_boolean("playing", player.playing())?;
                imp.settings
                    .set_uint64("playing-position", player.position().useconds())?;
            }
            imp.settings
                .set_enum("replay-gain", player.replay_gain().into())?;
            imp.settings.set_enum("repeat", player.repeat().into())?;
            imp.settings
                .set_value("library-path", &Some(player.library_path()).to_variant())?;

            Ok(())
        }

        fn load_window_size(&self) {
            let imp = self.imp();
            let width = imp.settings.int("window-width");
            let height = imp.settings.int("window-height");
            let is_maximized = imp.settings.boolean("is-maximized");
            debug!(width, height, is_maximized);

            self.set_default_size(width, height);

            if is_maximized {
                self.maximize();
            }
        }

        fn load_import_options(&self) {
            let imp = self.imp();
            self.set_transfer_mode(TransferMode::from(imp.settings.enum_("transfer-mode")));
            self.set_tagging_mode(TaggingMode::from(imp.settings.enum_("tagging-mode")));
        }

        fn load_player_settings(&self) {
            let imp = self.imp();
            let player = imp.player.borrow();
            let Some(player) = player.as_ref() else {
                error!("Player not set");
                return;
            };
            player.set_replay_gain(ReplayGain::from(imp.settings.enum_("replay-gain")));
            player.set_repeat(Repeat::from(imp.settings.enum_("repeat")));
            let restore_state = imp.settings.boolean("restore-state");
            self.set_restore_state(restore_state);
            if restore_state {
                let playing_position =
                    ClockTime::from_useconds(imp.settings.uint64("playing-position"));
                let playing = imp.settings.boolean("playing");
                if playing {
                    player.seek(playing_position);
                    player.set_playing(true);
                } else {
                    glib::idle_add_local_once(glib::clone!(
                        #[weak]
                        player,
                        move || player.seek(playing_position)
                    ));
                }
            }
            if let Some(path) = imp.settings.value("library-path").get::<Option<String>>() {
                player.set_library_path(
                    path.or_else(|| {
                        glib::user_special_dir(UserDirectory::Music).and_then(|p| {
                            match String::from_utf8(p.into_os_string().into_encoded_bytes()) {
                                Ok(p) => Some(p),
                                Err(e) => {
                                    error!(
                                        "Failed to conver Music directory into UTF-8 string: {e}"
                                    );
                                    None
                                }
                            }
                        })
                    })
                    .unwrap_or_else(|| String::from("~/Music")),
                );
            } else {
                error!("Invalid library-path setting value");
            }
        }

        pub fn focus_track(&self, position: u32) {
            self.imp().track_page.focus_track(position);
        }
    }
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends Widget, gtk::Window, gtk::ApplicationWindow, ApplicationWindow,
        @implements ActionMap, ActionGroup, Root;
}
