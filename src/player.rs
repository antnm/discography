use {
    crate::{
        library,
        models::{
            Album, AlbumShuffle, AlbumStore, ArtStore, Artist, ArtistShuffle, Enqueue,
            InitialSidebarItem, InitialSidebarItemType, Queue, Repeat, ReplayGain, SearchStore,
            SidebarStore, Track, TrackStore,
        },
    },
    async_channel::{Receiver, Sender},
    gio::prelude::*,
    glib::{subclass::prelude::*, Object, Properties, SignalHandlerId},
    gstreamer_play::{
        gst::{prelude::*, Bin, ClockTime, Element, ElementFactory, GhostPad},
        Play, PlaySignalAdapter, PlayVideoRenderer,
    },
    gtk::{SingleSelection, SliceListModel},
    std::{
        cell::{Cell, RefCell},
        marker::PhantomData,
        rc::Rc,
    },
    tracing::{error, warn},
};

mod imp {
    use super::*;

    #[derive(Debug, Properties)]
    #[properties(wrapper_type = super::Player)]
    pub struct Player {
        art_store: ArtStore,
        #[property(get)]
        track_store: TrackStore,
        #[property(get)]
        current_tracks_slice: SliceListModel,
        #[property(get)]
        album_store: AlbumStore,
        #[property(get)]
        current_albums_selection: SingleSelection,
        #[property(get)]
        current_albums_slice: SliceListModel,
        #[property(get)]
        sidebar_store: SidebarStore,
        #[property(get)]
        sidebar_selection: SingleSelection,
        #[property(get)]
        queue: Queue,
        #[property(get)]
        search_store: SearchStore,
        #[property(get)]
        position: Cell<ClockTime>,
        #[property(get)]
        duration: Cell<ClockTime>,
        #[property(get = |this: &Self| {
            let obj = this.obj();
            let fraction = obj.position().seconds_f64() / obj.duration().seconds_f64();
            if (0.0..1.0).contains(&fraction) {
                fraction
            } else {
                0.0
            }
        })]
        progress: PhantomData<f64>,
        #[property(get, set, construct, builder(Repeat::None))]
        repeat: Cell<Repeat>,
        #[property(explicit_notify, get, set = Self::set_replay_gain, construct, builder(ReplayGain::None))]
        replay_gain: Cell<ReplayGain>,
        #[property(get, set = Self::set_playing)]
        playing: Cell<bool>,
        #[property(get)]
        can_play_next: Cell<bool>,
        #[property(get)]
        can_play_previous: Cell<bool>,
        #[property(get, set = Self::set_dragging)]
        dragging: Cell<bool>,
        #[property(get, set)]
        library_path: RefCell<String>,
        #[property(get, set)]
        updating_library: Cell<bool>,
        before_dragging_playing: Cell<bool>,
        gst_play: Play,
        receiver: Receiver<GStreamerChannelMessage>,
        rg_elements: Option<ReplayGainElements>,
        handler_ids: RefCell<Vec<(Object, SignalHandlerId)>>,
        _play_signal_adapter: PlaySignalAdapter,
    }

    enum GStreamerChannelMessage {
        DurationUpdated(ClockTime),
        PositionUpdated(ClockTime),
        EndOfStream,
    }

    #[derive(Debug)]
    struct ReplayGainElements {
        identity_filter: Element,
        rg_filter: Element,
        rg_volume: Element,
    }

    impl ReplayGainElements {
        fn new() -> Result<ReplayGainElements, glib::BoolError> {
            let rg_volume = ElementFactory::make_with_name("rgvolume", Some("rg volume"))?;
            let rg_limiter = ElementFactory::make_with_name("rglimiter", Some("rg limiter"))?;
            let rg_filter = Bin::with_name("rg filter");
            rg_filter.add(&rg_volume)?;
            rg_filter.add(&rg_limiter)?;
            rg_volume.link(&rg_limiter)?;
            let pad_src = rg_limiter
                .static_pad("src")
                .ok_or_else(|| glib::bool_error!("Could not get src pad from rg limiter"))?;
            let ghost_src = GhostPad::with_target(&pad_src)?;
            rg_filter.add_pad(&ghost_src)?;
            let pad_sink = rg_volume
                .static_pad("sink")
                .ok_or_else(|| glib::bool_error!("Could not get sink pad from rg volume"))?;
            let ghost_sink = GhostPad::with_target(&pad_sink)?;
            rg_filter.add_pad(&ghost_sink)?;

            let identity_filter = ElementFactory::make_with_name("identity", None)?;

            Ok(ReplayGainElements {
                identity_filter,
                rg_volume,
                rg_filter: rg_filter.upcast(),
            })
        }
    }

    impl Default for Player {
        fn default() -> Self {
            let (sender, receiver) = async_channel::unbounded::<GStreamerChannelMessage>();
            let gst_play = Play::new(None::<PlayVideoRenderer>);
            let rg_elements = match ReplayGainElements::new() {
                Ok(res) => Some(res),
                Err(err) => {
                    error!("Could not setup replay gain: {}", err.message);
                    None
                }
            };
            let _play_signal_adapter = setup_gst_signals(&gst_play, &sender);

            let connection =
                Rc::new(library::create_connection().expect("Failed to create connection"));
            let sidebar_store = SidebarStore::new(connection.clone());
            let art_store = ArtStore::new(connection.clone());
            let album_store = AlbumStore::new(
                connection.clone(),
                sidebar_store.downgrade(),
                art_store.clone(),
            );
            let current_albums_slice = SliceListModel::new(Some(album_store.clone()), 0, 0);
            let track_store = TrackStore::new(connection.clone(), album_store.downgrade());
            let current_tracks_slice = SliceListModel::new(Some(track_store.clone()), 0, 0);
            let search_store = SearchStore::new(connection.clone(), art_store.clone());
            let queue = Queue::new(connection, track_store.downgrade());
            let sidebar_selection = SingleSelection::new(Some(sidebar_store.clone()));
            let current_albums_selection = SingleSelection::builder()
                .model(&current_albums_slice)
                .can_unselect(true)
                .build();

            Self {
                art_store,
                track_store,
                current_tracks_slice,
                album_store,
                current_albums_selection,
                current_albums_slice,
                sidebar_store,
                sidebar_selection,
                queue,
                search_store,
                position: Default::default(),
                repeat: Default::default(),
                replay_gain: Default::default(),
                dragging: Default::default(),
                before_dragging_playing: Default::default(),
                playing: Default::default(),
                can_play_next: Default::default(),
                can_play_previous: Default::default(),
                gst_play,
                rg_elements,
                receiver,
                _play_signal_adapter,
                handler_ids: Default::default(),
                duration: Default::default(),
                progress: Default::default(),
                library_path: Default::default(),
                updating_library: Default::default(),
            }
        }
    }

    fn setup_gst_signals(
        gst_play: &Play,
        sender: &Sender<GStreamerChannelMessage>,
    ) -> PlaySignalAdapter {
        let signal_adapter = PlaySignalAdapter::new(gst_play);
        signal_adapter.connect_duration_changed(glib::clone!(
            #[strong]
            sender,
            move |_, duration| {
                let Some(duration) = duration else {
                    return;
                };
                if let Err(err) =
                    sender.send_blocking(GStreamerChannelMessage::DurationUpdated(duration))
                {
                    error!("Failed to send DurationUpdated message: {}", err);
                }
            }
        ));
        signal_adapter.connect_position_updated(glib::clone!(
            #[strong]
            sender,
            move |_, position| {
                let Some(position) = position else {
                    return;
                };
                if let Err(err) =
                    sender.send_blocking(GStreamerChannelMessage::PositionUpdated(position))
                {
                    error!("Failed to send PositionUpdated message: {}", err);
                }
            }
        ));
        signal_adapter.connect_end_of_stream(glib::clone!(
            #[strong]
            sender,
            move |_| {
                if let Err(err) = sender.send_blocking(GStreamerChannelMessage::EndOfStream) {
                    error!("Failed to send EndOfStream message: {}", err);
                }
            }
        ));
        signal_adapter
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Player {
        const NAME: &'static str = "Player";
        type Type = super::Player;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Player {
        fn constructed(&self) {
            self.gst_play.set_video_track_enabled(false);

            let mut config = self.gst_play.config();
            config.set_position_update_interval(250);
            if let Err(err) = self.gst_play.set_config(config) {
                error!("Failed setting gst::Play config: {}", err.message.as_ref());
            };

            let ref_counted = self.ref_counted();
            glib::MainContext::default().spawn_local(glib::clone!(
                #[strong]
                ref_counted,
                async move {
                    while let Ok(message) = ref_counted.receiver.recv().await {
                        match message {
                            GStreamerChannelMessage::EndOfStream => {
                                if ref_counted.repeat.get() == Repeat::Track {
                                    ref_counted.obj().seek(ClockTime::ZERO);
                                } else if ref_counted.can_play_next.get() {
                                    ref_counted.obj().play_next();
                                } else {
                                    ref_counted.queue.set_playing_track_position(0);
                                    ref_counted.obj().set_playing(false);
                                }
                            }
                            GStreamerChannelMessage::PositionUpdated(position) => {
                                let obj = ref_counted.obj();
                                ref_counted.position.replace(position);
                                obj.notify_position();
                                obj.notify_progress();
                            }
                            GStreamerChannelMessage::DurationUpdated(duration) => {
                                let obj = ref_counted.obj();
                                ref_counted.duration.replace(duration);
                                obj.notify_position();
                                obj.notify_duration();
                            }
                        }
                    }
                }
            ));

            self.handler_ids.borrow_mut().push((
                self.sidebar_selection.clone().upcast(),
                self.sidebar_selection.connect_selected_notify(glib::clone!(
                    #[weak(rename_to = this)]
                    self,
                    move |sidebar_selection| this.with_blocked_signals(|| {
                        let Some(item) = sidebar_selection.selected_item() else {
                            this.current_albums_slice.set_size(0);
                            this.current_albums_slice.set_offset(0);
                            this.current_tracks_slice.set_size(0);
                            this.current_tracks_slice.set_offset(0);
                            return;
                        };
                        if let Some(initial_item) = item.downcast_ref::<InitialSidebarItem>() {
                            match initial_item.item_type() {
                                InitialSidebarItemType::AllArtists => {
                                    this.current_albums_slice.set_offset(0);
                                    this.current_albums_slice
                                        .set_size(this.album_store.n_items());
                                }
                                InitialSidebarItemType::Playlists => {
                                    this.current_albums_slice.set_size(0);
                                    this.current_albums_slice.set_offset(0);
                                }
                            }
                        } else if let Some(artist) = item.downcast_ref::<Artist>() {
                            let range = this.album_store.artist_range(artist.position());
                            this.current_albums_slice.set_offset(range.start);
                            this.current_albums_slice.set_size(range.end - range.start);
                        }
                        let Some(album) = this
                            .current_albums_selection
                            .selected_item()
                            .and_downcast::<Album>()
                        else {
                            this.current_tracks_slice.set_size(0);
                            this.current_tracks_slice.set_offset(0);
                            return;
                        };
                        let range = this.track_store.album_range(album.position());
                        this.current_tracks_slice.set_offset(range.start);
                        this.current_tracks_slice.set_size(range.end - range.start);
                    })
                )),
            ));
            self.handler_ids.borrow_mut().push((
                self.current_albums_selection.clone().upcast(),
                self.current_albums_selection
                    .connect_selected_notify(glib::clone!(
                        #[weak(rename_to = this)]
                        self,
                        move |current_albums_selection| this.with_blocked_signals(|| {
                            let Some(album) = current_albums_selection
                                .selected_item()
                                .and_downcast::<Album>()
                            else {
                                this.current_tracks_slice.set_size(0);
                                this.current_tracks_slice.set_offset(0);
                                return;
                            };
                            let range = this.track_store.album_range(album.position());
                            this.current_tracks_slice.set_offset(range.start);
                            this.current_tracks_slice.set_size(range.end - range.start);
                        })
                    )),
            ));
            self.sidebar_selection.notify("selected");
            self.current_albums_selection.notify("selected");

            let update_can_play_properties = |this: &super::Player, queue: &Queue| {
                let playing_track_position = queue.playing_track_position();
                let imp = this.imp();
                let repeat = imp.repeat.get();
                let can_play_next = playing_track_position != gtk::INVALID_LIST_POSITION
                    && (playing_track_position + 1 < queue.n_items() || repeat == Repeat::All);
                if imp.can_play_next.replace(can_play_next) != can_play_next {
                    this.notify_can_play_next();
                }
                let can_play_previous = playing_track_position != gtk::INVALID_LIST_POSITION
                    && (playing_track_position > 0 || repeat == Repeat::All);
                if imp.can_play_previous.replace(can_play_previous) != can_play_previous {
                    this.notify_can_play_previous();
                }
            };
            let obj = self.obj();
            self.queue.connect_playing_track_notify(glib::clone!(
                #[weak]
                obj,
                move |queue| {
                    update_can_play_properties(&obj, queue);
                }
            ));
            self.queue.connect_local(
                "playback-changed",
                false,
                glib::clone!(
                    #[weak]
                    obj,
                    #[upgrade_or]
                    None,
                    move |_| {
                        obj.imp().playing_track_changed();
                        None
                    }
                ),
            );
            self.queue.connect_items_changed(glib::clone!(
                #[weak]
                obj,
                move |queue, _, _, _| {
                    update_can_play_properties(&obj, queue);
                }
            ));
            obj.connect_repeat_notify(glib::clone!(
                #[weak(rename_to = queue)]
                self.queue,
                move |obj| {
                    update_can_play_properties(obj, &queue);
                }
            ));

            self.playing_track_changed();
            obj.set_playing(false);
        }
    }

    impl Player {
        fn with_blocked_signals<T>(&self, f: impl FnOnce() -> T) -> T {
            let handler_ids = self.handler_ids.borrow();
            for (object, handler_id) in handler_ids.iter() {
                object.block_signal(handler_id);
            }
            let result = f();
            for (object, handler_id) in handler_ids.iter() {
                object.unblock_signal(handler_id);
            }
            result
        }

        fn playing_track_changed(&self) {
            if let Some(item) = self.queue.playing_track() {
                let Some(track) = item.track() else {
                    error!("Track not set");
                    return;
                };
                let uri = match glib::filename_to_uri(track.path(), None) {
                    Ok(uri) => uri,
                    Err(e) => {
                        error!("Failed to construct URI: {e}");
                        return;
                    }
                };
                let obj = self.obj();
                if self.gst_play.uri().filter(|old| old == &uri).is_none() {
                    self.gst_play.set_uri(Some(&uri));
                }
                obj.seek(ClockTime::ZERO);
                obj.set_playing(true);
            } else {
                self.gst_play.stop();
                let obj = self.obj();
                obj.set_playing(false);
            }
        }

        fn set_playing(&self, playing: bool) {
            if self.gst_play.uri().is_none() {
                return;
            }
            if playing {
                self.gst_play.play();
            } else {
                self.gst_play.pause();
            }
            self.playing.set(playing);
        }

        fn set_dragging(&self, dragging: bool) {
            if dragging && !self.dragging.get() {
                self.dragging.set(true);
                self.before_dragging_playing.set(self.playing.get());
                self.gst_play.pause();
            } else if !dragging && self.dragging.get() {
                self.dragging.set(false);
                if self.before_dragging_playing.get() {
                    self.gst_play.play();
                }
            }
        }

        fn set_replay_gain(&self, replay_gain: ReplayGain) {
            if self.replay_gain.replace(replay_gain) == replay_gain {
                return;
            }
            if let Some(rg_elements) = &self.rg_elements {
                match replay_gain {
                    ReplayGain::None => {
                        rg_elements.rg_volume.set_property("album-mode", false);
                        self.gst_play
                            .pipeline()
                            .set_property("audio-filter", &rg_elements.identity_filter);
                    }
                    ReplayGain::Track => {
                        rg_elements.rg_volume.set_property("album-mode", false);
                        self.gst_play
                            .pipeline()
                            .set_property("audio-filter", &rg_elements.rg_filter);
                    }
                    ReplayGain::Album => {
                        rg_elements.rg_volume.set_property("album-mode", true);
                        self.gst_play
                            .pipeline()
                            .set_property("audio-filter", &rg_elements.rg_filter);
                    }
                }
            } else {
                warn!(
                    "Could not set ReplayGain because the gstreamer elements were not initialized"
                );
            }
            self.obj().notify_replay_gain();
        }
    }

    impl super::Player {
        pub fn new() -> Self {
            Object::new()
        }

        pub fn reload_library(&self) {
            let imp = self.imp();
            imp.art_store.reload();
            imp.sidebar_store.reload();
            imp.album_store.reload();
            imp.track_store.reload();
            imp.queue.reload();
            imp.search_store.reload();
            imp.sidebar_selection.notify("selected");
            imp.current_albums_selection.notify("selected");
        }

        pub fn has_next(&self) -> bool {
            let imp = self.imp();
            let n_items = imp.queue.n_items();
            let playing_track_position = imp.queue.playing_track_position();
            playing_track_position != gtk::INVALID_LIST_POSITION
                && (n_items > 0 && playing_track_position < n_items - 1
                    || imp.repeat.get() == Repeat::All)
        }

        pub fn has_previous(&self) -> bool {
            let imp = self.imp();
            let playing_track_position = imp.queue.playing_track_position();
            playing_track_position != gtk::INVALID_LIST_POSITION
                && (playing_track_position > 0 || imp.repeat.get() == Repeat::All)
        }

        pub fn seek(&self, position: ClockTime) {
            let imp = self.imp();
            if position != self.position() {
                imp.gst_play.seek(position);
                imp.position.replace(position);
                self.notify_position();
                self.notify_progress();
            }
        }

        pub fn play_next(&self) {
            let imp = self.imp();
            let playing_track_position = imp.queue.playing_track_position();
            let valid = playing_track_position != gtk::INVALID_LIST_POSITION;
            if valid && playing_track_position < imp.queue.n_items() - 1 {
                imp.queue
                    .set_playing_track_position(playing_track_position + 1);
            } else if valid && imp.repeat.get() == Repeat::All {
                if playing_track_position == 0 {
                    self.seek(ClockTime::ZERO);
                } else {
                    imp.queue.set_playing_track_position(0);
                }
            }
        }

        pub fn play_previous(&self) {
            let imp = self.imp();
            let playing_track_position = imp.queue.playing_track_position();
            let valid = playing_track_position != gtk::INVALID_LIST_POSITION;
            if valid && playing_track_position > 0 {
                imp.queue
                    .set_playing_track_position(playing_track_position - 1);
            } else if valid && imp.repeat.get() == Repeat::All {
                let next = imp.queue.n_items() - 1;
                if next == 0 {
                    self.seek(ClockTime::ZERO);
                } else {
                    imp.queue.set_playing_track_position(next);
                }
            }
        }

        pub fn play_artist(&self, shuffle: ArtistShuffle, enqueue: Enqueue) {
            let imp = self.imp();
            let Some(item) = self.sidebar_selection().selected_item() else {
                return;
            };
            if let Some(initial_item) = item.downcast_ref::<InitialSidebarItem>() {
                if initial_item.item_type() == InitialSidebarItemType::AllArtists {
                    imp.queue.play_all_artists(shuffle, enqueue);
                }
            } else if let Some(artist) = item.downcast_ref::<Artist>() {
                imp.queue.play_artist(artist.position(), shuffle, enqueue);
            }
        }

        pub fn play_album(&self, shuffle: AlbumShuffle, enqueue: Enqueue) {
            let Some(album) = self
                .current_albums_selection()
                .selected_item()
                .and_downcast::<Album>()
            else {
                error!("No album selected");
                return;
            };
            let imp = self.imp();
            imp.queue.play_album(album.position(), shuffle, enqueue);
        }

        pub fn show_artist(&self, position: u32) {
            let imp = self.imp();
            imp.sidebar_selection
                .set_selected(imp.sidebar_store.initial_items_count() + position);
        }

        pub fn show_album(&self, position: u32) {
            let imp = self.imp();
            imp.sidebar_selection.set_selected(0);
            imp.current_albums_selection.set_selected(position);
        }

        pub fn show_album_of_track(&self, position: u32) -> Option<Track> {
            let imp = self.imp();
            imp.sidebar_selection
                .set_selected(imp.sidebar_store.initial_items_count());
            let Some(track) = imp.track_store.item(position).and_downcast::<Track>() else {
                error!("Track {position} not found");
                return None;
            };
            if let Some(album) = track.album() {
                imp.current_albums_selection.set_selected(album.position());
            }
            Some(track)
        }

        pub fn seek_backward(&self, offset: ClockTime) {
            self.seek(self.position().saturating_sub(offset));
        }

        pub fn seek_forward(&self, offset: ClockTime) {
            self.seek(self.duration().min(self.position() + offset))
        }

        pub fn play_track_from_album(&self, track: u32) {
            let imp = self.imp();
            let Some(track) = imp.current_tracks_slice.item(track).and_downcast::<Track>() else {
                error!("Failed to get track");
                return;
            };
            if let Some(album_position) = track.album().map(|album| album.position()) {
                imp.queue
                    .play_album(album_position, AlbumShuffle::None, Enqueue::Replace);
                imp.queue
                    .set_playing_track_position(track.position_in_album());
            }
        }

        pub fn add_to_queue(&self, track: u32) {
            let imp = self.imp();
            imp.queue.add_to_queue(track);
        }
    }

    impl Default for super::Player {
        fn default() -> Self {
            Self::new()
        }
    }
}

glib::wrapper! {
    pub struct Player(ObjectSubclass<imp::Player>);
}
