mod application;
mod config;
mod i18n;
mod import_dialog;
mod library;
mod logs;
mod models;
mod mpris;
mod pages;
mod player;
mod preferences_dialog;
mod utils;
mod widgets;
mod window;

use {
    application::Application,
    clap::{ArgAction, Parser},
    config::{GETTEXT_PACKAGE, LOCALEDIR, RESOURCES_FILE},
    gettextrs::{gettext, LocaleCategory},
    gio::Resource,
    glib::{prelude::*, ExitCode},
    models::{Album, Artist, QueuedTrack, SearchItem, SearchStore, Track, TrackStore},
    pages::{AlbumPage, SearchPage, SidebarPage, TrackPage},
    std::path::PathBuf,
    tracing::error,
    widgets::{ArtContainer, PlayControls, QueueRow, TimeScale, TrackInfo, TrackRow},
};

#[derive(Parser)]
#[command(version)]
enum Args {
    Import {
        #[arg(long)]
        library_path: PathBuf,
        #[arg(long)]
        server_name: String,
        #[arg(long)]
        import_path: PathBuf,
        #[arg(long, action = ArgAction::Set)]
        move_files: bool,
        #[arg(long, action = ArgAction::Set)]
        autotag: bool,
    },
    Update {
        library_path: PathBuf,
    },
    Clear {
        library_path: PathBuf,
    },
}

fn main() -> ExitCode {
    gettextrs::setlocale(LocaleCategory::LcAll, "");
    if let Err(e) = gettextrs::bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR) {
        error!("Unable to bind the text domain: {e}");
        return ExitCode::FAILURE;
    }
    if let Err(e) = gettextrs::textdomain(GETTEXT_PACKAGE) {
        error!("Unable to switch to the text domain: {e}");
        return ExitCode::FAILURE;
    }

    if let Ok(args) = Args::try_parse() {
        logs::setup_tracing(false);
        return match args {
            Args::Import {
                server_name,
                library_path,
                import_path,
                move_files,
                autotag,
            } => {
                if let Err(e) =
                    library::importer(server_name, library_path, import_path, move_files, autotag)
                {
                    error!("Error importing: {e}");
                    ExitCode::FAILURE
                } else {
                    ExitCode::SUCCESS
                }
            }
            Args::Update { library_path } => {
                if let Err(e) = library::update_library(&library_path) {
                    error!("Error updating library: {e}");
                    ExitCode::FAILURE
                } else {
                    ExitCode::SUCCESS
                }
            }
            Args::Clear { library_path } => {
                if let Err(e) = library::clear_library(&library_path) {
                    error!("Error clearing library: {e}");
                    ExitCode::FAILURE
                } else {
                    ExitCode::SUCCESS
                }
            }
        };
    }

    logs::setup_tracing(true);

    // There is a memory leak when using playbin3 and switching audio files
    if let Err(e) = glib::setenv("GST_PLAY_USE_PLAYBIN3", "0", false) {
        error!("Failed to set GST_PLAY_USE_PLAYBIN3 envvar: {e}");
    }

    glib::log_set_default_handler(glib::rust_log_handler);
    if let Err(e) = adw::init() {
        error!("Could not initialize libadwaita: {e}");
        return ExitCode::FAILURE;
    }
    if let Err(e) = gstreamer_play::gst::init() {
        error!("Could not initialize GStreamer: {e}");
        return ExitCode::FAILURE;
    }

    glib::set_application_name(&gettext("Encore"));

    match Resource::load(RESOURCES_FILE) {
        Ok(res) => gio::resources_register(&res),
        Err(e) => {
            error!("Could not load gresource file: {e}");
            return ExitCode::FAILURE;
        }
    }

    Album::ensure_type();
    AlbumPage::ensure_type();
    ArtContainer::ensure_type();
    Artist::ensure_type();
    SidebarPage::ensure_type();
    PlayControls::ensure_type();
    QueueRow::ensure_type();
    QueuedTrack::ensure_type();
    SearchItem::ensure_type();
    SearchPage::ensure_type();
    SearchStore::ensure_type();
    TimeScale::ensure_type();
    Track::ensure_type();
    TrackInfo::ensure_type();
    TrackPage::ensure_type();
    TrackRow::ensure_type();
    TrackStore::ensure_type();

    let app = Application::default();
    app.run()
}
