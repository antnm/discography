use {
    std::{
        fs::{self, File},
        io::Error,
    },
    tracing::error,
    tracing_subscriber::{fmt, layer::SubscriberExt, util::SubscriberInitExt, Registry},
};

pub fn setup_tracing(remove_existing_log: bool) {
    let registry = Registry::default().with(fmt::layer().with_line_number(true));
    match open_logs_file(remove_existing_log) {
        Ok(file) => registry
            .with(
                fmt::layer()
                    .with_line_number(true)
                    .with_ansi(false)
                    .with_writer(file),
            )
            .init(),
        Err(e) => {
            error!("Failed to get logs file: {e}");
            registry.init();
        }
    }
}

pub fn read_logs_file() -> String {
    match fs::read_to_string(glib::user_state_dir().join("encore").join("logs")) {
        Ok(s) => s,
        Err(e) => {
            error!("Failed to read logs file: {e}");
            String::new()
        }
    }
}

fn open_logs_file(remove_existing: bool) -> Result<File, Error> {
    let state_dir = glib::user_state_dir().join("encore");
    if !fs::exists(&state_dir)? {
        fs::create_dir_all(&state_dir)?;
    }
    let logs_path = state_dir.join("logs");
    if remove_existing && fs::exists(&logs_path)? {
        fs::remove_file(&logs_path)?;
    }
    File::options().append(true).create(true).open(&logs_path)
}
