use {
    crate::{player::Player, utils::format_clock_time},
    adw::{prelude::*, subclass::prelude::*},
    glib::{subclass::InitializingObject, ParamSpec, Properties},
    gstreamer_play::gst::ClockTime,
    gtk::{Adjustment, Box, CompositeTemplate, Scale, Widget},
    std::cell::{Cell, RefCell},
    tracing::error,
};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/eu/a11s/Encore/time_scale.ui")]
    #[properties(wrapper_type = super::TimeScale)]
    pub struct TimeScale {
        #[property(get, set)]
        player: RefCell<Option<Player>>,
        dragging: Cell<bool>,
        #[template_child]
        time_adjustment: TemplateChild<Adjustment>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TimeScale {
        const NAME: &'static str = "TimeScale";
        type Type = super::TimeScale;
        type ParentType = Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[gtk::template_callbacks]
    impl TimeScale {
        #[template_callback(function)]
        fn format_time(sensitive: bool, time: f64) -> String {
            if sensitive {
                if let Ok(time) = ClockTime::try_from_seconds_f64(time) {
                    return format_clock_time(time);
                }
            }
            String::new()
        }

        #[template_callback]
        fn seek(&self, adjustment: Adjustment) {
            let value = ClockTime::from_seconds_f64(adjustment.value());
            if let Some(player) = self.player.borrow().as_ref() {
                if player.position().absdiff(value) > ClockTime::SECOND {
                    if let Err(e) = self
                        .obj()
                        .activate_action("app.seek", Some(&value.to_variant()))
                    {
                        error!("Activating action app.seek failed: {e}");
                    }
                }
            } else {
                error!("Player not set");
            }
        }

        #[template_callback(function)]
        fn clock_time_to_seconds(time: ClockTime) -> f32 {
            time.seconds_f32()
        }

        #[template_callback]
        fn time_drag_state_changed(&self, _: ParamSpec, scale: Scale) {
            let new_value = scale.has_css_class("dragging");
            if new_value != self.dragging.get() {
                self.dragging.set(new_value);
                if let Err(e) = self.obj().activate_action("app.dragging-time", None) {
                    error!("Activating action app.dragging-time failed: {e}");
                }
            }
        }

        #[template_callback(function)]
        fn is_valid(selected: u32) -> bool {
            selected != gtk::INVALID_LIST_POSITION
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for TimeScale {}

    impl WidgetImpl for TimeScale {}

    impl BoxImpl for TimeScale {}
}

glib::wrapper! {
    pub struct TimeScale(ObjectSubclass<imp::TimeScale>)
        @extends Widget;
}
