use {
    crate::player::Player,
    adw::{prelude::*, subclass::prelude::*},
    glib::{subclass::InitializingObject, Properties},
    gtk::{
        pango::{AttrInt, AttrList, Weight},
        Box, CompositeTemplate, Label, Widget,
    },
    std::cell::RefCell,
};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/eu/a11s/Encore/track_info.ui")]
    #[properties(wrapper_type = super::TrackInfo)]
    pub struct TrackInfo {
        #[property(get, set)]
        player: RefCell<Option<Player>>,
        #[template_child]
        title_label: TemplateChild<Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TrackInfo {
        const NAME: &'static str = "TrackInfo";
        type Type = super::TrackInfo;
        type ParentType = Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for TrackInfo {
        fn constructed(&self) {
            let attr_list = AttrList::new();
            attr_list.insert(AttrInt::new_weight(Weight::Bold));
            self.title_label.set_attributes(Some(&attr_list));
        }
    }

    impl WidgetImpl for TrackInfo {}

    impl BoxImpl for TrackInfo {}

    #[gtk::template_callbacks]
    impl TrackInfo {
        #[template_callback]
        fn is_valid(&self, selected: u32) -> bool {
            selected != gtk::INVALID_LIST_POSITION
        }
    }
}

glib::wrapper! {
    pub struct TrackInfo(ObjectSubclass<imp::TrackInfo>)
        @extends Widget;
}
