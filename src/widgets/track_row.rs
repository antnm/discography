use {
    crate::models::Track,
    adw::{prelude::*, subclass::prelude::*},
    glib::{subclass::InitializingObject, Object, Properties},
    gtk::{Box, CompositeTemplate, Widget},
    std::cell::{Cell, RefCell},
    tracing::error,
};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/eu/a11s/Encore/track_row.ui")]
    #[properties(wrapper_type = super::TrackRow)]
    pub struct TrackRow {
        #[property(get, set)]
        item: RefCell<Option<Track>>,
        #[property(get, set)]
        playing: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TrackRow {
        const NAME: &'static str = "TrackRow";
        type Type = super::TrackRow;
        type ParentType = Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.install_action("row.add-to-queue", None, move |row, _, _| {
                let item = row.imp().item.borrow();
                let Some(track) = item.as_ref() else {
                    return;
                };
                if let Err(e) =
                    row.activate_action("app.add-to-queue", Some(&track.position().to_variant()))
                {
                    error!("Failed to activate action to add track to queue: {e}");
                }
            });
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for TrackRow {}

    impl WidgetImpl for TrackRow {}

    impl BoxImpl for TrackRow {}

    impl super::TrackRow {
        pub fn new() -> Self {
            Object::new()
        }
    }

    impl Default for super::TrackRow {
        fn default() -> Self {
            Self::new()
        }
    }
}

glib::wrapper! {
    pub struct TrackRow(ObjectSubclass<imp::TrackRow>)
        @extends Widget;
}
