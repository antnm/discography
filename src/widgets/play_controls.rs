use {
    crate::{models::Repeat, player::Player},
    adw::{prelude::*, subclass::prelude::*},
    glib::{subclass::InitializingObject, Properties},
    gtk::{glib, Box, CompositeTemplate, Widget},
    std::cell::RefCell,
};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/eu/a11s/Encore/play_controls.ui")]
    #[properties(wrapper_type = super::PlayControls)]
    pub struct PlayControls {
        #[property(get, set)]
        player: RefCell<Option<Player>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PlayControls {
        const NAME: &'static str = "PlayControls";
        type Type = super::PlayControls;
        type ParentType = Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[gtk::template_callbacks]
    impl PlayControls {
        #[template_callback]
        fn play_button_icon(&self, playing: bool) -> &'static str {
            if playing {
                "media-playback-pause"
            } else {
                "media-playback-start"
            }
        }

        #[template_callback]
        fn repeat_menu_icon(&self, repeat: Repeat) -> &'static str {
            match repeat {
                Repeat::None => "media-playlist-consecutive",
                Repeat::Track => "media-playlist-repeat-song",
                Repeat::All => "media-playlist-repeat",
            }
        }

        #[template_callback]
        fn is_valid(&self, selected: u32) -> bool {
            selected != gtk::INVALID_LIST_POSITION
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for PlayControls {}

    impl WidgetImpl for PlayControls {}

    impl BoxImpl for PlayControls {}
}

glib::wrapper! {
    pub struct PlayControls(ObjectSubclass<imp::PlayControls>)
        @extends Widget;
}
