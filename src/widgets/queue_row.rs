use {
    crate::models::QueuedTrack,
    adw::{prelude::*, subclass::prelude::*},
    glib::{subclass::InitializingObject, Object, Properties, Variant, VariantTy},
    gtk::{Box, CompositeTemplate, Widget},
    std::cell::{Cell, RefCell},
};

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate, Properties)]
    #[template(resource = "/eu/a11s/Encore/queue_row.ui")]
    #[properties(wrapper_type = super::QueueRow)]
    pub struct QueueRow {
        #[property(get, set)]
        item: RefCell<Option<QueuedTrack>>,
        #[property(get, set, builder(VariantTy::UINT32))]
        position: RefCell<Variant>,
        #[property(get, set)]
        selection_mode: Cell<bool>,
    }

    impl Default for QueueRow {
        fn default() -> Self {
            Self {
                item: Default::default(),
                position: RefCell::new(0.to_variant()),
                selection_mode: Default::default(),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for QueueRow {
        const NAME: &'static str = "QueueRow";
        type Type = super::QueueRow;
        type ParentType = Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for QueueRow {}

    impl WidgetImpl for QueueRow {}

    impl BoxImpl for QueueRow {}

    impl super::QueueRow {
        pub fn new() -> Self {
            Object::new()
        }
    }

    impl Default for super::QueueRow {
        fn default() -> Self {
            Self::new()
        }
    }
}

glib::wrapper! {
    pub struct QueueRow(ObjectSubclass<imp::QueueRow>)
        @extends Widget;
}
