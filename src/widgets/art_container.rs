use {
    adw::{prelude::*, subclass::prelude::*, Bin},
    gdk::Paintable,
    gettextrs::pgettext,
    glib::{Object, Properties},
    gtk::{accessible::Property, Align, IconPaintable, Overflow, Picture, Widget},
    std::marker::PhantomData,
};

mod imp {
    use super::*;

    #[derive(Debug, Default, Properties)]
    #[properties(wrapper_type = super::ArtContainer)]
    pub struct ArtContainer {
        #[property(
            set = |this: &Self, art: Option<Paintable>| {
                let obj = this.obj();
                if let Some(art) = art.as_ref() {
                    if art.is::<IconPaintable>() {
                        obj.set_width_request(160);
                        obj.set_height_request(160);
                    } else {
                        obj.set_width_request(art.intrinsic_width());
                        obj.set_height_request(art.intrinsic_height());
                    }
                }
                this.picture.set_paintable(art.as_ref());
            }
        )]
        art: PhantomData<Option<Paintable>>,
        picture: Picture,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ArtContainer {
        const NAME: &'static str = "ArtContainer";
        type Type = super::ArtContainer;
        type ParentType = Bin;

        fn class_init(klass: &mut Self::Class) {
            klass.set_css_name("artcontainer");
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for ArtContainer {
        fn constructed(&self) {
            let obj = self.obj();
            obj.set_overflow(Overflow::Hidden);
            obj.set_halign(Align::Center);
            obj.set_valign(Align::Center);
            obj.set_width_request(160);
            obj.set_height_request(160);
            let picture = &self.picture;
            picture.set_halign(Align::Center);
            picture.set_valign(Align::Center);
            picture.update_property(&[Property::Label(&pgettext("a11y", "Album cover"))]);
            obj.set_child(Some(picture));
        }
    }

    impl WidgetImpl for ArtContainer {}

    impl BinImpl for ArtContainer {}

    impl super::ArtContainer {
        pub fn new() -> Self {
            Object::new()
        }
    }

    impl Default for super::ArtContainer {
        fn default() -> Self {
            Self::new()
        }
    }
}

glib::wrapper! {
    pub struct ArtContainer(ObjectSubclass<imp::ArtContainer>)
        @extends Widget, Bin;
}
