use {
    crate::{
        i18n::fgettext,
        library,
        models::{
            ImportDialogChoice, ImportDialogMessage, ImportFailure, ImportItem,
            ImportItemDescription, ImportSessionMessage, TaggingMode, TransferMode,
        },
        utils::{DARK_ERROR_COLOR, LIGHT_ERROR_COLOR},
    },
    adw::{
        prelude::*, subclass::prelude::*, ActionRow, AlertDialog, Dialog, EntryRow, ExpanderRow,
        NavigationView, PreferencesGroup, PreferencesRow, StyleManager,
    },
    futures::stream::StreamExt,
    gdk::pango::WrapMode,
    gettextrs::gettext,
    gio::Subprocess,
    glib::{
        subclass::InitializingObject, MainContext, Object, ParamSpec, Properties, SignalHandlerId,
        SourceId, Variant, WeakRef,
    },
    gtk::{
        accessible::{Property, Relation},
        Accessible, Align, Box, CheckButton, CompositeTemplate, Label, ListBox, Orientation,
        Widget,
    },
    ipc_channel::ipc::{IpcOneShotServer, IpcReceiver, IpcSender},
    std::{
        cell::{Cell, RefCell},
        ffi::OsStr,
        marker::PhantomData,
        ops::Deref,
        path::Path,
        time::Duration,
    },
    tracing::{debug, error, warn},
};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate, Properties)]
    #[template(resource = "/eu/a11s/Encore/import_dialog.ui")]
    #[properties(wrapper_type = super::ImportDialog)]
    pub struct ImportDialog {
        #[property(
            get = |this: &Self| {
                match serde_json::to_string(&this.choice) {
                    Ok(choice) => choice,
                    Err(e) => {
                        error!("Failed to serialize to JSON: {e}");
                        String::from("")
                    }
                }
            },
            set = |this: &Self, s: String| {
                match serde_json::from_str(&s) {
                    Ok(choice) => {
                        this.choice.replace(choice);
                    }
                    Err(e) => error!("Invalid choice JSON: {e}"),
                }
            }
        )]
        _choice: PhantomData<String>,
        choice: RefCell<ImportDialogChoice>,
        #[property(get = |this: &Self| match &*this.choice.borrow() {
            ImportDialogChoice::SearchByTitle { artist, title } => !title.is_empty() && !artist.is_empty(),
            ImportDialogChoice::SearchById(id) => !id.is_empty(),
            _ => true,
        })]
        can_continue: PhantomData<bool>,
        #[template_child]
        old_tracks_group: TemplateChild<PreferencesGroup>,
        #[template_child]
        old_tracks_list_box: TemplateChild<ListBox>,
        #[template_child]
        new_tracks_list_box: TemplateChild<ListBox>,
        #[template_child]
        choices_list_box: TemplateChild<ListBox>,
        added_candidates: Cell<i32>,
        #[template_child]
        navigation_view: TemplateChild<NavigationView>,
        #[template_child]
        id_entry_row: TemplateChild<EntryRow>,
        #[template_child]
        artist_entry_row: TemplateChild<EntryRow>,
        #[template_child]
        title_entry_row: TemplateChild<EntryRow>,
        #[template_child]
        skip_row: TemplateChild<PreferencesRow>,
        #[template_child]
        remove_old_row: TemplateChild<PreferencesRow>,
        #[template_child]
        merge_row: TemplateChild<PreferencesRow>,
        #[template_child]
        keep_row: TemplateChild<PreferencesRow>,
        #[template_child]
        import_as_is_row: TemplateChild<PreferencesRow>,
        #[template_child]
        as_tracks_row: TemplateChild<PreferencesRow>,
        #[template_child]
        group_row: TemplateChild<PreferencesRow>,
        #[template_child]
        search_by_title_row: TemplateChild<ExpanderRow>,
        #[template_child]
        search_by_id_row: TemplateChild<ExpanderRow>,
        #[template_child]
        search_by_title_check_button: TemplateChild<CheckButton>,
        #[template_child]
        search_by_id_check_button: TemplateChild<CheckButton>,
        #[template_child]
        import_as_is_check_button: TemplateChild<CheckButton>,
        #[template_child]
        skip_check_button: TemplateChild<CheckButton>,
        #[template_child]
        merge_check_button: TemplateChild<CheckButton>,
        #[template_child]
        keep_check_button: TemplateChild<CheckButton>,
        #[template_child]
        remove_old_check_button: TemplateChild<CheckButton>,
        #[template_child]
        as_tracks_check_button: TemplateChild<CheckButton>,
        #[template_child]
        groups_check_button: TemplateChild<CheckButton>,
        sender: RefCell<Option<IpcSender<ImportDialogMessage>>>,
        subprocess: RefCell<Option<Subprocess>>,
        rows_styles_signal_handler_id: RefCell<Option<SignalHandlerId>>,
        labels_with_dark_styles: RefCell<Vec<WeakRef<Label>>>,
        update_view_tag_source_id: RefCell<Option<SourceId>>,
        closing: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ImportDialog {
        const NAME: &'static str = "ImportDialog";
        type Type = super::ImportDialog;
        type ParentType = Dialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
            klass.install_property_action("dialog.activate-choice", "choice");
            klass.install_action_async("dialog.continue", None, |dialog, _, _| async move {
                let imp = dialog.imp();
                'error: {
                    if let Some(sender) = imp.sender.borrow().as_ref() {
                        if let Err(e) = sender.send(ImportDialogMessage::Choice(
                            imp.choice.replace(ImportDialogChoice::Skip),
                        )) {
                            error!("Failed sending message {e}");
                        } else {
                            break 'error;
                        }
                    } else {
                        error!("Sender is none");
                    }
                    dialog.show_fail_toast(ImportFailure::Unknown);
                }
                imp.show_searching_page();
            });
        }

        fn instance_init(obj: &InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[gtk::template_callbacks(functions)]
    impl ImportDialog {
        fn clear(&self) {
            self.old_tracks_list_box.remove_all();
            self.new_tracks_list_box.remove_all();
            for _ in 0..self.added_candidates.take() {
                if let Some(row) = self.choices_list_box.row_at_index(0) {
                    self.choices_list_box.remove(&row);
                }
            }
            self.id_entry_row.set_text("");
            self.artist_entry_row.set_text("");
            self.title_entry_row.set_text("");
            self.navigation_view.pop();
            self.skip_row.set_visible(false);
            self.import_as_is_row.set_visible(false);
            self.as_tracks_row.set_visible(false);
            self.group_row.set_visible(false);
            self.keep_row.set_visible(false);
            self.remove_old_row.set_visible(false);
            self.merge_row.set_visible(false);
            self.search_by_title_row.set_expanded(false);
            self.search_by_title_check_button.set_active(false);
            self.search_by_id_check_button.set_active(false);
            self.import_as_is_check_button.set_active(false);
            self.skip_check_button.set_active(false);
            self.merge_check_button.set_active(false);
            self.keep_check_button.set_active(false);
            self.remove_old_check_button.set_active(false);
            self.as_tracks_check_button.set_active(false);
            self.groups_check_button.set_active(false);
            self.search_by_id_row.set_expanded(false);
            self.search_by_title_row.set_visible(false);
            self.search_by_id_row.set_visible(false);
        }

        fn show_main_page(&self) {
            if let Some(source_id) = self.update_view_tag_source_id.take() {
                source_id.remove();
            }
            if self
                .navigation_view
                .visible_page()
                .and_then(|p| p.tag())
                .is_some_and(|t| t == "searching")
            {
                self.navigation_view.push_by_tag("main");
            }
        }

        fn show_searching_page(&self) {
            let obj = self.obj();
            obj.set_sensitive(false);
            let source_id = glib::timeout_add_local_once(
                Duration::from_millis(500),
                glib::clone!(
                    #[weak]
                    obj,
                    move || {
                        let imp = obj.imp();
                        if !imp.closing.get() {
                            obj.set_sensitive(true);
                        }
                        imp.navigation_view.pop_to_tag("searching");
                        imp.update_view_tag_source_id.take();
                    }
                ),
            );
            if let Some(old_source_id) = self.update_view_tag_source_id.replace(Some(source_id)) {
                old_source_id.remove();
            }
        }

        fn add_label_with_dark_style(&self, label: &Label) {
            self.labels_with_dark_styles
                .borrow_mut()
                .push(label.downgrade());
        }

        fn autoupdate_rows_with_dark_styles(&self, style_manager: &StyleManager) {
            let is_dark = style_manager.is_dark();
            for label in self.labels_with_dark_styles.borrow().iter() {
                if let Some(label) = label.upgrade() {
                    label.set_label(&label.label().replace(
                        &format!(
                            "<span color=\"{}\">",
                            if is_dark {
                                LIGHT_ERROR_COLOR
                            } else {
                                DARK_ERROR_COLOR
                            }
                        ),
                        &format!(
                            "<span color=\"{}\">",
                            if is_dark {
                                DARK_ERROR_COLOR
                            } else {
                                LIGHT_ERROR_COLOR
                            }
                        ),
                    ));
                }
            }
        }

        fn construct_preferences_row(
            &self,
            import_item: ImportItem,
            candidate: Option<u32>,
            autoupdate_subrows_with_dark_styles: bool,
        ) -> PreferencesRow {
            let check_button = candidate.map(|i| {
                let action_target = match serde_json::to_string(&ImportDialogChoice::Candidate(i)) {
                    Ok(t) => t,
                    Err(e) => {
                        error!("Failed to serialize to JSON: {e}");
                        String::from("")
                    }
                };
                CheckButton::builder()
                    .action_name("dialog.activate-choice")
                    .action_target(&action_target.to_variant())
                    .build()
            });
            match import_item {
                ImportItem::Single { description } => {
                    let row = self.construct_action_row(description);
                    if let Some(check_button) = check_button {
                        row.add_prefix(&check_button);
                        row.set_activatable_widget(Some(&check_button));
                    }
                    row.upcast()
                }
                ImportItem::Multiple {
                    description,
                    children,
                } => {
                    let row = ExpanderRow::builder().title(&description.title);
                    let row = if let Some(subtitle) = &description.subtitle {
                        row.subtitle(subtitle)
                    } else {
                        row
                    }
                    .build();
                    for child in children {
                        row.add_row(
                            &self.construct_expanded_item(
                                child,
                                autoupdate_subrows_with_dark_styles,
                            ),
                        );
                    }
                    if let Some(check_button) = check_button {
                        row.add_prefix(&check_button);
                        check_button.update_property(&[Property::Label(&description.title)]);
                    }
                    row.upcast()
                }
            }
        }

        fn construct_action_row(&self, value: ImportItemDescription) -> ActionRow {
            let row = ActionRow::builder().title(value.title);
            if let Some(subtitle) = value.subtitle {
                row.subtitle(subtitle)
            } else {
                row
            }
            .build()
        }

        fn construct_expanded_item(
            &self,
            value: ImportItemDescription,
            autoupdate_subrows_with_dark_styles: bool,
        ) -> Box {
            let result = Box::builder()
                .orientation(Orientation::Vertical)
                .margin_bottom(12)
                .margin_end(12)
                .margin_start(12)
                .margin_top(12)
                .spacing(6)
                .build();
            let title = Label::builder()
                .halign(Align::Start)
                .label(value.title)
                .use_markup(true)
                .wrap(true)
                .wrap_mode(WrapMode::WordChar)
                .css_classes(["title"])
                .build();
            if autoupdate_subrows_with_dark_styles {
                self.add_label_with_dark_style(&title);
            }
            result.update_relation(&[Relation::LabelledBy(&[title.upcast_ref::<Accessible>()])]);
            result.append(&title);
            if let Some(subtitle) = value.subtitle {
                let subtitle = Label::builder()
                    .halign(Align::Start)
                    .label(subtitle)
                    .use_markup(true)
                    .wrap(true)
                    .wrap_mode(WrapMode::WordChar)
                    .css_classes(["subtitle"])
                    .build();
                result.update_relation(&[Relation::DescribedBy(&[
                    subtitle.upcast_ref::<Accessible>()
                ])]);
                result.append(&subtitle);
            }
            result
        }

        #[template_callback]
        fn search_by_title_variant(artist: String, title: String) -> Variant {
            match serde_json::to_string(&ImportDialogChoice::SearchByTitle { artist, title }) {
                Ok(t) => t,
                Err(e) => {
                    error!("Failed to serialize to JSON: {e}");
                    String::from("")
                }
            }
            .to_variant()
        }

        #[template_callback]
        fn search_by_id_variant(id: String) -> Variant {
            match serde_json::to_string(&ImportDialogChoice::SearchById(id)) {
                Ok(t) => t,
                Err(e) => {
                    error!("Failed to serialize to JSON: {e}");
                    String::from("")
                }
            }
            .to_variant()
        }

        #[template_callback]
        fn activate_choice(_: ParamSpec, check_button: CheckButton) {
            if let Err(e) = check_button.activate_action(
                "dialog.activate-choice",
                check_button.action_target_value().as_ref(),
            ) {
                debug!("Failed to activate action, probably the check button is not part of the window anymore: {e}")
            }
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for ImportDialog {
        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();
            obj.connect_choice_notify(|obj| {
                obj.notify_can_continue();
            });
            self.rows_styles_signal_handler_id.replace(Some(
                StyleManager::default().connect_dark_notify(glib::clone!(
                    #[weak]
                    obj,
                    move |style_manager| {
                        obj.imp().autoupdate_rows_with_dark_styles(style_manager);
                    }
                )),
            ));
        }

        fn dispose(&self) {
            if let Some(handler_id) = self.rows_styles_signal_handler_id.take() {
                StyleManager::default().disconnect(handler_id);
            } else {
                warn!("Styles signal handler ID not set when trying to disconnect");
            }
        }
    }

    impl WidgetImpl for ImportDialog {}

    impl AdwDialogImpl for ImportDialog {
        fn close_attempt(&self) {
            if self.closing.replace(true) {
                return;
            }
            if let Some(sender) = self.sender.take() {
                self.obj().set_sensitive(false);
                if let Err(e) = sender.send(ImportDialogMessage::Abort) {
                    debug!("Channel already closed: {e}")
                }
            } else {
                debug!("Sender is none");
            }
        }
    }

    impl Default for super::ImportDialog {
        fn default() -> Self {
            Self::new()
        }
    }

    impl super::ImportDialog {
        pub fn new() -> Self {
            Object::new::<Self>()
        }

        pub fn import(
            &self,
            library_path: &Path,
            import_path: &Path,
            transfer_mode: TransferMode,
            tagging_mode: TaggingMode,
        ) {
            let Some(receiver) =
                self.start_subprocess(library_path, import_path, transfer_mode, tagging_mode)
            else {
                self.show_fail_toast(ImportFailure::Unknown);
                self.force_close();
                return;
            };
            MainContext::default().spawn_local(glib::clone!(
                #[weak(rename_to = obj)]
                self,
                async move {
                    let mut receiver = receiver.to_stream();
                    let imp = obj.imp();
                    while let Some(message) = receiver.next().await {
                        imp.clear();
                        obj.set_sensitive(true);
                        if imp.sender.borrow().is_none() {
                            break;
                        }
                        match message {
                            Err(e) => {
                                error!("Failed receiving message {e}");
                                obj.show_fail_toast(ImportFailure::Unknown);
                                break;
                            }
                            Ok(ImportSessionMessage::StartedServer { name: _ }) => {
                                error!("Received StartedServer message twice");
                                obj.show_fail_toast(ImportFailure::Unknown);
                                break;
                            }
                            Ok(ImportSessionMessage::Finished(None)) => {
                                break;
                            }
                            Ok(ImportSessionMessage::Finished(Some(failure))) => {
                                obj.show_fail_toast(failure);
                                break;
                            }
                            Ok(ImportSessionMessage::Resume { path }) => {
                                let href = match glib::filename_to_uri(&path, None) {
                                    Ok(uri) => uri.to_string(),
                                    Err(e) => {
                                        error!("Failed to get URI: {e}");
                                        obj.show_fail_toast(ImportFailure::Unknown);
                                        break;
                                    }
                                };
                                let display = path
                                    .strip_prefix(glib::home_dir())
                                    .unwrap_or(&path)
                                    .to_string_lossy()
                                    .to_string();
                                let dialog = AlertDialog::builder()
                                    .heading(gettext("Resume import?"))
                                    .body(fgettext(
                                        // Translators: Do not translate the contents between '{' and '}', this is a variable name
                                        "Import of the folder {folder_name} was interrupted",
                                        &[(
                                            "folder_name",
                                            &format!("<a href=\"{href}\">{display}</a>"),
                                        )],
                                    ))
                                    .body_use_markup(true)
                                    .build();
                                dialog.add_response("y", &gettext("_Resume"));
                                dialog.add_response("n", &gettext("_Don't resume"));
                                if let Err(e) = if dialog.choose_future(&obj).await == "y" {
                                    if let Some(sender) = imp.sender.borrow().deref() {
                                        sender.send(ImportDialogMessage::Resume)
                                    } else {
                                        break;
                                    }
                                } else if let Some(sender) = imp.sender.borrow().deref() {
                                    sender.send(ImportDialogMessage::NoResume)
                                } else {
                                    break;
                                } {
                                    error!("Failed to send resume message: {e}");
                                    obj.show_fail_toast(ImportFailure::Unknown);
                                    break;
                                }
                                imp.show_searching_page();
                            }
                            Ok(ImportSessionMessage::ResolveConflict { old, new }) => {
                                imp.new_tracks_list_box
                                    .append(&imp.construct_preferences_row(new, None, false));
                                old.into_iter().for_each(|old| {
                                    imp.old_tracks_list_box
                                        .append(&imp.construct_preferences_row(old, None, false))
                                });
                                imp.choice.replace(ImportDialogChoice::Skip);
                                obj.notify_choice();
                                imp.old_tracks_group.set_visible(true);
                                imp.skip_row.set_visible(true);
                                imp.keep_row.set_visible(true);
                                imp.remove_old_row.set_visible(true);
                                imp.merge_row.set_visible(true);
                                imp.show_main_page();
                            }
                            Ok(ImportSessionMessage::Import {
                                item,
                                candidates,
                                can_group,
                            }) => {
                                imp.new_tracks_list_box
                                    .append(&imp.construct_preferences_row(item, None, false));
                                imp.added_candidates
                                    .set(imp.added_candidates.get() + candidates.len() as i32);
                                let choice = if candidates.is_empty() {
                                    ImportDialogChoice::Skip
                                } else {
                                    ImportDialogChoice::Candidate(0)
                                };
                                for (i, candidate) in candidates.into_iter().enumerate().rev() {
                                    imp.choices_list_box.prepend(&imp.construct_preferences_row(
                                        candidate,
                                        Some(i as u32),
                                        true,
                                    ));
                                }
                                imp.choice.replace(choice);
                                obj.notify_choice();
                                imp.autoupdate_rows_with_dark_styles(&StyleManager::default());
                                imp.skip_row.set_visible(true);
                                imp.import_as_is_row.set_visible(true);
                                imp.search_by_title_row.set_visible(true);
                                imp.search_by_id_row.set_visible(true);
                                imp.old_tracks_group.set_visible(false);
                                if can_group {
                                    imp.as_tracks_row.set_visible(true);
                                    imp.group_row.set_visible(true);
                                }
                                imp.show_main_page();
                            }
                        }
                    }
                    obj.force_close();
                    if let Some(subprocess) = imp.subprocess.take() {
                        if let Err(e) = subprocess.wait_check_future().await {
                            error!("Waiting on subprocess failed: {e}");
                        }
                    } else {
                        error!("Subprocess not set");
                    }
                }
            ));
        }

        fn start_subprocess(
            &self,
            library_path: &Path,
            import_path: &Path,
            transfer_mode: TransferMode,
            tagging_mode: TaggingMode,
        ) -> Option<IpcReceiver<ImportSessionMessage>> {
            let (session_message_server, session_message_server_name) = IpcOneShotServer::new()
                .map_err(|e| error!("Failed to start server: {e}"))
                .ok()?;
            let current_exe = std::env::current_exe()
                .map_err(|e| error!("Failed to retrieve current executable: {e}"))
                .ok()?;
            let subprocess = library::subprocess_launcher()
                .spawn(&[
                    current_exe.as_os_str(),
                    OsStr::new("import"),
                    OsStr::new("--library-path"),
                    library_path.as_os_str(),
                    OsStr::new("--server-name"),
                    OsStr::new(&session_message_server_name),
                    OsStr::new("--import-path"),
                    import_path.as_os_str(),
                    OsStr::new("--move-files"),
                    OsStr::new(&(transfer_mode == TransferMode::Move).to_string()),
                    OsStr::new("--autotag"),
                    OsStr::new(&(tagging_mode == TaggingMode::Update).to_string()),
                ])
                .map_err(|e| error!("Failed to start subprocess: {e}"))
                .ok()?;
            let imp = self.imp();
            imp.subprocess.replace(Some(subprocess));
            let (receiver, message) = session_message_server
                .accept()
                .map_err(|e| error!("Failed to receive subprocess: {e}"))
                .ok()?;
            let sender = IpcSender::connect(match message {
                ImportSessionMessage::StartedServer { name } => name,
                unexpected => {
                    error!("Expected StartedServer message, got: {unexpected:?}");
                    return None;
                }
            })
            .map_err(|e| error!("Failed to connect to server: {e}"))
            .ok()?;
            sender
                .send(ImportDialogMessage::StartedServer)
                .map_err(|e| error!("Failed to send started server message: {e}"))
                .ok()?;
            imp.sender.replace(Some(sender));
            Some(receiver)
        }

        fn show_fail_toast(&self, failure: ImportFailure) {
            let message = match failure {
                ImportFailure::Unknown => gettext("Importing failed"),
                ImportFailure::MusicBrainzNotReachable => gettext(
                    // Translators: MusicBrainz is an external service to retrieve information about music, the name should not be translated
                    "MusicBrainz is not reachable",
                ),
            };
            if let Err(e) = self.activate_action("win.show-toast", Some(&message.to_variant())) {
                error!("Failed showing toast: {e}");
            }
            self.force_close();
        }
    }
}

glib::wrapper! {
    pub struct ImportDialog(ObjectSubclass<imp::ImportDialog>)
        @extends Widget, Dialog;
}
