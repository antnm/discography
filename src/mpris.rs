use {
    crate::{application::Application, models::Repeat, player::Player, window::Window},
    gio::prelude::ApplicationExt,
    glib::MainContext,
    gstreamer_play::gst::ClockTime,
    gtk::prelude::GtkWindowExt,
    mpris_server::{
        zbus::fdo::Result, LocalPlayerInterface, LocalRootInterface, LocalServer, LoopStatus,
        Metadata, PlaybackRate, PlaybackStatus, Property, Signal, Time, TrackId, Volume,
    },
    std::{path::Path, rc::Rc},
    tracing::error,
};

struct Mpris {
    player: Player,
    window: Window,
    app: Application,
}

impl Mpris {
    fn position(&self) -> Time {
        Time::from_micros(self.player.position().useconds() as i64)
    }

    fn fullscreen(&self) -> bool {
        self.window.is_fullscreen()
    }

    fn playback_status(&self) -> PlaybackStatus {
        if !self.can_play() {
            PlaybackStatus::Stopped
        } else if self.player.playing() {
            PlaybackStatus::Playing
        } else {
            PlaybackStatus::Paused
        }
    }

    fn loop_status(&self) -> LoopStatus {
        match self.player.repeat() {
            Repeat::None => LoopStatus::None,
            Repeat::Track => LoopStatus::Track,
            Repeat::All => LoopStatus::Playlist,
        }
    }

    fn metadata(&self) -> mpris_server::zbus::fdo::Result<Metadata> {
        if let Some(track) = self.player.queue().playing_track().and_then(|t| t.track()) {
            let position = track.position();
            let mut builder = Metadata::builder();

            builder = builder.trackid(
                TrackId::try_from(format!("/eu/a11s/Encore/TrackId/{position}"))
                    .map_err(mpris_server::zbus::Error::Variant)
                    .map_err(mpris_server::zbus::fdo::Error::ZBus)?,
            );
            if let Some(track_number) = track.track_number() {
                builder = builder.track_number(track_number as i32);
            }
            if let Some(title) = track.title() {
                builder = builder.title(title);
            }
            if let Some(duration) = track.duration() {
                builder = builder.length(Time::from_micros(duration.useconds() as i64));
            }
            if let Some(artist) = track.artist() {
                builder = builder.artist([artist]);
            }
            if let Ok(url) = glib::filename_to_uri(Path::new(&track.path()), None) {
                builder = builder.url(url);
            }
            if let Some(album) = track.album() {
                if let Some(album) = album.title() {
                    builder = builder.album(album);
                }
                if let Some(art_path) = album.art_path() {
                    if let Ok(art_url) = glib::filename_to_uri(Path::new(&art_path), None) {
                        builder = builder.art_url(art_url);
                    }
                }
                if let Some(artist) = album.artist() {
                    if let Some(album_artist) = artist.title() {
                        builder = builder.album_artist([album_artist]);
                    }
                }
            }
            Ok(builder.build())
        } else {
            Ok(Metadata::builder().trackid(TrackId::NO_TRACK).build())
        }
    }

    fn can_go_next(&self) -> bool {
        self.player.can_play_next()
    }

    fn can_go_previous(&self) -> bool {
        self.player.can_play_previous()
    }

    fn can_play(&self) -> bool {
        self.player.queue().playing_track_position() != gtk::INVALID_LIST_POSITION
    }

    fn can_seek(&self) -> bool {
        self.can_play()
    }

    fn can_pause(&self) -> bool {
        self.can_play()
    }
}

pub fn start_server(player: Player, window: Window, app: Application) {
    MainContext::default().spawn_local(async move {
        let mpris = Mpris {
            player: player.clone(),
            window: window.clone(),
            app,
        };
        let mpris_server = match LocalServer::new("eu.a11s.Encore", mpris).await {
            Err(e) => {
                error!("Failed to start MPRIS server: {e}");
                return;
            }
            Ok(s) => Rc::new(s),
        };
        player.connect_position_notify(glib::clone!(
            #[weak]
            mpris_server,
            move |_| {
                MainContext::default().spawn_local(async move {
                    if let Err(e) = mpris_server
                        .emit(Signal::Seeked {
                            position: mpris_server.imp().position(),
                        })
                        .await
                    {
                        error!("Seeked notify error: {e}");
                    }
                });
            }
        ));
        window.connect_fullscreened_notify(glib::clone!(
            #[weak]
            mpris_server,
            move |_| {
                MainContext::default().spawn_local(async move {
                    if let Err(e) = mpris_server
                        .properties_changed([Property::Fullscreen(mpris_server.imp().fullscreen())])
                        .await
                    {
                        error!("Fullscreen notify error: {e}");
                    }
                });
            }
        ));
        player.queue().connect_playing_track_notify(glib::clone!(
            #[weak]
            mpris_server,
            move |_| {
                MainContext::default().spawn_local(async move {
                    let r = if let Ok(metadata) = mpris_server.imp().metadata() {
                        mpris_server
                            .properties_changed([
                                Property::PlaybackStatus(mpris_server.imp().playback_status()),
                                Property::Metadata(metadata),
                                Property::CanPlay(mpris_server.imp().can_play()),
                                Property::CanPause(mpris_server.imp().can_play()),
                                Property::CanSeek(mpris_server.imp().can_seek()),
                            ])
                            .await
                    } else {
                        mpris_server
                            .properties_changed([
                                Property::PlaybackStatus(mpris_server.imp().playback_status()),
                                Property::CanPause(mpris_server.imp().can_play()),
                                Property::CanSeek(mpris_server.imp().can_seek()),
                            ])
                            .await
                    };
                    if let Err(e) = r {
                        error!("Current playing track notify error: {e}");
                    }
                });
            }
        ));
        player.connect_playing_notify(glib::clone!(
            #[weak]
            mpris_server,
            move |_| {
                MainContext::default().spawn_local(async move {
                    if let Err(e) = mpris_server
                        .properties_changed([Property::PlaybackStatus(
                            mpris_server.imp().playback_status(),
                        )])
                        .await
                    {
                        error!("Playback status notify error: {e}");
                    }
                });
            }
        ));
        player.connect_repeat_notify(glib::clone!(
            #[weak]
            mpris_server,
            move |_| {
                MainContext::default().spawn_local(async move {
                    if let Err(e) = mpris_server
                        .properties_changed([Property::LoopStatus(
                            mpris_server.imp().loop_status(),
                        )])
                        .await
                    {
                        error!("Loop status notify error: {e}");
                    }
                });
            }
        ));
        player.connect_can_play_next_notify(glib::clone!(
            #[weak]
            mpris_server,
            move |_| {
                MainContext::default().spawn_local(async move {
                    if let Err(e) = mpris_server
                        .properties_changed([Property::CanGoNext(mpris_server.imp().can_go_next())])
                        .await
                    {
                        error!("Can play next notify error: {e}");
                    }
                });
            }
        ));
        player.connect_can_play_previous_notify(glib::clone!(
            #[weak]
            mpris_server,
            move |_| {
                MainContext::default().spawn_local(async move {
                    if let Err(e) = mpris_server
                        .properties_changed([Property::CanGoPrevious(
                            mpris_server.imp().can_go_previous(),
                        )])
                        .await
                    {
                        error!("Can play previous error: {e}");
                    }
                });
            }
        ));
        mpris_server.run().await;
    });
}

impl LocalRootInterface for Mpris {
    async fn raise(&self) -> Result<()> {
        self.window.present();
        Ok(())
    }

    async fn quit(&self) -> Result<()> {
        self.app.quit();
        Ok(())
    }

    async fn can_quit(&self) -> Result<bool> {
        Ok(true)
    }

    async fn fullscreen(&self) -> Result<bool> {
        Ok(self.fullscreen())
    }

    async fn set_fullscreen(&self, fullscreen: bool) -> mpris_server::zbus::Result<()> {
        self.window.set_fullscreened(fullscreen);
        Ok(())
    }

    async fn can_set_fullscreen(&self) -> Result<bool> {
        Ok(true)
    }

    async fn can_raise(&self) -> Result<bool> {
        Ok(true)
    }

    async fn has_track_list(&self) -> Result<bool> {
        Ok(false)
    }

    async fn identity(&self) -> Result<String> {
        Ok("Encore".to_string())
    }

    async fn desktop_entry(&self) -> Result<String> {
        Ok("eu.a11s.Encore".to_string())
    }

    async fn supported_uri_schemes(&self) -> Result<Vec<String>> {
        Ok(Vec::new())
    }

    async fn supported_mime_types(&self) -> Result<Vec<String>> {
        Ok(Vec::new())
    }
}

impl LocalPlayerInterface for Mpris {
    async fn next(&self) -> Result<()> {
        self.player.play_next();
        Ok(())
    }

    async fn previous(&self) -> Result<()> {
        self.player.play_previous();
        Ok(())
    }

    async fn pause(&self) -> Result<()> {
        self.player.set_playing(false);
        Ok(())
    }

    async fn play_pause(&self) -> Result<()> {
        self.player.set_playing(!self.player.playing());
        Ok(())
    }

    async fn stop(&self) -> Result<()> {
        self.player
            .queue()
            .set_playing_track_position(gtk::INVALID_LIST_POSITION);
        Ok(())
    }

    async fn play(&self) -> Result<()> {
        self.player.set_playing(true);
        Ok(())
    }

    async fn seek(&self, offset: Time) -> Result<()> {
        let micros = offset.as_micros();
        if micros >= 0 {
            self.player
                .seek_forward(ClockTime::from_useconds(micros as u64));
        } else {
            self.player
                .seek_backward(ClockTime::from_useconds((-micros) as u64));
        }
        Ok(())
    }

    async fn set_position(&self, _: TrackId, _: Time) -> Result<()> {
        Ok(())
    }

    async fn open_uri(&self, _: String) -> Result<()> {
        Ok(())
    }

    async fn playback_status(&self) -> Result<PlaybackStatus> {
        Ok(self.playback_status())
    }

    async fn loop_status(&self) -> Result<LoopStatus> {
        Ok(self.loop_status())
    }

    async fn set_loop_status(&self, loop_status: LoopStatus) -> mpris_server::zbus::Result<()> {
        self.player.set_repeat(match loop_status {
            LoopStatus::None => Repeat::None,
            LoopStatus::Track => Repeat::Track,
            LoopStatus::Playlist => Repeat::All,
        });
        Ok(())
    }

    async fn rate(&self) -> Result<PlaybackRate> {
        Ok(1.0)
    }

    async fn set_rate(&self, _: PlaybackRate) -> mpris_server::zbus::Result<()> {
        Ok(())
    }

    async fn shuffle(&self) -> Result<bool> {
        Ok(false)
    }

    async fn set_shuffle(&self, _: bool) -> mpris_server::zbus::Result<()> {
        Ok(())
    }

    async fn metadata(&self) -> Result<Metadata> {
        self.metadata()
    }

    async fn volume(&self) -> Result<Volume> {
        Ok(1.0)
    }

    async fn set_volume(&self, _: Volume) -> mpris_server::zbus::Result<()> {
        Ok(())
    }

    async fn position(&self) -> Result<Time> {
        Ok(self.position())
    }

    async fn minimum_rate(&self) -> Result<PlaybackRate> {
        Ok(1.0)
    }

    async fn maximum_rate(&self) -> Result<PlaybackRate> {
        Ok(1.0)
    }

    async fn can_go_next(&self) -> Result<bool> {
        Ok(self.can_go_next())
    }

    async fn can_go_previous(&self) -> Result<bool> {
        Ok(self.can_go_previous())
    }

    async fn can_play(&self) -> Result<bool> {
        Ok(self.can_play())
    }

    async fn can_pause(&self) -> Result<bool> {
        Ok(self.can_pause())
    }

    async fn can_seek(&self) -> Result<bool> {
        Ok(self.can_seek())
    }

    async fn can_control(&self) -> Result<bool> {
        Ok(true)
    }
}
